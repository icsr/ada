package br.icsr.assinador.utils;

import static org.junit.Assert.*;

import org.junit.Test;

import br.icsr.assinador.utils.Cipher;

public class CipherTest {

	String textoPlano = "Olá Israel!";

	@Test
	public void testEncryptAndDecryptBytes() {
		byte[] encryptedText = null;
		try {
			encryptedText = Cipher.encrypt(textoPlano.getBytes());
		} catch (Exception e) {
			e.printStackTrace();
			fail("Erro ao criptografar!");
		}
		try {
			assertEquals("Mensagem descriptografada não correponde ao texto plano que foi criptografado.", textoPlano, new String(Cipher.decrypt(encryptedText)));
		} catch (Exception e) {
			e.printStackTrace();
			fail("Erro ao descriptografar!");
		}

	}
	
	@Test
	public void testEncryptAndDecryptChar(){
		
		byte[] encryptedText = null;
		try {
			encryptedText = Cipher.encrypt(textoPlano.toCharArray());
		} catch (Exception e) {
			e.printStackTrace();
			fail("Erro ao criptografar!");
		}
		try {
			assertEquals("Mensagem descriptografada não correponde ao texto plano que foi criptografado.", textoPlano, new String(Cipher.decrypt(encryptedText)));
		} catch (Exception e) {
			e.printStackTrace();
			fail("Erro ao descriptografar!");
		}
		
	}

}
