package br.icsr.assinador.addon;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import br.icsr.assinador.addon.AssinadorAddon;
import br.icsr.util.AssinadorUtils;
import br.icsr.util.Base64;

public class AssinadorAddonTest {

	private static final int TEMPO_MÉDIO_ACEITÁVEL_EM_SEGUNDOS_POR_ASSINATURA = 2;
	public static final String CONTENT_TEMP_FILEPATH = System.getProperty("java.io.tmpdir") + File.separator
			+ "_content";
	private static String conteudoAAssinarPades;
	private static String compromissos;

	private static boolean primeiroAcessoAoToken = true;
	private static String conteudoAAssinarCades;

	@BeforeClass
	public static void setUp() throws Exception {
		conteudoAAssinarPades = Base64.encodeToBase64(AssinadorUtils.readFile("arquivos/conteudo/arquivo.pdf"));
		compromissos = AssinadorUtils.COMPROMISSO_AUTORIA;

		conteudoAAssinarCades = Base64.encodeToBase64(AssinadorUtils.readFile("arquivos/conteudo/arquivo.odp"));
		compromissos = AssinadorUtils.COMPROMISSO_AUTORIA;

		AssinadorAddon.exitAtEnd = false;
	}

	@AfterClass
	public static void tearDown() throws Exception {
		AssinadorAddon.exitAtEnd = true;
	}

	@Test
	public void testAssinaturaDeUmDocumentoCades() throws Exception {
		assinarMixed(0, 1);
	}

	@Test
	public void testAssinaturaDeDoisDocumentosCades() throws Exception {
		assinarMixed(0, 2);
	}

	@Test
	public void testAssinaturaDeUmDocumentoPades() throws Exception {
		assinarMixed(1, 0);
	}

	@Test
	public void testAssinaturaDeDoisDocumentosPades() throws Exception {
		assinarMixed(2, 0);
	}

	@Test
	public void testAssinaturaDeCincoDocumentosPadesECades() throws Exception {

		assinarMixed(2, 3);
	}

	private void assinarMixed(int quantidadeDePades, int quantidadeDeCades) throws Exception {

		BufferedWriter writer = new BufferedWriter(new FileWriter(CONTENT_TEMP_FILEPATH));

		String conteudosAAssinarPades = repetirObjetoComSeparador(conteudoAAssinarPades, quantidadeDePades);

		String conteudosAAssinarCades = repetirObjetoComSeparador(conteudoAAssinarCades, quantidadeDeCades);

		StringBuilder conteudosAAssinar = new StringBuilder();

		if (conteudosAAssinarPades != null && conteudosAAssinarCades != null) {
			conteudosAAssinar.append(conteudosAAssinarPades);
			conteudosAAssinar.append(AssinadorUtils.SEPARADOR_OBJETOS);
			conteudosAAssinar.append(conteudosAAssinarCades);
		} else {
			if (conteudosAAssinarPades == null) {
				conteudosAAssinar.append(conteudosAAssinarCades);
			} else {
				conteudosAAssinar.append(conteudosAAssinarPades);
			}
		}

		writer.append(conteudosAAssinar.toString());

		writer.flush();
		writer.close();

		AssinadorUtils.tic();
		AssinadorAddon
				.main(new String[] { "-commitments", repetirObjetoComSeparador(compromissos, quantidadeDeCades + quantidadeDePades), "-contentsFilepath",
						CONTENT_TEMP_FILEPATH});
		long fimTempo = AssinadorUtils.toc();

		BufferedReader reader = new BufferedReader(new FileReader(CONTENT_TEMP_FILEPATH));

		String conteudoDevolvidoPeloAssinadorAddon = reader.readLine();
		reader.close();

		assertEquals(
				"Quantidade de documentos devolvida pelo assinador não corresponde à quantidade que lhe foi passada",
				conteudosAAssinar.toString().split(AssinadorUtils.SEPARADOR_OBJETOS).length,
				conteudoDevolvidoPeloAssinadorAddon.split(AssinadorUtils.SEPARADOR_OBJETOS).length);

		int contador = 0;
		for (String str : conteudoDevolvidoPeloAssinadorAddon.split(AssinadorUtils.SEPARADOR_OBJETOS)) {
			if (contador++ < quantidadeDePades) {
				assertTrue("Conteúdo devolvido pelo assinador deveria ser um PADES, ie. um PDF assinado.",
						AssinadorUtils.isPADES(str));
			} else {
				// TODO falta implementar o método isCADES()
				// assertTrue("Conteúdo devolvido pelo assinador deveria ser um
				// CADES, ie. um não PDF assinado.",
				// AssinadorUtils.isCADES(str));
			}
		}

		verificarPerformanceIgnorandoPrimeiroAcesso(quantidadeDeCades + quantidadeDePades, fimTempo);

	}

	private int tempoMedio(long tempoExecucao, int quantidade) {
		return (int) (tempoExecucao / (1000 * quantidade));
	}

	private String repetirObjetoComSeparador(String objetoEmBase64, int quantidade) {

		if (quantidade <= 0) {
			return null;
		}

		StringBuilder str = new StringBuilder();
		for (int i = 0; i < quantidade; i++) {
			str.append(objetoEmBase64);
			if (i + 1 < quantidade) {
				str.append(AssinadorUtils.SEPARADOR_OBJETOS);
			}
		}

		return str.toString();

	}

	private void verificarPerformanceIgnorandoPrimeiroAcesso(int quantidadeDeReplicacoesDoConteudo, long fimTempo) {

		System.out.println(AssinadorUtils.concatWithSpaces("Tempo Médio para cada uma das",
				quantidadeDeReplicacoesDoConteudo, "assinatura(s):",
				AssinadorUtils.concatWithoutSpaces(tempoMedio(fimTempo, quantidadeDeReplicacoesDoConteudo), "s.")));

		if (!primeiroAcessoAoToken) {
			assertTrue(
					AssinadorUtils.concatWithSpaces("Performance degradada. O tempo médio foi",
							tempoMedio(fimTempo, quantidadeDeReplicacoesDoConteudo)),
					tempoMedio(fimTempo,
							quantidadeDeReplicacoesDoConteudo) <= TEMPO_MÉDIO_ACEITÁVEL_EM_SEGUNDOS_POR_ASSINATURA);
		}

		primeiroAcessoAoToken = false;
	}

}
