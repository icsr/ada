package br.icsr.assinador.core;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import org.bouncycastle.cms.CMSSignedData;

import br.icsr.assinador.assinante.Assinante;
import br.icsr.assinador.cms.CAdESGenerator;
import br.icsr.assinador.cms.DefaultCAdESGenerator;
import br.icsr.assinador.core.exception.AssinaturaException;
import br.icsr.assinador.politica.SignaturePolicy;
import br.icsr.assinador.repository.pkcs11.exception.AccessCancelException;
import br.icsr.assinador.repository.pkcs11.exception.AccessUnavailableException;
import br.icsr.util.Base64;
import br.icsr.util.Logger;

/**
 * Assinador Digital CADES perfil AD-RB.
 * 
 * @author israelicsr
 */
public class AssinadorCades extends Assinador {

	public AssinadorCades(SignaturePolicy politicaAssinatura, String compromissosOID, String conteudosBase64) {
		this(politicaAssinatura, compromissosOID);
		this.conteudoAAssinar = conteudosBase64;
	}

	protected AssinadorCades(SignaturePolicy politicaAssinatura, String compromissosOID) {
		super(politicaAssinatura, compromissosOID);
	}

	@Override
	public String assinar(int indiceCertificadoEscolhido) throws AssinaturaException, NoSuchAlgorithmException,
			CertificateException, IOException, AccessUnavailableException, AccessCancelException, KeyStoreException {

		Logger.info("Assinando conteúdo com certificado de ",
				getCertificado(indiceCertificadoEscolhido).getSubjectDN().getName());

		atualizarAndamento(10, 100, "Assinando conteúdo digital em CADES...");

		checkConteudoCodificadoEmBase64(getConteudoAAssinar());

		try {
			atualizarAndamento(10, 100, "Acessando keystore...");
			Assinante chaveSignatario = getAssinante(indiceCertificadoEscolhido);

			if (chaveSignatario == null) {
				return getConteudoAAssinar();
			}

			X509Certificate certificado = chaveSignatario.getCertificado();

			Logger.info("Assinando com certificado de ", certificado.getSubjectDN().getName());

			atualizarAndamento(20, 100, "Compilando CMS...");
			PrivateKey chavePrivada = chaveSignatario.getPrivateKey();
			X509Certificate[] cadeiaCertificados = chaveSignatario.getCadeia();

			CAdESGenerator gen = new DefaultCAdESGenerator(getSignature());

			atualizarAndamento(10, 100, "Finalizando CADES, aguarde...");
			gen.adicionarAssinante(certificado, chavePrivada, cadeiaCertificados, this.politicaAssinatura,
				 this.compromissoOID);

			CMSSignedData sigData = gen.gerar(Base64.decode(getConteudoAAssinar()));

			atualizarAndamento(10, 100, "CADES gerado com sucesso...");

			return Base64.encodeToBase64(sigData.getEncoded());
		} catch (Exception e) {
			e.printStackTrace();
			Logger.critical("Erro Crítico! ", e.getMessage());

			return getConteudoAAssinar();
		}

	}

}