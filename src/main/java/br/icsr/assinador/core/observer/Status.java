package br.icsr.assinador.core.observer;

import br.icsr.assinador.core.observer.Observable;
import br.icsr.assinador.core.observer.StatusObserver;
import br.icsr.assinador.core.observer.annotation.StatusObserverCallback;
import br.icsr.util.Logger;

/**
 * Classe Observable do Observer Pattern.
 * @author israelicsr
 * @see StatusObserver
 * @see StatusObserverCallback
 */
public class Status implements Observable<String, Integer> {

	private String title = "";
	private Integer value = MIN_VALUE;

	@Override
	public void setValue(Integer value) {
		if (value < MIN_VALUE || value > MAX_VALUE) {
			return;
		}
		
		this.value = value;
	}

	@Override
	public Integer getValue() {
		return this.value;
	}

	@Override
	public void setLabel(String label) {
		Logger.info(label);
		this.title = label;
	}

	@Override
	public String getLabel() {
		return title;
	}

	/**
	 * Valor máximo do status
	 */
	public static int MAX_VALUE = 100;
	
	/**
	 * Valor mínimo do status
	 */
	public static int MIN_VALUE = 0;
}
