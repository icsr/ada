package br.icsr.assinador.core.observer;

/**
 * Observer
 * @author israelicsr
 * @see StatusObserver
 * @param <TITLE>
 * @param <VALUE>
 */
public interface Observer<TITLE, VALUE> {

	/**
	 * Notifica Observer para atualizar componente gráfico de informação ao
	 * usuário sobre o andamento da assinatura. Ver <a
	 * href="http://en.wikipedia.org/wiki/Observer_pattern">Observer Pattern</a>
	 * 
	 * @param observable
	 */
	public abstract void notifyObserver(Observable<TITLE, VALUE> observable);

}
