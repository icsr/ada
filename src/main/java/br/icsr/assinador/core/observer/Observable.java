package br.icsr.assinador.core.observer;

/**
 * Observable do Observer Pattern
 * @author israelicsr
 *
 * @param <LABEL>
 * @param <VALUE>
 * @see StatusObserver
 */
public interface Observable<LABEL, VALUE> {

	/**
	 * Setter do Valor da Propriedade em Observação
	 * @param value valor da propriedade
	 */
	public abstract void setValue(VALUE value);

	/**
	 * Getter do Valor da Propriedade em Observação
	 * @return Valor da Propriedade em Observação
	 */
	public abstract VALUE getValue();

	/**
	 * Setter do Rótulo da Propriedade em Observação
	 * @param label rótulo da propriedade em Questão
	 */
	public abstract void setLabel(LABEL label);

	/**
	 * Getter do rótulo da Propriedade em Observação
	 * @return rótulo da Propriedade em Observação
	 */
	public abstract LABEL getLabel();

}
