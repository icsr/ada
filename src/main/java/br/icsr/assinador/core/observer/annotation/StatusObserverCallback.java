package br.icsr.assinador.core.observer.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import br.icsr.assinador.core.observer.StatusObserver;

/**
 * Anotação para identificar o método que o {@link StatusObserver} deve invocar ao 
 * ser notificado.
 * @author israelicsr
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface StatusObserverCallback {
	
}
