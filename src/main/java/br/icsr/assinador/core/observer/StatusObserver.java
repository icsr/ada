package br.icsr.assinador.core.observer;

import java.lang.reflect.Method;

import br.icsr.assinador.core.exception.ParametroNaoDefinidoException;
import br.icsr.assinador.core.observer.annotation.StatusObserverCallback;

/**
 * Observer Pattern para atualizar componente gráfico de informação ao usuário
 * sobre o andamento da assinatura.
 * 
 * @see <a href="http://en.wikipedia.org/wiki/Observer_pattern">Observer
 *      Pattern</a>
 * @author israelicsr
 *
 * @param <TITLE>
 *            Título do Parâmetro observer
 * @param <VALUE>
 *            Valor do Parâmetro do observer
 * @see StatusObserverCallback
 */
public final class StatusObserver implements Observer<String, Integer> {

	public static final int MAX_VALUE_FINDING_TOKEN = 100;

	private Object targetObject;
	
	public static int INCREMENTO_MIN = 5;

	public StatusObserver(Object target) {
		this.targetObject = target;
	}

	@Override
	public void notifyObserver(Observable<String, Integer> observable) {
		
		Class<? extends Object> clazz = targetObject.getClass();

		for (Method m : clazz.getMethods()) {
			if (!m.isAnnotationPresent(StatusObserverCallback.class) && !m.getName().matches("atualizarAndamento")) {
				continue;
			}
			try {

				if (m.getParameterTypes()[0].isInstance(String.class)) {
					throw new ParametroNaoDefinidoException(
							"Parâmetro do método " + m.getName()
									+ " deve ser uma String.");
				}

				if (m.getParameterTypes()[1].isInstance(Integer.class)) {
					throw new ParametroNaoDefinidoException(
							"Parâmetro do método " + m.getName()
									+ " deve ser uma Integer.");
				}

				m.invoke(targetObject, observable.getLabel(), observable.getValue());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		
	}
	
}
