package br.icsr.assinador.core;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.Signature;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import br.icsr.assinador.assinante.Assinante;
import br.icsr.assinador.core.exception.AssinaturaException;
import br.icsr.assinador.core.observer.Observable;
import br.icsr.assinador.core.observer.Status;
import br.icsr.assinador.core.observer.StatusObserver;
import br.icsr.assinador.politica.SignaturePolicy;
import br.icsr.assinador.politica.exception.SignaturePolicyException;
import br.icsr.assinador.repository.KeyRepository;
import br.icsr.assinador.repository.KeyRepositoryFactory;
import br.icsr.assinador.repository.Repository;
import br.icsr.assinador.repository.pkcs11.exception.AccessCancelException;
import br.icsr.assinador.repository.pkcs11.exception.AccessUnavailableException;
import br.icsr.util.AssinadorUtils;
import br.icsr.util.Base64;

/**
 * Classe para Assinar Digitalmente um conteúdo.
 * 
 * @author 1T Israel C. S. Rocha (CCASJ)
 */
public abstract class Assinador {

	static {
		AssinadorUtils.addProvider(AssinadorUtils.getBCProvider());
	}

	protected static final long serialVersionUID = 1L;

	/**
	 * Política de Assinatura
	 */
	protected SignaturePolicy politicaAssinatura;

	/**
	 * OID do compromisso a ser assumido com a assinatura.
	 * 
	 * @see Assinador#setCompromissoOID(String)
	 */
	protected String compromissoOID;

	KeyRepository keyRepository;
	Signature signature;

	protected Observable<String, Integer> status;

	protected StatusObserver statusChangeObserver;

	protected String conteudoAAssinar;

	/**
	 * Construtor.
	 * 
	 * @param politicaAssinatura
	 *            uma instância de {@link SignaturePolicy}.
	 * @param compromissoOID
	 *            v. {@link #setCompromissoOID(String)}
	 */
	protected Assinador(SignaturePolicy politicaAssinatura, String compromissoOID, String conteudoAAsssinarEmBase64) {
		this(politicaAssinatura, compromissoOID);
		this.conteudoAAssinar = conteudoAAsssinarEmBase64;
	}

	/**
	 * Construtor.
	 * 
	 * @param politicaAssinatura
	 *            uma instância de {@link SignaturePolicy}.
	 * @param compromissoOID
	 *            v. {@link #setCompromissoOID(String)}
	 */
	protected Assinador(SignaturePolicy politicaAssinatura, String compromissoOID) {
		this.politicaAssinatura = politicaAssinatura;
		this.compromissoOID = compromissoOID;
		this.status = new Status();
	}

	/**
	 * Inicia Assinador uma vez já se tendo adicionado os {@link KeyRepository}
	 * extras que se deseja utilizar
	 * 
	 * @throws NoSuchAlgorithmException
	 *             se repositório criptográfico {@link KeyRepository} não
	 *             suporta o algoritmo previsto pela Política de Assinatura
	 *             {@link SignaturePolicy}.
	 * @throws AccessCancelException
	 *             se o acesso foi cancelado pelo usuário na tela de inserção
	 *             do @see PIN
	 * @throws AccessUnavailableException
	 *             se o token está indisponível.
	 * @throws IOException
	 * @throws CertificateException
	 * @throws changeSignerException
	 */
	public void iniciar(boolean withPKCS11) throws NoSuchAlgorithmException, AccessCancelException,
			AccessUnavailableException, CertificateException, IOException {
		
		atualizarAndamento(AssinadorUtils.concatWithoutSpaces("Iniciando ", AssinadorUtils.APP_TITLE, "..."));

		if (this.keyRepository != null && this.signature != null) {
			return;
		}

		initAvailableKeyRepositoryAndSignature(withPKCS11);

	}

	private void initAvailableKeyRepositoryAndSignature(boolean withPKCS11) throws AccessUnavailableException {

		if (this.keyRepository == null) {

			try {
				this.keyRepository = KeyRepositoryFactory.create(this, withPKCS11);

				this.keyRepository.init();

				atualizarAndamento("Acessando Repositório de Chaves...");
				this.keyRepository.access();
				atualizarAndamento("Repositório inicializado corretamente.");
				this.signature = Signature.getInstance(this.politicaAssinatura.getSignerAlg(),
						this.keyRepository.getProvider());
				
			} catch (Exception e) {
				e.printStackTrace();
				throw new AccessUnavailableException(e.getMessage());
			}
		}

	}

	/**
	 * Obtém todos os certificados contidos no token
	 * 
	 * @return lista de Certificados
	 * @throws NoSuchAlgorithmException
	 *             se algoritmo não suportado pelo token
	 * @throws CertificateException
	 *             se certificado não pode ser lido
	 * @throws IOException
	 *             se token não pode ser lido
	 * @throws AccessCancelException
	 * @throws AccessUnavailableException
	 * @throws KeyStoreException
	 */
	public X509Certificate[] getCertificados() throws NoSuchAlgorithmException, CertificateException, IOException,
			AccessUnavailableException, AccessCancelException, KeyStoreException {

		return keyRepository.getCertificados();
	}

	/**
	 * Método para Assinar com certificado escolhido um determinado conteúdo.
	 * 
	 * @param indiceCertificadoAssinante
	 *            parâmetro de seleção do certificado do assinante a assinar o
	 *            documento conforme lista obtida no {@link #getCertificados()}
	 * @param conteudoBase64
	 *            conteúdo a assinar codificado em
	 *            <a href="http://pt.wikipedia.org/wiki/Base64">Base64</a>.
	 * @return conteúdo assinado e codificado em
	 *         <a href="http://pt.wikipedia.org/wiki/Base64">Base64</a>.
	 * @throws AssinaturaException
	 *             se houve algum problema ao assinar
	 * @throws IOException
	 *             se houve algum problema com o token
	 * @throws SignaturePolicyException
	 *             se houve algum problema com a Política de Assinatura
	 * @throws AccessUnavailableException
	 * @throws AccessCancelException
	 * @throws NoSuchAlgorithmException
	 * @throws CertificateException
	 * @throws KeyStoreException
	 */
	public final String assinar(int indiceCertificadoAssinante, String conteudoBase64)
			throws AssinaturaException, IOException, SignaturePolicyException, NoSuchAlgorithmException,
			AccessCancelException, AccessUnavailableException, CertificateException, KeyStoreException {
		this.conteudoAAssinar = conteudoBase64;
		return assinar(indiceCertificadoAssinante);
	}

	/**
	 * Método para Assinar com certificado escolhido e já tendo sido definido o
	 * conteúdo a assinar ({@link #setConteudoAAssinar(String)}).
	 * 
	 * @param indiceCertificadoAssinante
	 *            parâmetro de seleção do certificado do assinante a assinar o
	 *            documento conforme lista obtida no {@link #getCertificados()}
	 * @return conteúdo assinado e codificado em
	 *         <a href="http://pt.wikipedia.org/wiki/Base64">Base64</a>.
	 * @throws AssinaturaException
	 *             se houve algum problema ao assinar
	 * @throws IOException
	 *             se houve algum problema com o token
	 * @throws SignaturePolicyException
	 *             se houve algum problema com a Política de Assinatura
	 * @throws AccessUnavailableException
	 * @throws AccessCancelException
	 * @throws NoSuchAlgorithmException
	 * @throws CertificateException
	 * @throws KeyStoreException
	 */
	public abstract String assinar(int indiceCertificadoAssinante)
			throws AssinaturaException, IOException, SignaturePolicyException, NoSuchAlgorithmException,
			AccessCancelException, AccessUnavailableException, CertificateException, KeyStoreException;

	/**
	 * Assina com primeiro certificado já tendo sido definido o conteúdo a
	 * assinar ({@link #setConteudoAAssinar(String)}).
	 * 
	 * @return conteúdo assinado e codificado em
	 *         <a href="http://pt.wikipedia.org/wiki/Base64">Base64</a>.
	 * @throws AssinaturaException
	 *             se houve algum problema ao assinar
	 * @throws IOException
	 *             se houve algum problema com o token
	 * @throws SignaturePolicyException
	 *             se houve algum problema com a Política de Assinatura
	 * @throws AccessUnavailableException
	 * @throws AccessCancelException
	 * @throws NoSuchAlgorithmException
	 * @throws CertificateException
	 * @throws KeyStoreException
	 */
	public final String assinar()
			throws AssinaturaException, IOException, SignaturePolicyException, NoSuchAlgorithmException,
			AccessCancelException, AccessUnavailableException, CertificateException, KeyStoreException {
		int selectedOption = 0;
		if (getCertificados().length > 1) {
			selectedOption = chooseSigner();
		}

		return assinar(selectedOption);
	}

	private int chooseSigner() throws NoSuchAlgorithmException, CertificateException, IOException,
			AccessCancelException, AccessUnavailableException, KeyStoreException {
		StringBuilder messagem = new StringBuilder(
				"Digite o número correspondente \nao assinante que se deseja utilizar:\n");

		List<String> signerNames = new LinkedList<String>();
		Set<Integer> acceptableOptions = new HashSet<Integer>();

		int i = 0;
		for (X509Certificate c : getCertificados()) {
			signerNames.add(Assinante.extractName(c));

			messagem.append("     ");
			messagem.append(i);
			messagem.append(" - ");
			messagem.append(signerNames.get(i));
			messagem.append("\n");
			acceptableOptions.add(i++);
		}

		int selectedOption = AssinadorUtils.selectOption(messagem.toString(), acceptableOptions);
		return selectedOption;
	}

	public final String assinar(String conteudoBase64)
			throws NoSuchAlgorithmException, CertificateException, AssinaturaException, IOException,
			SignaturePolicyException, AccessCancelException, AccessUnavailableException, KeyStoreException {
		this.conteudoAAssinar = conteudoBase64;
		return assinar();
	}

	/**
	 * Define o OID do compromisso estabelecido pelo assinante com relação ao
	 * conteúdo. <br/>
	 * Os OID previstos pela ICP-Brasil são:
	 * <ul>
	 * <li>Concordância <br/>
	 * 2.16.76.1.8.1 <br/>
	 * A assinatura aposta indica que o signatário concorda com o conteúdo
	 * assinado. <br/>
	 * </li>
	 * <li>Autorização <br/>
	 * 2.16.76.1.8.2 <br/>
	 * A assinatura aposta indica que o signatário autoriza<br/>
	 * o constante no conteúdo assinado. <br/>
	 * </li>
	 * <li>Testemunho <br/>
	 * 2.16.76.1.8.3 <br/>
	 * A assinatura aposta indica o compromisso de testemunho do signatário. Não
	 * necessariamente indica<br/>
	 * concordância do signatário com o conteúdo. <br/>
	 * </li>
	 * <li>Autoria <br/>
	 * 2.16.76.1.8.4 <br/>
	 * A assinatura aposta indica que o signatário foi autor<br/>
	 * do conteúdo assinado. Não necessariamente indica<br/>
	 * concordância do signatário com o conteúdo. <br/>
	 * </li>
	 * <li>Conferência <br/>
	 * 2.16.76.1.8.5 <br/>
	 * A assinatura aposta indica que o signatário realizou<br/>
	 * a conferência do conteúdo. <br/>
	 * </li>
	 * <li>Revisão <br/>
	 * 2.16.76.1.8.6 <br/>
	 * A assinatura aposta indica que o signatário revisou<br/>
	 * o conteúdo assinado. Não necessariamente indica<br/>
	 * concordância do signatário com o conteúdo. <br/>
	 * </li>
	 * <li>Ciência <br/>
	 * 2.16.76.1.8.7 <br/>
	 * A assinatura aposta indica que o signatário tomou<br/>
	 * ciência do conteúdo assinado. Não necessariamente<br/>
	 * indica concordância do signatário com o conteúdo. <br/>
	 * </li>
	 * <li>Publicação <br/>
	 * 2.16.76.1.8.8 <br/>
	 * A assinatura tem o propósito de indicar que o signatário publicou o
	 * documento em algum meio de<br/>
	 * comunicação externo à entidade que o originou. <br/>
	 * </li>
	 * <li>Protocolo <br/>
	 * 2.16.76.1.8.9 <br/>
	 * A assinatura aposta indica a intenção do signatário<br/>
	 * em protocolar o conteúdo. <br/>
	 * </li>
	 * <li>Integridade <br/>
	 * 2.16.76.1.8.10 <br/>
	 * A assinatura aposta indica a intenção do signatário<br/>
	 * em garantir somente a integridade da mensagem. <br/>
	 * </li>
	 * <li>Autenticação de<br/>
	 * usuário <br/>
	 * 2.16.76.1.8.11 <br/>
	 * A assinatura aposta é utilizada somente como prova<br/>
	 * de autenticação do signatário. <br/>
	 * </li>
	 * <li>Teste <br/>
	 * 2.16.76.1.8.12 <br/>
	 * A assinatura aposta indica a intenção do signatário<br/>
	 * em realizar um teste.</li>
	 * <ul>
	 * 
	 * @see <a href=
	 *      "http://www.jusbrasil.com.br/diarios/425865/pg-25-secao-1-diario-oficial-da-uniao-dou-de-13-01-2009">
	 *      Tipos de Compromisso na ICP-BRASIL</a>
	 * @param compromissoOID
	 *            OID do compromisso.
	 */
	public final void setCompromissoOID(String compromissoOID) {
		this.compromissoOID = compromissoOID;
	}

	/**
	 * Método para finalizar a aplicação.
	 */
	public void finalizar() {
		keyRepository.shutdown();

		AssinadorUtils.removeProvider(keyRepository.getProvider().getName());
		AssinadorUtils.removeProvider(BouncyCastleProvider.PROVIDER_NAME);

		keyRepository = null;
		signature = null;

	}

	/**
	 * Adiciona o {@link Provider} criptográfico do {@link KeyRepository} e o
	 * retorna.
	 * 
	 * @return {@link Provider} do dispositivo criptográfico.
	 */
	public Provider getTokenProvider() {

		AssinadorUtils.addProvider(keyRepository.getProvider()); //TODO refatorar para eliminar esta linha

		return keyRepository.getProvider();
	}

	private void notifyObserver() {
		if (this.statusChangeObserver != null) {
			this.statusChangeObserver.notifyObserver(this.status);
		}
	}

	/**
	 * Atualiza andamento da assinatura.
	 * 
	 * @param mensagem
	 */
	public void atualizarAndamento(String mensagem) {
		atualizarAndamento(StatusObserver.INCREMENTO_MIN, mensagem);
	}

	/**
	 * Atualiza andamento da Assinatura.
	 * @param mensagem
	 * @param progresso
	 */
	public void atualizarAndamento(int valueToAdd, int maxValue, String mensagem) {
		this.status.setLabel(AssinadorUtils.concatWithoutSpaces(mensagem));
		int newValue = this.status.getValue() + valueToAdd;
		if (newValue <= maxValue) {
			this.status.setValue(newValue);
		} else {
			this.status.setValue(maxValue);
		}

		notifyObserver();
	}

	public void atualizarAndamento(int valueToAdd, String mensagem) {
		atualizarAndamento(valueToAdd, StatusObserver.MAX_VALUE_FINDING_TOKEN, mensagem);
	}

	/**
	 * Retorna o {@link Status} do andamento da assinatura @see Observer
	 * Pattern. Esse pattern foi implementado para propiciar que o Assinador
	 * atualize algum componente gráfico na aplicação que o invocou de forma a
	 * informar ao usuário o andamento da assinatura.
	 * 
	 * @return o {@link Status} do andamento.
	 */
	public Observable<String, Integer> getStatus() {
		return this.status;
	}

	/**
	 * Verifica se conteúdo passado está codificado em Base64
	 * 
	 * @param conteudoBase64
	 * @throws AssinaturaException
	 */
	protected void checkConteudoCodificadoEmBase64(String conteudoBase64) throws AssinaturaException {
		if (conteudoBase64 == null || conteudoBase64.length() == 0) {
			throw new AssinaturaException("Nenhum conteúdo foi enviado para assinar...");
		}

		try {
			Base64.decode(conteudoBase64);
		} catch (Exception e1) {
			throw new AssinaturaException("Conteúdo enviado não está codificado em Base64.");
		}

	}

	/**
	 * Getter
	 * 
	 * @return instância do {@link Signature} utilizado por delegação para
	 *         assinar.
	 */
	protected Signature getSignature() {
		return this.signature;
	}

	/**
	 * Obtém Assinante ({@link Assinante}) cujas chaves e Certificados estão
	 * guardado no {@link KeyRepository}.
	 * 
	 * @param indiceCertificadoAssinante
	 *            índice de certificado do Assinante de acordo com a lista de
	 *            certificados obtidas pelo {@link #getCertificados()}
	 * @return Assinante ({@link Assinante})
	 * @throws NoSuchAlgorithmException
	 *             se algoritimo não existe.
	 * @throws CertificateException
	 *             se houve problema com o Certificado.
	 * @throws IOException
	 *             se {@link Repository} está indisponível.
	 * @throws AccessUnavailableException
	 *             se {@link Repository} está indisponível.
	 * @throws AccessCancelException
	 *             se acesso ao {@link Repository} foi cancelado.
	 * @throws KeyStoreException
	 * @see {@link #getCertificados()}
	 */
	public Assinante getAssinante(int indiceCertificadoAssinante) throws NoSuchAlgorithmException, CertificateException,
			IOException, AccessUnavailableException, AccessCancelException, KeyStoreException {
		if (keyRepository == null) {
			throw new AccessUnavailableException("O Repositório de Chaves não está disponível");
		}
		return keyRepository.getSigner(indiceCertificadoAssinante);
	}

	/**
	 * Getter da Chave Privada do Assinante.
	 * 
	 * @param indiceCertificadoAssinante
	 *            índice de certificado do Assinante de acordo com a lista de
	 *            certificados obtidas pelo {@link #getCertificados()}.
	 * @return Chave Privada do Assinante
	 * @throws NoSuchAlgorithmException
	 *             se algoritmo criptográfico não disponível
	 * @throws CertificateException
	 *             se há algum problema com o certificado digital
	 * @throws IOException
	 *             se há algum problema de acesso ao token
	 * @throws AccessUnavailableException
	 *             se há problema de acesso ao token
	 * @throws AccessCancelException
	 *             se operação foi cancelada pelo usuário
	 * @throws KeyStoreException
	 * @see {@link #getCertificados()}
	 * @see {@link #getAssinante(int)}
	 * @see {@link PrivateKey}
	 */
	public PrivateKey getChavePrivada(int indiceCertificadoAssinante) throws NoSuchAlgorithmException,
			CertificateException, IOException, AccessUnavailableException, AccessCancelException, KeyStoreException {
		return getAssinante(indiceCertificadoAssinante).getPrivateKey();
	}

	/**
	 * Getter do Certificado do assinante.
	 * 
	 * @param indiceCertificadoEscolhido
	 *            índice de certificado do Assinante de acordo com a lista de
	 *            certificados obtidas pelo {@link #getCertificados()}.
	 * @return Certificado do Assinante.
	 * @throws NoSuchAlgorithmException
	 * @throws CertificateException
	 * @throws IOException
	 * @throws AccessUnavailableException
	 * @throws AccessCancelException
	 * @throws KeyStoreException
	 */
	public X509Certificate getCertificado(int indiceCertificadoEscolhido) throws NoSuchAlgorithmException,
			CertificateException, IOException, AccessUnavailableException, AccessCancelException, KeyStoreException {
		return getAssinante(indiceCertificadoEscolhido).getCertificado();
	}

	/**
	 * Getter da Cadeia de Certificação do Certificado escolhido.
	 * 
	 * @param indiceCertificadoAssinante
	 * @return Array de Certificados referente ao Assinante.
	 * @throws NoSuchAlgorithmException
	 *             se algoritmo criptográfico não disponível
	 * @throws CertificateException
	 *             se há algum problema com o certificado digital
	 * @throws IOException
	 *             se há algum problema de acesso ao token
	 * @throws AccessUnavailableException
	 *             se há problema de acesso ao token
	 * @throws AccessCancelException
	 *             se operação foi cancelada pelo usuário
	 * @throws KeyStoreException
	 */
	public X509Certificate[] getCadeiaCertificado(int indiceCertificadoAssinante) throws NoSuchAlgorithmException,
			CertificateException, IOException, AccessUnavailableException, AccessCancelException, KeyStoreException {
		return getAssinante(indiceCertificadoAssinante).getCadeia();
	}

	/**
	 * Getter
	 * 
	 * @return Conteúdo a ser assinado.
	 */
	public String getConteudoAAssinar() {
		return conteudoAAssinar;
	}

	public void setConteudoAAssinar(String conteudoBase64) {
		this.conteudoAAssinar = conteudoBase64;
	}

	public void setObserver(StatusObserver observer) {
		this.statusChangeObserver = observer;

	}

	void setRepository(KeyRepository keyRepository) {
		this.keyRepository = keyRepository;
	}

}
