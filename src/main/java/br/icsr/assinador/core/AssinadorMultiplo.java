package br.icsr.assinador.core;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import br.icsr.assinador.core.exception.AssinaturaException;
import br.icsr.assinador.politica.SignaturePolicy;
import br.icsr.assinador.politica.exception.SignaturePolicyException;
import br.icsr.assinador.repository.pkcs11.exception.AccessCancelException;
import br.icsr.assinador.repository.pkcs11.exception.AccessUnavailableException;
import br.icsr.util.AssinadorUtils;
import br.icsr.util.Logger;

/**
 * Classe composta de {@link Assinador} para Assinar Digitalmente vÃ¡rios
 * conteÃºdos passados simultaneamente.
 * 
 * @author 1T Israel C. S. Rocha (CCASJ)
 */
public class AssinadorMultiplo extends Assinador {

	protected AssinadorMultiplo(SignaturePolicy politicaAssinatura,
			String compromissosOID) {
		super(politicaAssinatura, compromissosOID);
	}

	@Override
	public String assinar(int indiceCertificadoEscolhido) throws NoSuchAlgorithmException, CertificateException, KeyStoreException, AssinaturaException, IOException, SignaturePolicyException, AccessCancelException, AccessUnavailableException {

		StringBuilder conteudosAssinados = new StringBuilder();

		String[] conteudosBase64 = conteudoAAssinar
				.split(AssinadorUtils.SEPARADOR_OBJETOS);
		String[] compromissosOID = compromissoOID
				.split(AssinadorUtils.SEPARADOR_OBJETOS);

		if (conteudosBase64.length != compromissosOID.length) {
			Logger.critical("Quantidade de compromissos não corresponde à quantidade de conteúdos a serem assinados. Abortando Assinatura...");
			System.exit(0);
		}

		int incremento = calcularIncremento(conteudosBase64, 90);
		for (int i = 0; i < conteudosBase64.length; i++) {

			atualizarAndamento(incremento,
					AssinadorUtils.concatWithSpaces("Assinando",
							(i + 1), "de", conteudosBase64.length, "documentos..."));

			conteudosAssinados.append(assinarConteudo(
					indiceCertificadoEscolhido, compromissosOID[i],
					conteudosBase64[i]));

			if (i + 1 < conteudosBase64.length) {
				conteudosAssinados.append(AssinadorUtils.SEPARADOR_OBJETOS);
			}

		}

		atualizarAndamento(100, "Assinaturas concluídas com sucesso");

		return conteudosAssinados.toString();

	}

	private int calcularIncremento(String[] conteudosBase64, int maxValor) {

		return (maxValor - this.status.getValue()) / conteudosBase64.length;
	}

	private String assinarConteudo(int indiceCertificadoEscolhido,
			String compromissoOIDdoConteudo, String conteudoBase64)
			throws AssinaturaException, IOException, SignaturePolicyException,
			NoSuchAlgorithmException, AccessCancelException,
			AccessUnavailableException, CertificateException, KeyStoreException {

		Assinador assinador = new AssinadorBuilder(politicaAssinatura,
				compromissoOIDdoConteudo, conteudoBase64).build();

		assinador.keyRepository = this.keyRepository;
		assinador.signature = this.signature;

		try {
			return assinador.assinar(indiceCertificadoEscolhido);
		} finally {
			assinador.finalizar();
		}

	}

}