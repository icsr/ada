package br.icsr.assinador.core;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import br.icsr.assinador.core.observer.StatusObserver;
import br.icsr.assinador.politica.SignaturePolicy;
import br.icsr.assinador.repository.KeyRepository;
import br.icsr.assinador.repository.pkcs11.exception.AccessCancelException;
import br.icsr.assinador.repository.pkcs11.exception.AccessUnavailableException;
import br.icsr.util.AssinadorUtils;

/**
 * Builder de {@link Assinador}.
 * 
 * @author israelicsr
 *
 */
public class AssinadorBuilder {

	private Assinador assinador;
	private StatusObserver observer;

	/**
	 * Construtor.
	 * 
	 * @param politicaAssinatura
	 *            v. {@link Assinador#Assinador(SignaturePolicy, String)}
	 * @param compromissoOID
	 *            v. {@link Assinador#setCompromissoOID(String)}
	 * @param conteudosBase64
	 *            conteudo a ser assinado codificado em Base64
	 * @see Assinador#assinar(int, String)
	 */
	public AssinadorBuilder(SignaturePolicy politicaAssinatura,
			String compromissoOID, String conteudosBase64) {

		if (conteudosBase64.contains(AssinadorUtils.SEPARADOR_OBJETOS)) {
			this.assinador = new AssinadorMultiplo(politicaAssinatura,
					compromissoOID);
		} else if (AssinadorUtils.isPDF(conteudosBase64)) {
			assinador = new AssinadorPades(politicaAssinatura, compromissoOID);
		} else {
			assinador = new AssinadorCades(politicaAssinatura, compromissoOID);
		}

		this.assinador.setConteudoAAssinar(conteudosBase64);

	}

	/**
	 * Método em linguagem fluente para adicionar um {@link StatusObserver} ao
	 * Builder.
	 * 
	 * @param observer
	 *            {@link StatusObserver}
	 * @return this
	 */
	public AssinadorBuilder withStatusObserver(StatusObserver observer) {
		this.observer = observer;
		return this;
	}

	/**
	 * Método de construção de instância de Assinador.
	 * 
	 * @return Instância de {@link AssinadorPades} ou de {@link AssinadorCades}
	 *         de acordo com conteudo codificado em Base64 passado no
	 *         construtor.
	 * @throws NoSuchAlgorithmException
	 *             se algoritmo não suportaado pelo {@link KeyRepository}.
	 * @throws AccessCancelException
	 *             se acesso ao {@link KeyRepository} foi cancelado.
	 * @throws AccessUnavailableException
	 *             se {@link KeyRepository} está indisponível.
	 * @throws IOException
	 * @throws CertificateException
	 */
	public Assinador build() throws NoSuchAlgorithmException,
			AccessCancelException, AccessUnavailableException,
			CertificateException, IOException {
		this.assinador.setObserver(this.observer);
		return assinador;
	}

	public void setObserver(StatusObserver observer) {
		this.observer = observer;
	}
	
}