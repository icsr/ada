package br.icsr.assinador.core.bc;

import java.io.IOException;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;

import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.DefaultSignatureAlgorithmIdentifierFinder;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.OperatorStreamException;
import org.bouncycastle.operator.RuntimeOperatorException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;

import br.icsr.util.Logger;

/**
 * Extensão de Builder para assinar via PKCS11.
 * @author israelicsr
 *
 */
public class PKCS11ContentSignerBuilder extends JcaContentSignerBuilder {

	private Signature signature;
	private AlgorithmIdentifier sigAlgId;

	/**
	 * Construtor.
	 * @param signature Realizador da Assinatura.
	 */
	public PKCS11ContentSignerBuilder(Signature signature) {
		super(signature.getAlgorithm());
		this.signature = signature;
		this.sigAlgId = new DefaultSignatureAlgorithmIdentifierFinder()
				.find(this.signature.getAlgorithm());
	}

	/**
	 * Método de Construção.
	 */
	@Override
	public ContentSigner build(PrivateKey privateKey)
			throws OperatorCreationException {

		try {

			this.signature.initSign(privateKey);

			return new ContentSigner() {
				SignatureOutputStream stream = new SignatureOutputStream(
						signature);

				public AlgorithmIdentifier getAlgorithmIdentifier() {
					return sigAlgId;
				}

				public OutputStream getOutputStream() {
					return stream;
				}

				public byte[] getSignature() {
					try {
						return stream.getSignature();
					} catch (Exception e) {
						throw new RuntimeOperatorException(
								"exception obtaining signature: "
										+ e.getMessage(), e);
					}
				}
			};
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
			throw new OperatorCreationException("cannot create signer: "
					+ e.getMessage(), e);
		}
	}

	private class SignatureOutputStream extends OutputStream {
		private Signature sig;

		SignatureOutputStream(Signature sig) {
			this.sig = sig;
		}

		public void write(byte[] bytes, int off, int len) throws IOException {
			try {
				sig.update(bytes, off, len);
			} catch (SignatureException e) {
				throw new OperatorStreamException(
						"exception in content signer: " + e.getMessage(), e);
			}
		}

		public void write(byte[] bytes) throws IOException {
			try {
				sig.update(bytes);
			} catch (SignatureException e) {
				throw new OperatorStreamException(
						"exception in content signer: " + e.getMessage(), e);
			}
		}

		public void write(int b) throws IOException {
			try {
				sig.update((byte) b);
			} catch (SignatureException e) {
				throw new OperatorStreamException(
						"exception in content signer: " + e.getMessage(), e);
			}
		}

		byte[] getSignature() throws Exception {
			Logger.info("Signature Provider", sig.getProvider().getName());
			byte[] resp = sig.sign();
			return resp;
		}
	}

}
