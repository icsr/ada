package br.icsr.assinador.core;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Calendar;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.security.BouncyCastleDigest;
import com.itextpdf.text.pdf.security.ExternalSignature;
import com.itextpdf.text.pdf.security.MakeSignature;
import com.itextpdf.text.pdf.security.MakeSignature.CryptoStandard;

import br.icsr.assinador.core.exception.AssinaturaException;
import br.icsr.assinador.politica.SignaturePolicy;
import br.icsr.assinador.politica.exception.SignaturePolicyException;
import br.icsr.assinador.repository.pkcs11.exception.AccessCancelException;
import br.icsr.assinador.repository.pkcs11.exception.AccessUnavailableException;
import br.icsr.util.AssinadorUtils;
import br.icsr.util.Base64;

import com.itextpdf.text.pdf.security.PrivateKeySignature;

/**
 * Classe Concreta do {@link Assinador} para Assinatura Digital de PDFs.
 * 
 * @author israelicsr
 *
 */
public class AssinadorPades extends Assinador {

	/**
	 * Construtor de classe concreta.
	 * 
	 * @see Assinador#Assinador(SignaturePolicy, String)
	 */
	public AssinadorPades(SignaturePolicy politicaAssinatura,
			String compromissosOID, String conteudoBase64) {
		this(politicaAssinatura, compromissosOID);
		this.conteudoAAssinar = conteudoBase64;
	}
	
	protected AssinadorPades(SignaturePolicy politicaAssinatura,
			String compromissosOID) {
		super(politicaAssinatura, compromissosOID);
	}

	@Override
	public String assinar(int indiceCertificadoEscolhido)
			throws AssinaturaException, IOException, SignaturePolicyException {
		atualizarAndamento(10, 100, "Assinando conteúdo digital em PADES...");

		checkConteudoCodificadoEmBase64(getConteudoAAssinar());
		atualizarAndamento(10, 100, "Compilando PDF...");

		PdfStamper stp = null;
		PdfReader reader = null;
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		try {
			reader = new PdfReader(Base64.decode(getConteudoAAssinar()));
			stp = PdfStamper.createSignature(reader, out, '\0', null, true);
			
			atualizarAndamento(10, 100, "Assinando com certificado de "
							+ getAssinante(indiceCertificadoEscolhido).extractName() + "...");
			
			PdfSignatureAppearance appearance = getAppearence(stp, reader);

			ExternalSignature pks = createSignature(indiceCertificadoEscolhido);

			atualizarAndamento(20, 100, "Finalizando PADES, aguarde...");

			assinar(indiceCertificadoEscolhido, appearance, pks);

			atualizarAndamento(20, 100, "PADES finalizado com sucesso!");

			return Base64.encodeToBase64(out.toByteArray());

		} catch (AccessUnavailableException e) {
			throw new AssinaturaException("Operação cancelada pelo usuário");
		} catch (Exception e) {
			e.printStackTrace();
			AssinadorUtils
					.exibeInfo("Erro Crítico! \nPossível Solução:\n\t invoque assinador.iniciar() antes de assinador.assinar()");
		} finally {
			out.close();
		}
		return getConteudoAAssinar();
	}

	private void assinar(int indiceCertificadoEscolhido,
			PdfSignatureAppearance appearance, ExternalSignature pks)
			throws IOException, DocumentException, GeneralSecurityException,
			NoSuchAlgorithmException, CertificateException,
			AccessUnavailableException, AccessCancelException {
		MakeSignature.signDetached(appearance, new BouncyCastleDigest(), pks,
				getCadeiaCertificado(indiceCertificadoEscolhido), null, null,
				null, 0, CryptoStandard.CMS);
	}

	private ExternalSignature createSignature(int indiceCertificadoEscolhido)
			throws NoSuchAlgorithmException, CertificateException, IOException,
			AccessUnavailableException, AccessCancelException, KeyStoreException {
		ExternalSignature pks;
		pks = new PrivateKeySignature(
				getChavePrivada(indiceCertificadoEscolhido),
				this.politicaAssinatura.getSignerHashAlg(), getTokenProvider()
						.getName());
		return pks;
	}

	private PdfSignatureAppearance getAppearence(PdfStamper stp,
			PdfReader reader) {
		PdfSignatureAppearance appearance;
		appearance = stp.getSignatureAppearance();

		appearance
				.setCertificationLevel(PdfSignatureAppearance.NOT_CERTIFIED);
		Calendar dataAssinatura = Calendar.getInstance();
		appearance.setSignDate(dataAssinatura);
		if (this.compromissoOID != null) {
			appearance.setReason(this.compromissoOID);
		}
		return appearance;
	}

}
