package br.icsr.assinador.core.exception;

public class ParametroNaoDefinidoException extends Exception {

	public ParametroNaoDefinidoException(String message) {
		super(message);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -3912586985470213521L;

}
