package br.icsr.assinador.core.exception;

public class AssinaturaException extends Exception {

	public AssinaturaException(String mensagem) {
		super(mensagem);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -8646381079384824968L;

}
