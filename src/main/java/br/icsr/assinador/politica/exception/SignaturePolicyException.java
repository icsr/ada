package br.icsr.assinador.politica.exception;

public class SignaturePolicyException extends Exception {

	public SignaturePolicyException(String mensagem) {
		super(mensagem);
	}


	/**
	 * 
	 */
	private static final long serialVersionUID = 8153744393432410356L;

}
