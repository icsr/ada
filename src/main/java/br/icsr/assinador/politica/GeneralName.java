package br.icsr.assinador.politica;


public class GeneralName<T> {
	
	private T generalName;
	
	public GeneralName(T generalName){
		this.generalName = generalName;
	}

	public T getGeneralName() {
		return generalName;
	}

}
