package br.icsr.assinador.politica;

import java.util.Enumeration;

import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERGeneralizedTime;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DERUTF8String;

public class SignPolicyInfo {
	
	private ASN1ObjectIdentifier sigPolicyIdentifier;
	private DERGeneralizedTime dateOfIssue;
	private DERUTF8String fieldOfApplication;
	private PolicyIssuerName policyIssuerName;
	private SignatureValidationPolicy signatureValidationPolicy;
	private Object signPolExtensions;

	public SignPolicyInfo(ASN1Sequence der){
		extrairDados(der);
	}

	private void extrairDados(ASN1Sequence der) {

		@SuppressWarnings("unchecked")
		Enumeration<ASN1Object> objs = der.getObjects();

		while (objs.hasMoreElements()) {

			Object o = objs.nextElement();

			if (o instanceof DERObjectIdentifier && this.sigPolicyIdentifier == null) {
				this.sigPolicyIdentifier = (ASN1ObjectIdentifier) o;
			}

			else if (o instanceof DERGeneralizedTime && this.dateOfIssue == null) {
				this.dateOfIssue = (DERGeneralizedTime) o;
			}
			
			else if (o instanceof ASN1Sequence && this.policyIssuerName == null) {
				this.policyIssuerName = new PolicyIssuerName((ASN1Sequence) o);
			}

			else if (o instanceof DERUTF8String && this.fieldOfApplication == null) {
				this.fieldOfApplication = (DERUTF8String) o;
			}


			else if (o instanceof ASN1Sequence && ((ASN1Sequence) o).size() > 1 && this.signatureValidationPolicy == null) {
				this.signatureValidationPolicy = new SignatureValidationPolicy((ASN1Sequence)o);
			}
			
			else if (o instanceof ASN1Sequence && ((ASN1Sequence) o).size() > 1 && this.signatureValidationPolicy == null) {
				this.signatureValidationPolicy = new SignatureValidationPolicy((ASN1Sequence)o);
			}
			
			else if (o instanceof ASN1Sequence && this.signPolExtensions == null) {
				this.signPolExtensions = new SignPolExtensions((ASN1Sequence)o);
			}	

		}
		
	}

	public SignatureValidationPolicy getSignatureValidationPolicy() {
		return signatureValidationPolicy;
	}

	public Object getSignPolExtensions() {
		return signPolExtensions;
	}

	public ASN1ObjectIdentifier getSigPolicyIdentifier() {
		return sigPolicyIdentifier;
	}

	public DERGeneralizedTime getDateOfIssue() {
		return dateOfIssue;
	}

	public DERUTF8String getFieldOfApplication() {
		return fieldOfApplication;
	}

	public PolicyIssuerName getPolicyIssuerName() {
		return policyIssuerName;
	}

}
