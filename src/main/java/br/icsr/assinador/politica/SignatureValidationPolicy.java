package br.icsr.assinador.politica;

import java.util.Enumeration;

import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.ASN1Sequence;

public class SignatureValidationPolicy {

	private SigningPeriod signingPeriod;
	private CommonRules commonRules;
	private Object commitmentRules;
	private SignPolExtensions signPolExtensions;

	public SignatureValidationPolicy(ASN1Sequence o) {
		
		extrairDados(o);
	}

	private void extrairDados(ASN1Sequence der) {

			@SuppressWarnings("unchecked")
			Enumeration<ASN1Object> objs = der.getObjects();

			while (objs.hasMoreElements()) {

				Object o = objs.nextElement();

				if (o instanceof ASN1Sequence && this.signingPeriod == null) {
						this.signingPeriod = new SigningPeriod((ASN1Sequence) o);
				}
				
				else if (o instanceof ASN1Sequence && this.commonRules == null) {
					this.commonRules = new CommonRules((ASN1Sequence) o);
					
				}
				
//				else if(o instanceof ASN1Sequence && this.commitmentRules == null){
//					this.commitmentRules = new CommitmentRules((ASN1Sequence) o);
//				}
//				
//				else if (o instanceof ASN1Sequence && this.signPolExtensions == null) {
//					this.signPolExtensions = new SignPolExtensions((ASN1Sequence)o);
//				}


			}

		}

	public SigningPeriod getSigningPeriod() {
		return signingPeriod;
	}

	public CommonRules getCommonRules() {
		return commonRules;
	}

	public Object getCommitmentRules() {
		return commitmentRules;
	}

	public SignPolExtensions getSignPolExtensions() {
		return signPolExtensions;
	}
		
	
	

}
