package br.icsr.assinador.politica;

import java.util.Enumeration;

import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DEROctetString;

public class SignPolExtensions {

	private DERObjectIdentifier extnID;
	private DEROctetString extnValue;

	public SignPolExtensions(ASN1Sequence o) {
		extrairDados(o);
	}

	private void extrairDados(ASN1Sequence der) {
		@SuppressWarnings("unchecked")
		Enumeration<ASN1Object> objs = der.getObjects();

		while (objs.hasMoreElements()) {

			Object o = objs.nextElement();
			
			if(o instanceof DERObjectIdentifier){
				this.extnID = (DERObjectIdentifier) o;
			}
			
			else if(o instanceof DEROctetString){
				this.extnValue = (DEROctetString) o;
			}
		}
	}

	public DERObjectIdentifier getExtnID() {
		return extnID;
	}

	public DEROctetString getExtnValue() {
		return extnValue;
	}
}
