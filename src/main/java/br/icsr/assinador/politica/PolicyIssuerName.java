package br.icsr.assinador.politica;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.DERTaggedObject;

public class PolicyIssuerName {

	private List<GeneralName<?>> generalNames;

	public PolicyIssuerName(ASN1Sequence o) {
		this.generalNames = new ArrayList<GeneralName<?>>();
		extrairDados(o);
	}

	public List<GeneralName<?>> getGeneralNames() {
		return generalNames;
	}

	private void extrairDados(ASN1Sequence der) {

		@SuppressWarnings("unchecked")
		Enumeration<ASN1Object> objs = der.getObjects();

		while (objs.hasMoreElements()) {

			Object o = objs.nextElement();

			if (o instanceof DERTaggedObject) {
				this.generalNames.add(extrairDados((DERTaggedObject) o));
			}

		}
	}

	private GeneralName<?> extrairDados(DERTaggedObject der) {
		if (der instanceof DERTaggedObject) {

			switch (der.getTagNo()) {

			case 1:
			case 2:
			case 6:
				try {
					return new GeneralName<DERIA5String>(
							(DERIA5String) der.getObject());
				} catch (ClassCastException e) {
				}

			case 7:
				return new GeneralName<DEROctetString>(
						(DEROctetString) der.getObject());

			case 8:
				return new GeneralName<DERObjectIdentifier>(
						(DERObjectIdentifier) der.getObject());

			}
		}
		return null;
	}
}
