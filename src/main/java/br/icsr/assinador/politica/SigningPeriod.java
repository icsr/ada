package br.icsr.assinador.politica;

import java.util.Enumeration;

import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERGeneralizedTime;

public class SigningPeriod {
	
	private DERGeneralizedTime notBefore;
	private DERGeneralizedTime notAfter;

	public SigningPeriod(ASN1Sequence der){
		
		@SuppressWarnings("unchecked")
		Enumeration<ASN1Object> objs = der.getObjects();

		while (objs.hasMoreElements()) {

			Object o = objs.nextElement();

			if (o instanceof DERGeneralizedTime) {
				if(this.notBefore == null){
					this.notBefore = (DERGeneralizedTime)o;
				}else{
					this.notAfter = (DERGeneralizedTime)o;
				}
			}

		}
		
	}

	public DERGeneralizedTime getNotBefore() {
		return notBefore;
	}

	public DERGeneralizedTime getNotAfter() {
		return notAfter;
	}

}
