package br.icsr.assinador.politica;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.Enumeration;

import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERInteger;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DEROctetString;

import com.itextpdf.text.pdf.codec.Base64;
import com.itextpdf.text.pdf.security.DigestAlgorithms;
import com.itextpdf.text.pdf.security.EncryptionAlgorithms;

import br.icsr.assinador.core.Assinador;
import br.icsr.assinador.politica.exception.SignaturePolicyException;
import br.icsr.util.Logger;

public class SignaturePolicy {
	
	private ASN1ObjectIdentifier signPolicyHashAlg;
	private ASN1OctetString signPolicyHash;
	private SignPolicyInfo signPolicyInfo;

	/**
	 * Construtor
	 * @param politicaAssinaturaDERBase64 arquivo .DER da política de Assinatura codificado em Base64.
	 * @throws IOException
	 * @throws SignaturePolicyException
	 * @see {@link Assinador#Assinador(SignaturePolicy, String)}
	 */
	public SignaturePolicy(String politicaAssinaturaDERBase64)
			throws IOException, SignaturePolicyException {
		this(Base64.decode(politicaAssinaturaDERBase64));
	}

	/**
	 * Construtor
	 * @param politicaAssinaturaDER bytes do arquivo .DER da política de Assinatura.
	 * @throws IOException
	 * @throws SignaturePolicyException
	 */
	@SuppressWarnings("resource")
	public SignaturePolicy(byte[] politicaAssinaturaDER) throws IOException, SignaturePolicyException {
		ASN1Primitive asn1Primitive = new ASN1InputStream(politicaAssinaturaDER)
				.readObject();

		if (!(asn1Primitive instanceof ASN1Sequence)) {
			throw new IOException("Not a ASN1Sequence!");
		}

		extrairDados((ASN1Sequence) asn1Primitive);
		checarPolitica();

		Logger.info(this.toString());
	}

	private void checarPolitica() throws SignaturePolicyException {
		
		Date d1 = null;
		Date d2 = null;
		try {
			d1 = getSignPolicyInfo().getSignatureValidationPolicy().getSigningPeriod().getNotBefore().getDate();
			
			if(getSignPolicyInfo().getSignatureValidationPolicy().getSigningPeriod().getNotAfter() != null){
				d2 = getSignPolicyInfo().getSignatureValidationPolicy().getSigningPeriod().getNotAfter().getDate();
			}
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		if(new Date().before(d1)){
			throw new SignaturePolicyException("Política de Assinatura não entrou em vigor");
		}
		if(d2 != null && new Date().after(d2)){
			throw new SignaturePolicyException("Política de Assinatura expirou");
		}
		
	}

	/**
	 * Getter
	 * @return Identificado da Política.
	 */
	public ASN1ObjectIdentifier getSigPolicyIdentifier() {
		return this.signPolicyInfo.getSigPolicyIdentifier();
	}
	
	/**
	 * Getter
	 * @return Algoritmo de Hash da Política de Assinatura.
	 */
	public ASN1ObjectIdentifier getSignPolicyHashAlg() {
		return signPolicyHashAlg;
	}

	/**
	 * Getter
	 * @return Hash da Política de Assinatura.
	 */
	public ASN1OctetString getSignPolicyHash() {
		return signPolicyHash;
	}

	private void extrairDados(ASN1Sequence der) {

		@SuppressWarnings("unchecked")
		Enumeration<ASN1Object> objs = der.getObjects();

		while (objs.hasMoreElements()) {

			Object o = objs.nextElement();

			if (o instanceof ASN1Sequence && this.signPolicyHashAlg == null) {
				this.signPolicyHashAlg = (ASN1ObjectIdentifier) extraiProximoElemento((ASN1Sequence) o);
			}

			else if (o instanceof ASN1Sequence && this.signPolicyInfo == null) {
				this.signPolicyInfo = new SignPolicyInfo((ASN1Sequence) o);
			}

			else if (o instanceof DEROctetString && this.signPolicyHash == null) {
				this.signPolicyHash = (DEROctetString) o;
			}
		}
	}

	static Object extraiProximoElemento(ASN1Sequence o) {
		ASN1Primitive element = (ASN1Primitive) o.getObjects().nextElement();
		if (element instanceof DERObjectIdentifier) {
			return (ASN1ObjectIdentifier) element;
		}

		return element;
	}

	@Override
	public String toString() {

		StringBuilder str = new StringBuilder();
		str.append("Política de Assinatura\n");
		str.append("Id Política: " + getSigPolicyIdentifier() + "\n");
		str.append("Hash Política: " + this.signPolicyHash + " ("
				+ DigestAlgorithms.getDigest(this.signPolicyHashAlg.toString())
				+ ")\n");
		
		str.append("Política válida de "
				+ getSignPolicyInfo().getSignatureValidationPolicy()
						.getSigningPeriod().getNotBefore().getTimeString()
				+ " a "
				+ getSignPolicyInfo().getSignatureValidationPolicy()
						.getSigningPeriod().getNotAfter().getTime() + "\n");
		str.append("Algor. Assinatura: " + getSignerAlgId() + " ("
				+ getSignerAlg() + ")\n");
		str.append("Tam. Min. Chave: " + getSignerMinKeyLength() + " bits \n");
		str.append("Campo de Aplicação: "
				+ this.getSignPolicyInfo().getFieldOfApplication());
		return str.toString();
	}

	public DERObjectIdentifier getSignerAlgId() {
		return this.getSignPolicyInfo().getSignatureValidationPolicy()
				.getCommonRules().getAlgorithmConstraintSet().getSigner()
				.getAlgAndLength().get(0).getAlgID();
	}

	public String getSignerAlg() {

		return getSignerHashAlg()
				+ "with"
				+ EncryptionAlgorithms
						.getAlgorithm(getSignerAlgId().toString());

	}

	public DERInteger getSignerMinKeyLength() {
		return this.getSignPolicyInfo().getSignatureValidationPolicy()
				.getCommonRules().getAlgorithmConstraintSet().getSigner()
				.getAlgAndLength().get(0).getMinKeyLength();
	}

	public String getSignerHashAlg() {
		return DigestAlgorithms.getDigest(getSignerAlgId().toString());
	}

	public SignPolicyInfo getSignPolicyInfo() {
		return signPolicyInfo;
	}

}
