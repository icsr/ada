package br.icsr.assinador.politica;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.ASN1Sequence;

public class AlgorithmConstraints {

	private List<AlgAndLength> algAndLength;

	public AlgorithmConstraints(ASN1Sequence der) {
		this.algAndLength = new ArrayList<AlgAndLength>();
		extrairDados(der);
	}

	private void extrairDados(ASN1Sequence der) {

		@SuppressWarnings({ "unchecked" })
		Enumeration<ASN1Object> sequence = der.getObjects();

		while (sequence.hasMoreElements()) {

			Object o = sequence.nextElement();

			if (o instanceof ASN1Sequence) {
				this.algAndLength.add(new AlgAndLength((ASN1Sequence) o));
			}
		}
	}

	public List<AlgAndLength> getAlgAndLength() {
		return algAndLength;
	}
}
