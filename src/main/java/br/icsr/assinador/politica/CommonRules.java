package br.icsr.assinador.politica;

import java.util.Enumeration;

import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERTaggedObject;

public class CommonRules {

	private AlgorithmConstraintSet algorithmConstraintSet;

	public CommonRules(ASN1Sequence der) {
		extrairDados(der);
	}

	private void extrairDados(ASN1Sequence der) {

		@SuppressWarnings("unchecked")
		Enumeration<ASN1Object> sequencePrincipal = der.getObjects();

		while (sequencePrincipal.hasMoreElements()) {

			Object obj = sequencePrincipal.nextElement();

			if (obj instanceof DERTaggedObject) {

				switch (((DERTaggedObject) obj).getTagNo()) {
				case 4:
					this.algorithmConstraintSet = new AlgorithmConstraintSet(
							((DERTaggedObject) obj).getObject());
					break;
				}

			}

		}
	}

	public AlgorithmConstraintSet getAlgorithmConstraintSet() {
		return algorithmConstraintSet;
	}
}
