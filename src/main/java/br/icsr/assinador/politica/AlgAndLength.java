package br.icsr.assinador.politica;

import java.util.Enumeration;

import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERInteger;
import org.bouncycastle.asn1.DERObjectIdentifier;

public class AlgAndLength {
	
	private DERObjectIdentifier algID;
	private DERInteger minKeyLength;
	private SignPolExtensions other;

	public AlgAndLength(ASN1Sequence der) {
		extrairDados(der);
	}

	private void extrairDados(ASN1Sequence der) {
		
		@SuppressWarnings("unchecked")
		Enumeration<ASN1Object> sequence = der.getObjects();

		while(sequence.hasMoreElements()){
			
			Object o = sequence.nextElement();

			if(o instanceof DERObjectIdentifier && this.algID == null){
				this.algID = (DERObjectIdentifier)o;
			}
			
			if(o instanceof DERInteger && this.minKeyLength == null){
				this.minKeyLength = (DERInteger)o;
			}
			
			if(o instanceof ASN1Sequence && this.other == null){
				this.other = new SignPolExtensions((ASN1Sequence)o);
			}
		}		
	}

	public DERObjectIdentifier getAlgID() {
		return algID;
	}

	public DERInteger getMinKeyLength() {
		return minKeyLength;
	}

	public SignPolExtensions getOther() {
		return other;
	}

}
