package br.icsr.assinador.politica;

import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DERTaggedObject;

public class AlgorithmConstraintSet {

	private AlgorithmConstraints signer;

	public AlgorithmConstraintSet(ASN1Primitive der) {
		extrairDados(der);
	}

	private void extrairDados(ASN1Primitive der) {
		if(der instanceof DERTaggedObject){

			DERTaggedObject o = ((DERTaggedObject) der);
			
			switch(o.getTagNo()){
			case 0:
				this.signer = new AlgorithmConstraints((ASN1Sequence) o.getObject());
			}
		
		}
		else if(der instanceof DERSequence && ((DERSequence) der).size() == 1){
			extrairDados((ASN1Primitive) SignaturePolicy.extraiProximoElemento((DERSequence) der));
		}
	}

	public AlgorithmConstraints getSigner() {
		return signer;
	}
}
