package br.icsr.assinador.repository;

import br.icsr.assinador.repository.pkcs11.exception.AccessUnavailableException;

public class LastRepositoryFactory {
	
	public static LastPKCS11 create(KeyRepository tokenPKCS11){
		return new LastPKCS11(tokenPKCS11);
	}
	
	public static LastPKCS11 load() throws AccessUnavailableException {
		try {
			return LastPKCS11.loadLastTokenValidConfiguration();
		} catch (Exception e) {
			e.printStackTrace();
			throw new AccessUnavailableException(e.getMessage());
		}
	}

}
