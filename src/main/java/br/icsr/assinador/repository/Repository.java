package br.icsr.assinador.repository;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import br.icsr.assinador.assinante.Assinante;
import br.icsr.assinador.repository.pkcs11.exception.AccessCancelException;
import br.icsr.assinador.repository.pkcs11.exception.AccessUnavailableException;

public interface Repository {

	/**
	 * Retorna {@link Provider} do Repositório Criptográfico.
	 * @return Provider.
	 */
	public abstract Provider getProvider();

	/**
	 * Obtém uma instância da classe que representa um Assinante.
	 * 
	 * @param index
	 *            indice referente ao assinante.
	 * @return Um Assinante.
	 * @throws NoSuchAlgorithmException
	 *             Falha de algoritmo.
	 * @throws CertificateException
	 *             Falha de Certificado.
	 * @throws IOException
	 *             Falha de IO.
	 * @throws AccessCancelException 
	 * @throws AccessUnavailableException 
	 * @throws KeyStoreException 
	 */
	public abstract Assinante getSigner(int index)
			throws NoSuchAlgorithmException, CertificateException, IOException, AccessUnavailableException, AccessCancelException, KeyStoreException;

	/**
	 * Obtém todos os certificados contidos no {@link KeyRepository}.
	 * 
	 * @return Array de certificados
	 * @throws NoSuchAlgorithmException
	 *             Falha de algoritmo.
	 * @throws CertificateException
	 *             Falha de Certificado.
	 * @throws IOException
	 *             Falha de IO.
	 * @throws AccessCancelException 
	 * @throws AccessUnavailableException 
	 * @throws KeyStoreException 
	 */
	public abstract X509Certificate[] getCertificados()
			throws NoSuchAlgorithmException, CertificateException, IOException, AccessUnavailableException, AccessCancelException, KeyStoreException;

	/**
	 * Inicializa Repository criptográfico com o PIN já definido.
	 * @throws AccessCancelException se inicialização for cancelada
	 * @throws AccessUnavailableException se repositório indisponível.
	 * @throws IOException 
	 * @throws CertificateException 
	 * @throws NoSuchAlgorithmException 
	 */
	public abstract void access() throws AccessCancelException,
			AccessUnavailableException;


	void init() throws AccessCancelException, AccessUnavailableException;
	
	static final int MAX_ACCESS_TRIES = 3;
	
	static final int MAX_INIT_TRIES = 2;	
	
	void shutdown();
	
	
}