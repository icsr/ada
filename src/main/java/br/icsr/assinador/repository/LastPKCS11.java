package br.icsr.assinador.repository;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;

import com.itextpdf.text.pdf.codec.Base64;

import br.icsr.assinador.repository.pkcs11.TokenPKCS11;
import br.icsr.assinador.repository.pkcs11.exception.AccessUnavailableException;
import br.icsr.assinador.utils.Cipher;
import br.icsr.util.AssinadorUtils;
import br.icsr.util.Logger;

public class LastPKCS11 implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8813363129873758017L;

	String name;

	String libraryPath;

	char[] pin;

	Date date;

	int slot;

	private LastPKCS11(LastPKCS11 clonePKCS11) {
		this.name = clonePKCS11.name;
		this.libraryPath = clonePKCS11.libraryPath;
		this.date = new Date();
		this.slot = clonePKCS11.slot;
		this.pin = clonePKCS11.pin;
	}

	LastPKCS11(KeyRepository tokenPKCS11) {

		this.name = tokenPKCS11.getName();
		this.libraryPath = tokenPKCS11.getLibraryPath();
		this.date = new Date();

		this.pin = tokenPKCS11.getPin();

		this.slot = tokenPKCS11.getSlot();

	}

	private static File lastValidToken;

	static{
			lastValidToken = new File(AssinadorUtils.getAssinadorHome() + File.separator + "rep.obj");
	}

	public void persistTokenValidConfiguration() {

		try {
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(createHomeFolder()));

			out.writeObject(newEncryptedInstance());
			out.flush();
			out.close();
		} catch (Exception e) {
			Logger.error("Erro ao persistir Última configuração válida de acesso.", e.getMessage());
		}
	}

	static LastPKCS11 loadLastTokenValidConfiguration() throws FileNotFoundException, IOException {

		if (!lastValidToken.exists()) {
			throw new FileNotFoundException(
					"Nenhuma configuração válida de token foi encontrada em cache! Uma varredura será feita para determinar o token inserido. ");
		}

		ObjectInputStream in = new ObjectInputStream(new FileInputStream(lastValidToken));
		try {
			LastPKCS11 lastRepository = (LastPKCS11) in.readObject();

			LastPKCS11 decryptedLastRepository = decryptInstance(lastRepository);

			if (oldConfiguration(decryptedLastRepository)) {
				decryptedLastRepository.pin = null;
			}

			return decryptedLastRepository;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		} finally {
			in.close();
		}
	}

	private static boolean oldConfiguration(LastPKCS11 lastRepository) {
		return new Date().getTime()
				- lastRepository.date.getTime() > AssinadorUtils.MAX_TIME_TO_ACCEPT_CONFIG_IN_MINUTES * 60 * 1000;
	}

	private Object newEncryptedInstance() {

		LastPKCS11 encryptedLastRepository = new LastPKCS11(this);

		try {
			encryptedLastRepository.pin = Base64.encodeBytes(Cipher.encrypt(this.pin)).toCharArray();
		} catch (Exception e) {
			encryptedLastRepository.pin = null;
			e.printStackTrace();
		}

		return encryptedLastRepository;
	}

	private static LastPKCS11 decryptInstance(LastPKCS11 repository) {
		try {
			repository.pin = repository.pin = new String(Cipher.decrypt(Base64.decode(new String(repository.pin))))
					.toCharArray();
		} catch (Exception e) {
			repository.pin = null;
			e.printStackTrace();
		}

		return repository;
	}

	public KeyRepository getRepository() throws AccessUnavailableException {

		KeyRepository keyRepository = new TokenPKCS11(this.name, this.libraryPath);

		KeyRepository.pin = this.pin;
		keyRepository.setSlot(this.slot);

		return keyRepository;
	}

	private static File createHomeFolder() {

		if (lastValidToken.getParentFile().exists()) {
			return lastValidToken;
		}

		lastValidToken.getParentFile().mkdirs();

		return lastValidToken;
	}

	public static void removeLastTokenValidConfiguration() {
		if (lastValidToken.exists()) {
			lastValidToken.delete();
		}
	}

}
