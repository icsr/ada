package br.icsr.assinador.repository.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.EtchedBorder;

import br.icsr.assinador.core.observer.annotation.StatusObserverCallback;
import br.icsr.assinador.repository.KeyRepository;
import br.icsr.util.AssinadorUtils;

/**
 * Classe {@link JDialog} para entrada do PIN do {@link KeyRepository}.
 * @author israelicsr
 *
 */
public final class StatusDialog  extends JDialog{

	private static final Color BLUE_1 = new Color(184,195,202);
	private static final Color BLUE_2 = new Color(234,234,234);
	/**
	 * 
	 */
	private static final long serialVersionUID = 2002622445437897352L;
	private JProgressBar status;
	
	public static final int MIN_VALUE_PROGRESS = 0;
	public static final int MAX_VALUE_PROGRESS = 100;
	
	public StatusDialog() {
		super(new JFrame("tmp"));
	}
	
	private void initialize() {
		
		
		status = new JProgressBar(MIN_VALUE_PROGRESS, MAX_VALUE_PROGRESS);
		
		JPanel mainPanel = new JPanel();
		
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBackground(BLUE_2);
	
		
		status.setStringPainted(true);
		
		JPanel statusPanel = new JPanel();
		
		statusPanel.setLayout(new BorderLayout());
		statusPanel.add(new JLabel("       "), BorderLayout.NORTH);
		statusPanel.add(new JLabel("       "), BorderLayout.WEST);
		statusPanel.add(new JLabel("       "), BorderLayout.EAST);
		statusPanel.setBackground(BLUE_2);
		
		JLabel license = new JLabel(AssinadorUtils.LICENSE_SHORT);
		license.setHorizontalAlignment(JLabel.CENTER);
		license.setFont(new Font("Arial", Font.PLAIN, 10));
		
		JPanel innerPanel = new JPanel();
		innerPanel.setLayout(new GridLayout(2,0));
		innerPanel.add(status);
		innerPanel.add(license);
		innerPanel.setBackground(BLUE_2);
		
		innerPanel.setSize(410, 80);
		status.setSize(410, 60);
		
		statusPanel.add(innerPanel, BorderLayout.CENTER);
				
		mainPanel.add(statusPanel, BorderLayout.CENTER);

		this.add(mainPanel);
		
		mainPanel.add(license, BorderLayout.SOUTH);
		
		JPanel titlePanel = new JPanel(new BorderLayout());
		titlePanel.setBackground(BLUE_1);
		JLabel title = new JLabel(AssinadorUtils.APP_TITLE);
		titlePanel.add(title, BorderLayout.CENTER);
		
		titlePanel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
		
		mainPanel.setBorder(BorderFactory.createLineBorder(BLUE_1));
		title.setHorizontalAlignment(JLabel.CENTER);
		mainPanel.add(titlePanel, BorderLayout.NORTH);

		this.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		this.setAlwaysOnTop(true);
		this.setModal(false);
		this.setUndecorated(true);
		
	}
	
	@StatusObserverCallback
	public void atualizarAndamento(String mensagem, int progresso){
		status.setValue(progresso);
		status.setString(mensagem);
		status.repaint();
		this.repaint();		
	}

	
	public void exibir(){
		
		initialize();
		
		this.pack();
		this.setSize(420, 80);
//		this.setLocationByPlatform(true);
		this.setLocationRelativeTo(null);
		this.setLocation(this.getLocation().x, 0);
		this.setVisible(true);

	}
	
}
