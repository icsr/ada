package br.icsr.assinador.repository.gui;

import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;

import br.icsr.assinador.repository.KeyRepository;
import br.icsr.assinador.repository.pkcs11.exception.AccessCancelException;
import br.icsr.util.AssinadorUtils;
import br.icsr.util.Logger;

/**
 * Classe {@link JDialog} para entrada do PIN do {@link KeyRepository}.
 * @author israelicsr
 *
 */
public final class ChooseOptionDialog  extends JDialog{

	private static final long serialVersionUID = -7789934436938869404L;
	private JTextField optionField;
	private JButton cancelarButton;
	private boolean cancelSignature;

	/**
	 * Construtor.
	 */
	public ChooseOptionDialog(){
		cancelSignature = false;
	}

	private void initialize(String message) {
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));

		JPanel choicePanel = new JPanel();
		choicePanel.setLayout(new GridBagLayout());
		
		JPanel selectionPanel = new JPanel();
		selectionPanel.setLayout(new BoxLayout(selectionPanel, BoxLayout.Y_AXIS));
		
		JPanel buttonPanel = new JPanel();
		
		choicePanel.add(new JLabel(new ImageIcon(AssinadorUtils.LOGO)));
		choicePanel.add(selectionPanel);
		
		mainPanel.add(choicePanel);
		mainPanel.add(buttonPanel);
		this.add(mainPanel);
		
		optionField = new JTextField();
		final JLabel label = new JLabel();
		label.setFont(new Font("Arial", Font.PLAIN, 11));
		optionField.addKeyListener(new EnterAndCapsLockListener(this));
		optionField.setHorizontalAlignment(JTextField.CENTER);
		optionField.setFont(new Font("Arial", Font.PLAIN, 12));

		selectionPanel.add(new JLabel());
		for(String s : message.split("\n")){
			selectionPanel.add(new JLabel(s));
		}
		selectionPanel.add(optionField);
		selectionPanel.add(label);
		
		JButton assinarButton = new JButton("Ok");
		cancelarButton = new JButton("Cancelar");
		
		buttonPanel.add(assinarButton);
		buttonPanel.add(cancelarButton);
		
		assinarButton.addActionListener(new ClickListener(this));
		cancelarButton.addActionListener(new ClickListener(this));
		
		JLabel license = new JLabel(AssinadorUtils.LICENSE_SHORT);
		license.setFont(new Font("Arial", Font.PLAIN, 8));
		
		mainPanel.add(new JSeparator());
		mainPanel.add(license);

		this.setTitle(AssinadorUtils.APP_SHORT_TITLE);
		this.setAlwaysOnTop(true);
		this.setModal(true);
		
	}

	/**
	 * Método para solicitar o PIN.
	 * @return PIN digitado.
	 * @throws AccessCancelException se usuário cancelou o acesso.
	 */
	public String askForOption(String message) throws AccessCancelException {
		
		Logger.info(message);
		
		initialize(message);
		
		this.pack();
		this.setLocationByPlatform(true);
		this.setLocationRelativeTo(null);
		this.setVisible(true);

		if (cancelSignature) {
			throw new AccessCancelException("Operação cancelada pelo usuário");
		}

		Logger.info("PIN digitado!");
		return this.optionField.getText();
	}

	private final class EnterAndCapsLockListener implements KeyListener {
		private JDialog dialog;

		public EnterAndCapsLockListener(JDialog dialog) {
			this.dialog = dialog;
		}

		@Override
		public void keyTyped(KeyEvent e) {
		}

		@Override
		public void keyReleased(KeyEvent e) {
		}

		@Override
		public void keyPressed(KeyEvent e) {
			if(e.getKeyCode() == KeyEvent.VK_ENTER){
				this.dialog.setVisible(false);
			}
		}

	}
	
	private class ClickListener implements ActionListener {
		
		private JDialog dialog;

		public ClickListener(JDialog dialog){
			this.dialog = dialog;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == cancelarButton){
				cancelSignature = true;
			}
			dialog.setVisible(false);
			dialog.dispose();
		}

	}
	
}
