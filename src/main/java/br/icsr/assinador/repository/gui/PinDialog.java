package br.icsr.assinador.repository.gui;

import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JSeparator;
import javax.swing.JTextField;

import br.icsr.assinador.repository.KeyRepository;
import br.icsr.assinador.repository.pkcs11.exception.AccessCancelException;
import br.icsr.util.AssinadorUtils;
import br.icsr.util.Logger;

/**
 * Classe {@link JDialog} para entrada do PIN do {@link KeyRepository}.
 * @author israelicsr
 *
 */
public final class PinDialog  extends JDialog{

	private static final long serialVersionUID = -7789934436938869404L;
	private JPasswordField password;
	private JButton cancelarButton;
	private boolean cancelSignature;

	/**
	 * Construtor.
	 */
	public PinDialog(){
		cancelSignature = false;
	}

	private void initialize(String tokenLabel) {
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));

		JPanel choicePanel = new JPanel();
		choicePanel.setLayout(new GridBagLayout());
		
		JPanel passwordPanel = new JPanel();
		passwordPanel.setLayout(new GridLayout(4, 1));
		
		JPanel buttonPanel = new JPanel();
		
		choicePanel.add(new JLabel(new ImageIcon(AssinadorUtils.LOGO)));
		choicePanel.add(new JLabel("   "));
		choicePanel.add(passwordPanel);
		
		mainPanel.add(choicePanel);
		mainPanel.add(buttonPanel);
		this.add(mainPanel);
		
		password = new JPasswordField(15);
		final JLabel mensagem = new JLabel();
		mensagem.setFont(new Font("Arial", Font.PLAIN, 11));
		password.addKeyListener(new EnterAndCapsLockListener(this, mensagem, passwordPanel));
		password.setHorizontalAlignment(JTextField.CENTER);
		password.setFont(new Font("Arial", Font.PLAIN, 12));

		passwordPanel.add(new JLabel());
		passwordPanel.add(new JLabel(" Digite PIN de " + tokenLabel +":"));
		passwordPanel.add(password);
		passwordPanel.add(mensagem);
		choicePanel.add(passwordPanel);
		
		JButton assinarButton = new JButton("Assinar");
		cancelarButton = new JButton("Cancelar");
		
		buttonPanel.add(assinarButton);
		buttonPanel.add(cancelarButton);
		
		assinarButton.addActionListener(new ClickListener(this));
		cancelarButton.addActionListener(new ClickListener(this));
		
		JLabel license = new JLabel(AssinadorUtils.LICENSE_SHORT);
		license.setFont(new Font("Arial", Font.PLAIN, 8));
		
		mainPanel.add(new JSeparator());
		mainPanel.add(license);

		this.setTitle(AssinadorUtils.APP_SHORT_TITLE);
		this.setAlwaysOnTop(true);
		this.setModal(true);
		
	}

	/**
	 * Método para solicitar o PIN.
	 * @return PIN digitado.
	 * @throws AccessCancelException se usuário cancelou o acesso.
	 */
	public char[] askForPin(String tokenLabel) throws AccessCancelException {
		
		Logger.info("Solicitando digitação de PIN de ", tokenLabel);
		
		initialize(tokenLabel);
		
		this.pack();
		this.setLocationByPlatform(true);
		this.setLocationRelativeTo(null);
		this.setVisible(true);

		if (cancelSignature) {
			throw new AccessCancelException("Operação cancelada pelo usuário");
		}

		Logger.info("PIN digitado!");
		return this.password.getPassword();
	}

	private final class EnterAndCapsLockListener implements KeyListener {
		private final JLabel mensagem;
		private final JPanel passwordPanel;
		private JDialog dialog;

		private EnterAndCapsLockListener(JDialog dialog, JLabel mensagem, JPanel passwordPanel) {
			this.mensagem = mensagem;
			this.passwordPanel = passwordPanel;
			this.dialog = dialog;
		}

		@Override
		public void keyTyped(KeyEvent e) {
		}

		@Override
		public void keyReleased(KeyEvent e) {
			verificaCapsLock();
		}

		@Override
		public void keyPressed(KeyEvent e) {
			if(e.getKeyCode() == KeyEvent.VK_ENTER){
				this.dialog.setVisible(false);
			}
			verificaCapsLock();
		}

		private void verificaCapsLock() {
			if (Toolkit.getDefaultToolkit().getLockingKeyState(
					KeyEvent.VK_CAPS_LOCK)) {
				mensagem.setText("     Caps Lock ativado!");
			} else {
				mensagem.setText("");
			}
			passwordPanel.revalidate();
			passwordPanel.repaint();
		}
	}
	
	private class ClickListener implements ActionListener {
		
		private JDialog dialog;

		public ClickListener(JDialog dialog){
			this.dialog = dialog;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == cancelarButton){
				cancelSignature = true;
			}
			
			dialog.setVisible(false);
			dialog.dispose();
		}

	}
	
}
