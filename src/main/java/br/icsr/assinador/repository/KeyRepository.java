package br.icsr.assinador.repository;

import java.io.IOException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import br.icsr.assinador.assinante.Assinante;
import br.icsr.assinador.core.Assinador;
import br.icsr.assinador.core.observer.StatusObserver;
import br.icsr.assinador.repository.gui.PinDialog;
import br.icsr.assinador.repository.pkcs11.exception.AccessCancelException;
import br.icsr.assinador.repository.pkcs11.exception.AccessUnavailableException;
import br.icsr.util.AssinadorUtils;
import br.icsr.util.Logger;

/**
 * Classe abstrata que representa um Repositório de Chaves Criptográficas.
 * 
 * @author israelicsr
 * @see <a href=
 *      "http://docs.oracle.com/cd/B14099_19/idmanage.1012/b15975/jce.htm">
 *      Oracle JCE Provider</a>
 */
public abstract class KeyRepository implements Repository {

	public static final int MAX_PROGRESSO = 40;

	@Override
	public int hashCode() {
		return new String(this.name + this.slot + this.getLibraryPath()).hashCode();
	}

	@Override
	public boolean equals(Object obj) {

		if (!(obj instanceof KeyRepository)) {
			return false;
		}

		KeyRepository kr = (KeyRepository) obj;

		if (this.name.equals(kr.getName()) && this.slot == kr.getSlot()
				&& this.getLibraryPath().equals(kr.getLibraryPath())) {
			return true;
		}

		return false;
	}

	static final int UNDEFINED_SLOT = -1;
	/**
	 * 
	 */
	protected String name;
	protected Assinador assinador;

	static {
		AssinadorUtils.showLicense();
	}

	protected KeyRepository() {

	}

	protected KeyRepository(Assinador assinador) {
		this();
		this.assinador = assinador;
	}

	protected void atualizarAndamento(String message, int progress) {

		if (this.assinador == null) {
			return;
		}

		this.assinador.atualizarAndamento(progress, StatusObserver.MAX_VALUE_FINDING_TOKEN, message);
	}

	protected void atualizarAndamento(Object... message) {
		this.assinador.atualizarAndamento(AssinadorUtils.concatWithoutSpaces(message));
	}

	/**
	 * Provider criptográfico do Repositório de chaves.
	 * 
	 * @see
	 */
	protected Provider signProvider;
	protected static char[] pin;

	// TODO mover slot para TOKEN somente
	protected int slot = UNDEFINED_SLOT;

	/**
	 * Lista de Assinantes contidos no repositório.
	 */
	protected List<Assinante> signersList;

	/**
	 * {@link KeyStore} associado do repositório.
	 */
	protected KeyStore keystore;
	protected String libraryPath;

	@Override
	public Provider getProvider() {
		return this.signProvider;
	}

	public int getSlot() {
		return slot;
	}

	void setSlot(int slot) {
		this.slot = slot;
	}

	/**
	 * Getter do PIN do repositório.
	 * 
	 * @return PIN.
	 * @throws AccessCancelException
	 *             se PIN não disponível e pedido de inserção foi cancelado.
	 */
	protected char[] getPin() {
		return pin;
	}

	/**
	 * Setter do PIN do repositório.
	 * 
	 * @param pin
	 *            do repositório.
	 */
	protected static void setPin(char[] pin) {
		KeyRepository.pin = pin;
	}

	protected void resetSlot() {
		this.slot = UNDEFINED_SLOT;
	}

	protected boolean isSlotDefined() {
		return this.slot != UNDEFINED_SLOT;
	}

	@Override
	public Assinante getSigner(int index) throws NoSuchAlgorithmException, CertificateException, IOException,
			AccessUnavailableException, AccessCancelException, KeyStoreException {

		if (signersList == null) {
			checkAccessibility();
		}

		if (signersList == null || signersList.size() == 0 || index >= signersList.size()) {
			return null;
		}

		return signersList.get(index);
	}

	/**
	 * Retorna todos os assinantes possíveis cujos certificados estão no
	 * {@link KeyRepository}.
	 * 
	 * @return Lista de Assinantes possíveis.
	 * @throws NoSuchAlgorithmException
	 *             Falha de algoritmo.
	 * @throws CertificateException
	 *             Falha de Certificado.
	 * @throws IOException
	 *             Falha de IO.
	 * @throws AccessUnavailableException
	 * @throws AccessCancelException
	 * @throws KeyStoreException
	 */
	protected void checkAccessibility() throws NoSuchAlgorithmException, CertificateException, IOException,
			AccessUnavailableException, AccessCancelException {
		Logger.info("Listando assinantes do repositório digital...");

		if (keystore == null) {
			throw new AccessUnavailableException("Dispositivo não foi inicializado.");
		}

		this.signersList = new ArrayList<Assinante>();

		Enumeration<String> aliases;
		keystore.load(null, KeyRepository.pin);

		try {
			aliases = keystore.aliases();

			while (aliases.hasMoreElements()) {

				String s = aliases.nextElement();

				Key key = null;
				try {
					key = keystore.getKey(s, KeyRepository.pin);
				} catch (Exception e) {
					e.printStackTrace();
					if (key == null && aliases.hasMoreElements()) {
						continue;
					}
				}

				if (key == null) {
					throw new AccessUnavailableException("Dispositivo inacessível");
				}

				X509Certificate c = (X509Certificate) keystore.getCertificate(s);

				Logger.info("Encontrado assinante ", c.getSubjectDN().getName());

				try {
					c.checkValidity(new Date());
				} catch (Exception e) {
					Logger.info("O certificado de ", c.getSubjectDN().getName(), " não é válido!");
					continue;
				}

				Certificate[] cadeia = null;
				try {

					cadeia = keystore.getCertificateChain(s);

					if (this.signProvider == null) {
						this.signProvider = keystore.getProvider();
					}

				} catch (KeyStoreException e) {
					Logger.info("O certificado não tem uma cadeia de certificados associada");
				}

				if (cadeia == null) {
					cadeia = new Certificate[] { c };
				}
				Logger.info("Adicionado assinante ", c.getSubjectDN().getName());
				this.signersList.add(new Assinante((PrivateKey) key, c, cadeia, s));
			}
		} catch (KeyStoreException e) {
			e.printStackTrace();
			throw new AccessUnavailableException(e.getMessage());
		}

	}

	@Override
	public X509Certificate[] getCertificados() throws NoSuchAlgorithmException, CertificateException, IOException,
			AccessUnavailableException, AccessCancelException, KeyStoreException {

		checkAccessibility();

		if (signersList == null || signersList.size() == 0) {
			return null;
		}

		X509Certificate[] certs = new X509Certificate[signersList.size()];
		int i = 0;
		for (Assinante p : signersList) {
			certs[i++] = p.getCertificado();
		}

		return certs;
	}

	@Override
	public abstract void access() throws AccessCancelException, AccessUnavailableException;

	protected final void askForPin() throws AccessCancelException {
		KeyRepository.pin = new PinDialog().askForPin(this.name);
	}

	public String getName() {
		return name;
	}

	public String getLibraryPath() {
		return libraryPath;
	}

	
	public int chooseRepository(Map<Integer, KeyRepository> repositoriesFound) throws AccessCancelException {
		StringBuilder messagem = new StringBuilder(
				"Digite o número correspondente \nao repositório que deseja utilizar:\n");

		Set<Integer> keySet = repositoriesFound.keySet();
		Iterator<Integer> iterator = keySet.iterator();

		while (iterator.hasNext()) {
			int i = iterator.next();
			messagem.append("     ");
			messagem.append(i);
			messagem.append(" - ");
			messagem.append(((KeyRepository) repositoriesFound.get(i)).getName());
			messagem.append("\n");
		}

		return AssinadorUtils.selectOption(messagem.toString(), repositoriesFound.keySet());
	}

	public void loadKeystore(KeyStore keystore)
			throws NoSuchAlgorithmException, CertificateException, IOException, AccessCancelException {
		keystore.load(null, KeyRepository.pin);
	}
}
