package br.icsr.assinador.repository.pkcs11.exception;

public class AccessCancelException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2165713421752017365L;

	public AccessCancelException(String mensagem) {
		super(mensagem);
	}

}
