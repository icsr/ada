package br.icsr.assinador.repository.pkcs11.exception;

public class InitializationException extends Exception {

	public InitializationException(String message) {
		super(message);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 5673277549326675652L;

}
