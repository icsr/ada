package br.icsr.assinador.repository.pkcs11;

import java.io.ByteArrayInputStream;
import java.security.KeyStore;
import java.security.Provider;
import java.security.Security;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import br.icsr.assinador.repository.KeyRepository;
import br.icsr.assinador.repository.pkcs11.exception.AccessCancelException;
import br.icsr.assinador.repository.pkcs11.exception.AccessUnavailableException;
import br.icsr.assinador.repository.pkcs11.util.Console;
import br.icsr.util.AssinadorUtils;
import br.icsr.util.Logger;
import sun.security.pkcs11.SunPKCS11;

/**
 * Subclasse de {@link Token} com inicialização específica para LINUX.
 * 
 * @author israelicsr
 *
 */
@SuppressWarnings("restriction")
public class TokenPKCS11 extends Token {

	public TokenPKCS11(String name, String libraryPath) {
		super(name, libraryPath);
	}

	@Override
	public void access() throws AccessUnavailableException, AccessCancelException {
		removerPKCS11Providers();
		accessTokenUsingPKCS11();
	}

	@Override
	public void init() throws AccessCancelException, AccessUnavailableException {

		if (isSlotDefined()) {
			return;
		}

		Logger.info("Escaneando USB para token '", this.name, "' via driver PKCS11 '", this.libraryPath, "'...");

		finishScan(scan());
	}

	private Map<Integer, KeyRepository> scan() {

		Map<Integer, KeyRepository> tokensFound = new HashMap<Integer, KeyRepository>();
		
		int i = 0;
		for (int slot = 0; slot < MAX_ACCESS_TRIES; slot++) {
			try {
				Console.listen();
				new SunPKCS11(montarConfiguracao(slot));

				String tokenName = Console.readAndRelease();
				tokensFound.put(i++, new TokenInfo(tokenName, this.getLibraryPath(), slot));

				Logger.info("Encontrado Token ", tokenName, " no slot ", slot);
			} catch (Exception e) {
				Logger.warn("Nenhum token encontrado no slot ", slot);
				continue;
			} finally {
				Console.reset();
			}
		}

		return tokensFound;

	}

	private void finishScan(Map<Integer, KeyRepository> repositoriesFound)
			throws AccessCancelException, AccessUnavailableException {

		if (repositoriesFound.size() == 0) {
			throw new AccessUnavailableException("Nenhum dispositivo encontrado.");
		}

		KeyRepository selectedRepository = null;
		if (repositoriesFound.size() == 1) {
			selectedRepository = repositoriesFound.values().iterator().next();
		} else if (repositoriesFound.size() >= 2) {
			Logger.info(repositoriesFound.size(), " dispositivos encontrados.");
			selectedRepository = repositoriesFound.get(chooseRepository(repositoriesFound));
		}

		this.slot = selectedRepository.getSlot();
		this.name = selectedRepository.getName();
	}

	private ByteArrayInputStream montarConfiguracao(int selectedSlot) {
		StringBuilder str = new StringBuilder();

		str.append("name=" + this.name.replaceAll("[^\\w]+", "") + System.getProperty("line.separator"));
		str.append("library=" + this.libraryPath + System.getProperty("line.separator"));
		str.append("slotListIndex=" + selectedSlot + System.getProperty("line.separator"));
		str.append("showInfo = true" + System.getProperty("line.separator"));
		return new ByteArrayInputStream(str.toString().getBytes());
	}

	/**
	 * Remove todos os {@link Provider} PKCS11 que houver.
	 */
	protected void removerPKCS11Providers() {
		for (Provider p : Security.getProviders()) {
			if (p.getName().toLowerCase().contains("pkcs")) {
				AssinadorUtils.removeProvider(p.getName());
			}
		}

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
	}

	/**
	 * Acessa o token via padrão PKCS11.
	 * 
	 * @throws AccessUnavailableException
	 *             se acesso indisponível.
	 * @throws AccessCancelException
	 *             se acesso cancelado pelo usuário.
	 */
	protected void accessTokenUsingPKCS11() throws AccessUnavailableException, AccessCancelException {

		Logger.info("Tentando acessar Token ", this.name, " Via PKCS11 no slot ", this.getSlot());

		Provider p = null;
		try {

			p = new SunPKCS11(montarConfiguracao(this.slot));
			AssinadorUtils.addProvider(p);

			keystore = KeyStore.getInstance("PKCS11", p);
			keystore.load(null, getPin());

			this.signProvider = p;

			checkAccessibility();

			Enumeration<String> aliases = keystore.aliases();
			if (!aliases.hasMoreElements()) {
				throw new AccessUnavailableException();
			}

			Logger.info("Keystore PKCS11 OK!");
		} catch (AccessCancelException e) {
			throw new AccessCancelException(e.getMessage());
		} catch (Exception e) {
			if (p != null && p.getName() != null) {
				AssinadorUtils.removeProvider(p.getName());
			}
			keystore = null;
			throw new AccessUnavailableException(e.getMessage());
		}

	}


	/**
	 * Desativa o Token liberando recursos.
	 */
	@Override
	public void shutdown() {
		keystore = null;
		AssinadorUtils.removeProvider(signProvider.getName());

		for (Provider p : Security.getProviders()) {
			if (p.getName().toLowerCase().contains("pkcs11") || p.getName().contains("BC")) {
				AssinadorUtils.removeProvider(p.getName());
			}
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

}
