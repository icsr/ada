package br.icsr.assinador.repository.pkcs11;

import br.icsr.assinador.repository.KeyRepository;
import br.icsr.assinador.repository.pkcs11.exception.AccessCancelException;
import br.icsr.assinador.repository.pkcs11.exception.AccessUnavailableException;

/**
 * Classe que representa um {@link KeyRepository} - dispositivo criptográfivo -
 * Token ou Smartcard.
 * 
 * @author 1T Israel C. S. Rocha (CCASJ)
 * @version 1.0 - 2012
 */
public abstract class Token extends KeyRepository {

	/**
	 * Construtor
	 * 
	 * @param name
	 *            nome do {@link Token}
	 * @param libraryPath
	 *            caminho da biblioteca do sistema operacional de acesso ao
	 *            dispositivo.
	 */
	public Token(String name, String libraryPath) {
		this.name = name;
		this.libraryPath = libraryPath;
	}

	/**
	 * Getter
	 * 
	 * @return Caminho até a biblioteca de acesso ao dispositivo.
	 */
	public String getLibraryPath() {
		return libraryPath;
	}


	
	class TokenInfo extends Token{

		private TokenInfo(String name, String libraryPath) {
			super(name, libraryPath);
		}
		
		public TokenInfo(String name, String libraryPath, int slot){
			this(name, libraryPath);
			
			this.slot = slot;
		}

		@Override
		public void access() throws AccessCancelException, AccessUnavailableException {
			
		}

		@Override
		public void init() throws AccessCancelException, AccessUnavailableException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void shutdown() {
			// TODO Auto-generated method stub
			
		}

	}

}