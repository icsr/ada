package br.icsr.assinador.repository.pkcs11.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.icsr.util.Logger;

public class Console {

	private static PrintStream originalPrintStream = System.out;;
	private static ByteArrayOutputStream log;
	private static PrintStream out;

	public static void listen() throws IOException {

		log = new ByteArrayOutputStream();
		out = new PrintStream(log);
		
		System.setOut(out);
	}

	public static String readAndRelease() {
		
		try {
			out.flush();

			Logger.info(log.toString());

			Matcher m = Pattern.compile("(label\\s*:\\s*.*)").matcher(log.toString());
			m.find();

			String label = m.group().split("(:\\s*)")[1];
			
			m = Pattern.compile("(.*[^\\s])").matcher(label);
			m.find();	
			
			
			return m.group();
		} catch (Exception e) {
			return null;
		} finally {
			reset();
		}

	}

	public static void reset() {
		
		System.setOut(originalPrintStream);

		if (log != null) {
			try {
				log.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		if (out != null) {
			out.close();
		}

	}

}
