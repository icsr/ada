package br.icsr.assinador.repository.pkcs11.info;

public class TokenInfo {
	
	protected String label;
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public int getSlot() {
		return slot;
	}

	public void setSlot(int slot) {
		this.slot = slot;
	}

	public int slot;
	
	public TokenInfo(String label, int slot){
		this.label = label;
		this.slot = slot;
	}


}
