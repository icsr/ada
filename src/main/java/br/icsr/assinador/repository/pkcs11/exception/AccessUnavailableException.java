package br.icsr.assinador.repository.pkcs11.exception;

public class AccessUnavailableException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2165713421752017365L;

	public AccessUnavailableException(String mensagem) {
		super(mensagem);
	}

	public AccessUnavailableException() {
		this("Dispositivo Inacessível");
	}

}
