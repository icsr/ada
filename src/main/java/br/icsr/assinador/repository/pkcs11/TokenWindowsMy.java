package br.icsr.assinador.repository.pkcs11;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.Signature;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Enumeration;

import br.icsr.assinador.core.Assinador;
import br.icsr.assinador.repository.KeyRepository;
import br.icsr.assinador.repository.pkcs11.exception.AccessCancelException;
import br.icsr.assinador.repository.pkcs11.exception.AccessUnavailableException;
import br.icsr.util.AssinadorUtils;
import br.icsr.util.Logger;
import sun.security.mscapi.SunMSCAPI;

/**
 * Subclasse de {@link Token} com inicialização específica para WINDOWS.
 * 
 * @author israelicsr
 *
 */
public class TokenWindowsMy extends Token {

	public TokenWindowsMy() {
		super("WINDOWS-MY", null);
	}

	public TokenWindowsMy(Assinador assinador) {
		this();
		this.assinador = assinador;
	}

	@Override
	public void init() throws AccessCancelException, AccessUnavailableException {
		atualizarAndamento("Buscando dispositivo...");
		KeyRepository.pin = null;
		adicionarMSCAPIProvider();
	}

	@Override
	public void access() throws AccessUnavailableException, AccessCancelException {
		atualizarAndamento("Acessando Token Via Windows-MY");

		try {
			accessKeystore();
		} catch (Exception e) {
			throw new AccessUnavailableException(e.getMessage());
		}

	}

	private void accessKeystore() throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException,
			AccessUnavailableException, UnrecoverableKeyException, InvalidKeyException, SignatureException {
		keystore = KeyStore.getInstance("Windows-MY");
		keystore.load(null, null);
		if (keystore.size() <= 0) {
			keystore = null;
			throw new AccessUnavailableException("Repositório vazio ou inacessível");
		}

		Enumeration<String> aliases = keystore.aliases();

		while (aliases.hasMoreElements()) {
			String element = aliases.nextElement();
			PrivateKey key = (PrivateKey) keystore.getKey(element, null);
			Logger.info("Encontrada Chave Criptográfica Padrão ", key.getAlgorithm(), ".");
			Signature signer = Signature.getInstance("SHA1withRSA", this.signProvider);
			signer.initSign(key);
			signer.update("123456".getBytes());
			signer.sign();
		}

		atualizarAndamento("Keystore SunMSCAPI OK!");
	}

	private void adicionarMSCAPIProvider() {

		while (Security.getProvider("SunMSCAPI") != null) {
			AssinadorUtils.removeProvider("SunMSCAPI");
		}

		this.signProvider = new SunMSCAPI();
		AssinadorUtils.addProvider(this.signProvider);

	}
	
	/**
	 * Desativa o Token liberando recursos.
	 */
	public void shutdown() {
		keystore = null;
		AssinadorUtils.removeProvider(signProvider.getName());
	}
	
}
