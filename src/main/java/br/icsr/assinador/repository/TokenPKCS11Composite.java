package br.icsr.assinador.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.icsr.assinador.core.Assinador;
import br.icsr.assinador.repository.pkcs11.TokenPKCS11;
import br.icsr.assinador.repository.pkcs11.exception.AccessCancelException;
import br.icsr.assinador.repository.pkcs11.exception.AccessUnavailableException;
import br.icsr.util.AssinadorUtils;
import br.icsr.util.Logger;

/**
 * Composto de {@link KeyRepository} que identifica qual repositório está
 * disponível de acordo com o PIN digitado. O primeiro a ser encontrado
 * disponível será retornado pelo método
 * {@link TokenPKCS11Composite#getInstance()}
 * 
 * @author israelicsr
 */
public class TokenPKCS11Composite extends KeyRepository {

	/**
	 * 
	 */
	private static final int TRIES_TO_BLOCK = 3;
	private static List<KeyRepository> repositories;
	private KeyRepository repository;
	private static int accessErrorCount = 0;
	private static int initErrorCount = 0;

	/**
	 * Construtor
	 */
	protected TokenPKCS11Composite() {
		repositories = new ArrayList<KeyRepository>();
		addDefaultKeysRepositories();
	}

	protected TokenPKCS11Composite(Assinador assinador) {
		this();
		this.assinador = assinador;
	}

	private static void addDefaultKeysRepositories() {
		if (AssinadorUtils.isWindows()) {
			addDefaultKeyRepositoriesForWindows();
			return;
		}

		if (AssinadorUtils.isLinux()) {
			addDefaultKeyRepositoriesForLinux();
			return;
		}

		addDefaultKeyRepositoriesForMac();
	}

	private static void addDefaultKeyRepositoriesForMac() {
		repositories.add(new TokenPKCS11("morpho", "/usr/local/lib/libaetpkss.dylib"));
	}

	private static void addDefaultKeyRepositoriesForWindows() {
		repositories.add(new TokenPKCS11("aladdin", "c:/windows/system32/eTpkcs11.dll"));
		repositories.add(new TokenPKCS11("acs", "c:/windows/system32/acospkcs11.dll"));
		repositories.add(new TokenPKCS11("rainbow1", "c:/windows/system32/k1pk112.dll"));
		repositories.add(new TokenPKCS11("rainbow2", "c:/windows/system32/dkck201.dll"));
		repositories.add(new TokenPKCS11("rainbow3", "c:/windows/system32/cryptoki22.dll"));
		repositories.add(new TokenPKCS11("genérico", "c:/windows/system32/pkcs11.dll"));
		repositories.add(new TokenPKCS11("rainbow4", "c:/windows/system32/dkck232.dll"));
		repositories.add(new TokenPKCS11("rainbow5", "c:/windows/system32/iveacryptoki.dll"));
		repositories.add(new TokenPKCS11("aet", "c:/windows/system32/aetpkss1.dll"));

		repositories.add(new TokenPKCS11("siemens", "c:/windows/system32/siecap11.dll"));
		repositories.add(new TokenPKCS11("outros", "c:/windows/system32/fort32.dll"));
		repositories.add(new TokenPKCS11("eracom", "c:/windows/system32/cryptoki.dll"));
		repositories.add(new TokenPKCS11("smarttrust", "c:/windows/system32/smartp11.dll"));
		repositories.add(new TokenPKCS11("gemplus", "c:/windows/system32/pk2priv.dll"));
		repositories.add(new TokenPKCS11("safelayer", "c:/windows/system32/p11card.dll"));
		repositories.add(new TokenPKCS11("spyrus", "c:/windows/system32/SpyPK11.dll"));
		repositories.add(new TokenPKCS11("ibm", "c:/windows/system32/ibmpkcss.dll"));

		repositories.add(new TokenPKCS11("outros", "c:/windows/system32/AuCryptoki2-0.dll"));
		repositories.add(new TokenPKCS11("aloha", "c:/windows/system32/aloaha_pkcs11.dll"));
		repositories.add(new TokenPKCS11("feitain", "c:/windows/system32/EP1PK111.DLL"));
		repositories.add(new TokenPKCS11("utimaco", "c:/windows/system32/pkcs201n.dll"));
		repositories.add(new TokenPKCS11("atrust", "c:/windows/system32/asignp11.dll"));
		repositories.add(new TokenPKCS11("gemplus", "c:/windows/system32/gclib.dll"));
		repositories.add(new TokenPKCS11("schlumberger", "c:/windows/system32/slbck.dll"));
		repositories.add(new TokenPKCS11("mozilla", "c:/windows/system32/softokn3.dll"));
		repositories.add(new TokenPKCS11("opensc", "c:/windows/system32/opensc-pkcs11.dll"));
		repositories.add(new TokenPKCS11("feintain", "c:/windows/system32/ShuttleCsp11_3003.dll"));
		repositories.add(new TokenPKCS11("setec", "c:/windows/system32/SetTokI.dll"));
		repositories.add(new TokenPKCS11("algorithm_research", "c:/windows/system32/sadaptor.dll"));
		repositories.add(new TokenPKCS11("nexus", "c:/windows/system32/nxpkcs11.dll"));
		repositories.add(new TokenPKCS11("outros", "c:/windows/system32/3gp11csp.dll"));
		repositories.add(new TokenPKCS11("ibm", "c:/windows/system32/CccSigIT.dll"));
		repositories.add(new TokenPKCS11("schlumberger", "c:/windows/system32/acpkcs211.dll"));
		repositories.add(new TokenPKCS11("asign", "c:/windows/system32/psepkcs11.dll"));
		repositories.add(new TokenPKCS11("dallas", "c:/windows/system32/dspkcs.dll"));
		repositories.add(new TokenPKCS11("gemplus", "c:/windows/system32/w32pk2ig.dll"));
		repositories.add(new TokenPKCS11("ibm", "c:/windows/system32/csspkcs11.dll"));
		repositories.add(new TokenPKCS11("micardo", "c:/windows/system32/micardoPKCS11.dll"));
		repositories.add(new TokenPKCS11("chrysalis", "c:/windows/system32/cryst32.dll"));
		repositories.add(new TokenPKCS11("feintain", "c:/windows/system32/ngp11v211.dll"));
		repositories.add(new TokenPKCS11("athena", "c:/windows/system32/asepkcs.dll"));
		repositories.add(new TokenPKCS11("feintain", "c:/windows/system32/ep2pk11.dll"));
		repositories.add(new TokenPKCS11("chrysalis", "c:/windows/system32/cryst201.dll"));
		repositories.add(new TokenPKCS11("schlumberger", "c:/windows/system32/acpkcs.dll"));

		repositories.add(new TokenPKCS11("id2", "c:/windows/system32/id2cbox.dll"));
		repositories.add(new TokenPKCS11("ncipher", "c:/windows/system32/cknfast.dll"));
	}

	private static void addDefaultKeyRepositoriesForLinux() {
		repositories.add(new TokenPKCS11("etoken", "/usr/lib/libeTPkcs11.so"));
		repositories.add(new TokenPKCS11("opensc", "/usr/lib/opensc-pkcs11.so"));
		repositories.add(new TokenPKCS11("morpho", "/usr/lib/libaetpkss.so.3.0.2635"));
		repositories.add(new TokenPKCS11("watchdata", "/usr/lib/watchdata/ICP/lib/libwdpkcs_icp.so"));
	}

	/**
	 * Adicionar {@link KeyRepository} extras ao topo da lista de repositório
	 * cuja disponibilidade será verificada ao invocar-se o método
	 * {@link TokenPKCS11Composite#getInstance()}
	 * 
	 * @param repository
	 */
	public static void add(KeyRepository repository) {

		repositories.clear();

		repositories.add(repository);
		addDefaultKeysRepositories();
	}

	/**
	 * Informa erro de acesso ao dispositivo ao usuário.
	 * 
	 * @throws AccessCancelException
	 */
	protected void informAccessErrorToUser() throws AccessCancelException {

		StringBuilder message = new StringBuilder();

		switch (accessErrorCount++) {
		case 0:
			message.append("Por favor verifique o PIN digitado.");
			break;

		case 1:
			message.append("Por favor verifique o PIN digitado.\n");
			message.append("A digitação incorreta do PIN poderá BLOQUEÁ-LO!");
			break;

		case 2:
		default:
			message.append("Não foi possível acessar o dispositivo após " + accessErrorCount + " tentativas.");
			message.append("\nO dispositivo provavelmente foi bloqueado e poderá ser desbloqueado");
			message.append("\nutilizando-se o PUK por meio da aplicação do fabricante do TOKEN, que");
			message.append("\nfoi instalada no sistema operacional pelo suporte técnico.");
		}
		
		

		if (accessErrorCount == TRIES_TO_BLOCK) {
			accessErrorCount = 0;
			AssinadorUtils.exibeInfo(message.toString());
			throw new AccessCancelException("Número de tentativas excedido!");
		}
		
		AssinadorUtils.exibeAlerta(message.toString());

	}

	@Override
	public void init() throws AccessCancelException {
		KeyRepository.pin = null;

		initNewRepository();

		try {
			initLastValidRepository();
		} catch (Exception e) {
			Logger.warn(e.getMessage());		
		}

		atualizarAndamento("Buscando novos dispositivos...");
	}

	private void initNewRepository() throws AccessCancelException {
		Map<Integer, KeyRepository> rep = new HashMap<Integer, KeyRepository>();

		atualizarAndamento("Buscando dispositivos criptográficos...");

		int i = 0;
		for (KeyRepository t : repositories) {

			try {
				t.init();
				rep.put(i++, t);
			} catch (AccessUnavailableException e) {
				continue;
			}

		}

		if (rep.size() == 0) {
			atualizarAndamento("Nenhum repositório foi encontrado!");
			informInitErrorToUser();
			initNewRepository();
			return;
		}

		if (rep.size() == 1) {
			atualizarAndamento("Um repositório foi encontrado.");
			this.repository = rep.get(0);
		} else {
			atualizarAndamento("Mais de um repositório foi encontrado.");
			this.repository = rep.get(chooseRepository(rep));
		}

	}

	private void informInitErrorToUser() throws AccessCancelException {

		initErrorCount++;

		if (initErrorCount > MAX_INIT_TRIES) {
			throw new AccessCancelException("Dispositivo não foi encontrado");
		}

		StringBuilder message = new StringBuilder();

		switch (initErrorCount) {
		case 1:
			message.append("Por favor, desplugue o token e REPLUGUE-O.\n");
			break;

		default:
			message.append("Não foi possível encontrar nenhum dispositivo, queira ACIONAR o suporte técnico para verificar se:");
			message.append("\n\t\t\t a) se os drivers do dispositivo foram instalados corretamente;");
			message.append("\n\t\t\t b) se o token foi certificado digitalmente; e");
			message.append("\n\t\t\t c) se o token não está bloqueado.");
		}

		AssinadorUtils.exibeAlerta("O Dispostivo de Assinatura não pode ser encontrado\n\n" + message.toString());

	}

	@Override
	public void access() throws AccessCancelException {

		defineInstance();

		if (KeyRepository.pin == null) {
			atualizarAndamento("Digite o PIN...");
			askForPin();
		}

		try {
			atualizarAndamento("Acessando dispositivo criptográfico...");
			this.repository.access();

			defineInstance();

			LastRepositoryFactory.create(this.repository).persistTokenValidConfiguration();

			return;
		} catch (AccessUnavailableException e) {
			informAccessErrorToUser();
			KeyRepository.pin = null;
			access();
		}
	}

	private void defineInstance() {
		this.name = this.repository.name;
		this.keystore = this.repository.keystore;
		this.libraryPath = this.repository.libraryPath;
		this.slot = this.repository.slot;
		this.signersList = this.repository.signersList;
		this.signProvider = this.repository.signProvider;
	}

	private void initLastValidRepository() throws AccessCancelException, AccessUnavailableException {
		atualizarAndamento("Buscando configurações de acesso anteriores...");

		KeyRepository lastRepository;
		try {
			LastPKCS11 lastPKCS11 = LastRepositoryFactory.load();
			lastRepository = lastPKCS11.getRepository();
			
			checkIfLastRepositoryEqualsToPluggedToken(lastRepository);
			
			lastRepository.access();
			lastPKCS11.persistTokenValidConfiguration();
			
		} catch (AccessUnavailableException e) {
			e.printStackTrace();
			KeyRepository.pin = null;
			throw new AccessUnavailableException(e.getMessage());
		}

		atualizarAndamento("Acessando dispositivo via configurações anteriores...");
		this.repository = lastRepository;

	}

	private void checkIfLastRepositoryEqualsToPluggedToken(KeyRepository lastRepository)
			throws AccessUnavailableException {
		if(!this.repository.name.equals(lastRepository.name)){
			throw new AccessUnavailableException("Novo token Inserido!");
		}
	}

	@Override
	public void shutdown() {
		this.repository.shutdown();
	}

}
