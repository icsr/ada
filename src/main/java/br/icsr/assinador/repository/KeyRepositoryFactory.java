package br.icsr.assinador.repository;

import br.icsr.assinador.core.Assinador;
import br.icsr.assinador.repository.pkcs11.TokenWindowsMy;
import br.icsr.assinador.repository.pkcs11.exception.AccessUnavailableException;
import br.icsr.util.AssinadorUtils;
import br.icsr.util.Logger;

public class KeyRepositoryFactory {


	public static KeyRepository create(Assinador assinador, boolean pkcs11) throws AccessUnavailableException {
		
		try {
			if (!pkcs11 && AssinadorUtils.isWindows()) {
				return createMSCAPI(assinador);
			}
		} catch (Exception e) {
			Logger.info("WindowsMy/MSCAPI não disponível!");
		}

		return createPKCS(assinador);
		
	}

	public static KeyRepository create(Assinador assinador) throws AccessUnavailableException {

		return create(assinador, true);

	}

	public static KeyRepository createPKCS(Assinador assinador) {

		if (assinador != null) {
			return new TokenPKCS11Composite(assinador);
		} else {
			return new TokenPKCS11Composite();
		}
	}

	public static KeyRepository createMSCAPI(Assinador assinador) throws Exception {
		
		TokenWindowsMy repository;
		if (assinador != null) {
			repository = new TokenWindowsMy(assinador);
		} else {
			repository = new TokenWindowsMy();
		}

		repository.init();
		repository.access();

		return repository;
	}

}
