package br.icsr.assinador.repository.pkcs12;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.Provider;
import java.util.Properties;

import br.icsr.assinador.repository.KeyRepository;
import br.icsr.assinador.repository.pkcs11.exception.AccessCancelException;
import br.icsr.assinador.repository.pkcs11.exception.AccessUnavailableException;
import br.icsr.util.AssinadorUtils;

public class JKSFileRepository extends KeyRepository {

	private FileInputStream libraryStream;
	private final String type = "jks";

	public JKSFileRepository(String name, String jksFilePath,
			Provider signProvider) {
		super();
		this.name = name;
		this.libraryPath = jksFilePath;
		this.signProvider = signProvider;
	}

	public JKSFileRepository(String name, String jksFilePath) {
		this(name, jksFilePath, AssinadorUtils.getBCProvider());
		this.name = name;
		this.libraryPath = jksFilePath;
	}

	@Override
	public void init() throws AccessCancelException,
			AccessUnavailableException {

		try {
			this.keystore = KeyStore.getInstance(type.toUpperCase());
			libraryStream = new FileInputStream(this.libraryPath);
		} catch (Exception e) {
			throw new AccessUnavailableException(e.getMessage());
		}

	}

	@Override
	public void shutdown() {
		try {
			this.libraryStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void access() throws AccessCancelException,
			AccessUnavailableException {

		askForPin();

		try {
			this.keystore.load(libraryStream, null);
			checkAccessibility();
		} catch (Exception e) {
			AssinadorUtils
					.exibeAlerta("O Dispostivo de Assinatura não pode ser acessado.\nPor favor verifique o PIN.");
			
			access();
		}

	}

	public void save() {
		Properties prop = new Properties();
		prop.put("name", this.name);
		prop.put("library", this.libraryPath);

		try {
			prop.store(
					new FileOutputStream(AssinadorUtils.concatWithoutSpaces(
							AssinadorUtils.getAssinadorHome(), File.separator, type,
							File.separator, this.name, ".", type)), null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
