package br.icsr.assinador.cms;

import java.security.Signature;

import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.cms.Attribute;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.asn1.esf.CommitmentTypeIndication;
import org.bouncycastle.asn1.esf.OtherHashAlgAndValue;
import org.bouncycastle.asn1.esf.SignaturePolicyId;
import org.bouncycastle.asn1.esf.SignaturePolicyIdentifier;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.cms.DefaultSignedAttributeTableGenerator;
import org.bouncycastle.cms.jcajce.JcaSignerInfoGeneratorBuilder;

import br.icsr.assinador.politica.SignaturePolicy;

/**
 * Subclasse que implementa método para adicionar atributos assinados.
 * 
 * @author 1T Israel C. S. Rocha (CCASJ)
 * @version 1.0 - 2012
 */
public class DefaultCAdESGenerator extends CAdESGenerator {

	/**
	 * Construtor
	 * 
	 * @param signature
	 *            Realizador da assinatura digital.
	 *            <p>
	 *            Para dispositivos criptográficos o <b>provider</b> será um
	 *            SunPKCS11.
	 */
	public DefaultCAdESGenerator(Signature signature) {
		super(signature);
	}

	@Override
	protected void adicionaAtributosAssinados(
			JcaSignerInfoGeneratorBuilder signerInfoBuilder,
			SignaturePolicy politica, String... outros) {

		SignaturePolicyIdentifier sigPolicy = new SignaturePolicyIdentifier(
				new SignaturePolicyId(politica.getSigPolicyIdentifier(),
						new OtherHashAlgAndValue(new AlgorithmIdentifier(
								politica.getSignPolicyHashAlg()),
								politica.getSignPolicyHash())));

		ASN1EncodableVector atributosAssinados = new ASN1EncodableVector();
		atributosAssinados.add(new Attribute(
				PKCSObjectIdentifiers.id_aa_ets_sigPolicyId, new DERSet(
						sigPolicy)));
		
		if(outros != null && outros.length > 0){
			CommitmentTypeIndication commitment = new CommitmentTypeIndication(new ASN1ObjectIdentifier(outros[0]));
			atributosAssinados.add(new Attribute(
					PKCSObjectIdentifiers.id_aa_ets_commitmentType, new DERSet(
							commitment)));
		}
		
		DefaultSignedAttributeTableGenerator atgen = new DefaultSignedAttributeTableGenerator(
				new AttributeTable(atributosAssinados));
		

		signerInfoBuilder.setSignedAttributeGenerator(atgen);

	}

}
