package br.icsr.assinador.cms;

import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.cert.jcajce.JcaCertStore;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSSignedDataGenerator;
import org.bouncycastle.cms.CMSTypedData;
import org.bouncycastle.cms.SignerInfoGenerator;
import org.bouncycastle.cms.jcajce.JcaSignerInfoGeneratorBuilder;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;
import org.bouncycastle.util.Store;

import br.icsr.assinador.core.bc.PKCS11ContentSignerBuilder;
import br.icsr.assinador.politica.SignaturePolicy;
import br.icsr.util.AssinadorUtils;

/**
 * Gerador de CADES.
 * 
 * @author 1T Israel C. S. Rocha (CCASJ)
 * @version 1.0 - 2012
 */
public abstract class CAdESGenerator {

	/**
	 * Realizador da assinatura.
	 */
	protected final Signature signature;
	private CMSSignedDataGenerator gen;

	/**
	 * Construtor
	 * 
	 * @param signature
	 *            Realizador da assinatura digital.
	 *            <p>
	 *            Para dispositivos criptográficos o <b>provider</b> será um
	 *            SunPKCS11.
	 */
	public CAdESGenerator(Signature signature) {

		this.signature = signature;
		this.gen = new CMSSignedDataGenerator();

	}

	/**
	 * Método para adicionar atributos a serem
	 * assinados em conformidade com as normas da ICP-Brasil.
	 * @see <a href="http://www.iti.gov.br/images/twiki/URL/pub/Certificacao/DocIcp/docs13082012/DOC-ICP-15.03_-_Versao_6.1_2.pdf">REQUISITOS PARA GERAÇÃO E VERIFICAÇÃO
DE ASSINATURAS DIGITAIS NA ICP-BRASIL</a>
	 * 
	 * @param signerInfoBuilder
	 *            Builder de informações do assinante.
	 */
	protected abstract void adicionaAtributosAssinados(
			JcaSignerInfoGeneratorBuilder signerInfoBuilder,
			SignaturePolicy politica, String... outros);

	/**
	 * Adiciona assinante.
	 * 
	 * @param certificado
	 *            Certificado do assinante.
	 * @param chavePrivada
	 *            Referência a sua chave privada.
	 * @param cadeiaCertificados
	 *            Cadeia de Certificados.
	 * @throws OperatorCreationException
	 *             Se falhou adição.
	 * @throws CertificateEncodingException
	 *             Se certificado codificado incorretamente.
	 */
	public void adicionarAssinante(X509Certificate certificado,
			PrivateKey chavePrivada, X509Certificate[] cadeiaCertificados,
			SignaturePolicy politicaAssinatura, String... outros)
			throws OperatorCreationException, CertificateEncodingException {
		
		JcaContentSignerBuilder jcaContentSignerBuilder = new PKCS11ContentSignerBuilder(
				this.signature).setProvider(AssinadorUtils.getBCProvider());
		
		ContentSigner shaSigner = jcaContentSignerBuilder.setProvider(AssinadorUtils.getBCProvider()).build(chavePrivada);
		
		JcaSignerInfoGeneratorBuilder signerInfoBuilder = new JcaSignerInfoGeneratorBuilder(
				new JcaDigestCalculatorProviderBuilder().build());
		
		adicionaAtributosAssinados(signerInfoBuilder, politicaAssinatura,
				outros);

		SignerInfoGenerator signerInfoGen = signerInfoBuilder.build(shaSigner,
				certificado);
		gen.addSignerInfoGenerator(signerInfoGen);

		List<X509Certificate> certList = new ArrayList<X509Certificate>();

		for (X509Certificate c : cadeiaCertificados) {
			certList.add(c);
		}

		Store certs;
		try {
			certs = new JcaCertStore(certList);

			gen.addCertificates(certs);

		} catch (CertificateEncodingException e) {
			e.printStackTrace();
		} catch (CMSException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Gera o CADES assinando-o digitalmente.
	 * 
	 * @param conteudoAAssinar
	 *            conteúdo a assinar digitalmente
	 * @return CMSSignedData (CADES).
	 */
	public CMSSignedData gerar(byte[] conteudoAAssinar) {
		System.out.println("Gerando CADES pelo algoritmo "
				+ this.signature.getAlgorithm() + ".");

		CMSTypedData msg = new CMSProcessableByteArray(conteudoAAssinar);
		try {
			CMSSignedData signedData = gen.generate(
					PKCSObjectIdentifiers.signedData.getId(), msg, true,
					this.signature.getProvider(), true);
			System.out.println("Assinatura gerada corretamente!");
			return signedData;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (CMSException e) {
			e.printStackTrace();
		}

		return null;
	}

}
