package br.icsr.assinador.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Random;

import javax.crypto.spec.SecretKeySpec;

import com.itextpdf.text.pdf.codec.Base64;

import br.icsr.util.AssinadorUtils;
import br.icsr.util.Logger;

public class Cipher {

	private static javax.crypto.Cipher cipher;

	private static String keyStoreBase64 = "AAAAAgAAABSESKxtH3iYexv1qya/7AEzKk6uCAAAB88EAAlhZGNjb21hZXIAAAFQ17YISgAAAAAA\nAAA8AAAAFIHhbNnUpceaTxIgMXL6BRxYu6+0AAAGcwoGWDJndP1nMRT8i1Wm36810jGylVm757us\nCbcsk5ldAAKNAaBK/3II2soVNXVI8GpGPXHS";

	private static final String password = "@DC@PP20151C$RCC@$J";
	private static final String keyAlias = "adccomaer";

	private static final int RANDOM_SIZE = Integer.parseInt(new String(Base64.decode("OTY3")));
	private static final Random random = new Random();

	static {
		initCipher();
	}

	private static KeyStore getKeyStore() {

		KeyStore store = null;
		try {
			store = KeyStore.getInstance("bks", AssinadorUtils.getBCProvider());
			ByteArrayInputStream in = new ByteArrayInputStream(Base64.decode(keyStoreBase64));
			store.load(in, password.toCharArray());
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return store;

	}

	private static void initCipher() {
		
		if(cipher != null){
			return;
		}
		
		try {
			cipher = javax.crypto.Cipher.getInstance("AES", AssinadorUtils.getBCProvider());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static byte[] decrypt(byte[] encryptedText) throws Exception {
		Logger.info("Descriptografando conteúdo...");

		cipher.init(javax.crypto.Cipher.DECRYPT_MODE, getKey());

		return unmask(cipher.doFinal(encryptedText));
	}



	public static byte[] encrypt(byte[] plainText) throws Exception {
		Logger.info("Criptografando conteúdo...");
		cipher.init(javax.crypto.Cipher.ENCRYPT_MODE, getKey());
		return cipher.doFinal(mask(plainText));
	}
	
	private static byte[] mask(byte[] plainText) {
		return mask(new String(plainText)).getBytes();
	}
	
	private static String mask(String plainText) {
		
		StringBuilder str = new StringBuilder();
		for(int i = 0; i < RANDOM_SIZE; i++){
			str.append(ALFA_NUMBERS[random.nextInt(ALFA_NUMBERS.length - 1)]);
		}
		str.append(plainText);
		return str.toString();
	}

	private static byte[] unmask(byte[] encryptedText) {
		return new String(encryptedText).substring(RANDOM_SIZE).getBytes();
	}
	

	private static Key getKey() throws UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException,
			CertificateException, IOException {
		return getKeyStore().getKey(keyAlias, null);
	}

	static String generateNewKeyStore() throws NoSuchAlgorithmException, CertificateException, IOException,
			KeyStoreException, UnrecoverableKeyException {
		SecureRandom random = new SecureRandom(password.getBytes());
		byte[] keyBytes = new byte[javax.crypto.Cipher.getMaxAllowedKeyLength("AES")/8];
		random.nextBytes(mask(keyBytes));

		SecretKeySpec key = new SecretKeySpec(mask(keyBytes), "AES");

		KeyStore store = KeyStore.getInstance("bks", AssinadorUtils.getBCProvider());
		store.load(null, password.toCharArray());
		store.setKeyEntry(keyAlias, key, null, null);


		ByteArrayOutputStream out = new ByteArrayOutputStream();

		store.store(out, password.toCharArray());

		byte[] bout = out.toByteArray();
		out.close();

		return Base64.encodeBytes(mask(bout));

	}
	
	public static void main(String[] args) {
		try {
			System.out.println(generateNewKeyStore());
		} catch (UnrecoverableKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static byte[] encrypt(char[] charArray) throws Exception {
		
		return encrypt(new String(charArray).getBytes());
	}
	
	
	private static String[] ALFA_NUMBERS = {"a","b","c","d","e","f","g","h","i","j",
            "k","l","m","n","o","p","q","r","s","t",
            "u","v","w","x","y","z","0","1","2","3",
            "4","5","6","7","8","9"};

}
