package br.icsr.assinador.assinante;

import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;

/**
 * Classe que representa um assinante de documento encapsulando sua chave
 * privada ({@link PrivateKey}), seu certificado digital ({@link X509Certificate}) e a cadeia de certificados.
 * 
 * @author 1T Israel C. S. Rocha (CCASJ)
 * @version 1.0 - 2012
 */
public class Assinante {

    private String alias;
    private PrivateKey privateKey;
    private X509Certificate certificado;
    private X509Certificate[] cadeia;

    /**
     * Contrutor
     * @param privateKey referência para a chave privada do assinante. 
     * @param certificadoAssinante certificado digital do assinante.
     * @param cadeiaCertificados cadeia de certificados do certificado do assinante
     * @param alias Apelido que identifica o assinante.
     */
    public Assinante(PrivateKey privateKey, X509Certificate certificadoAssinante, Certificate[] cadeiaCertificados, String alias) {
	this.privateKey = privateKey;
	this.certificado = certificadoAssinante;

	this.cadeia = new X509Certificate[cadeiaCertificados.length];
	for (int i = 0; i < cadeiaCertificados.length; i++) {
	    this.cadeia[i] = (X509Certificate) cadeiaCertificados[i];
	}

	this.alias = alias;
    }

    /**
     * Getter
     * @return o alias do assinante.
     */
    public String getAlias() {
        return alias;
    }

    /**
     * Getter
     * @return referência para chave privada do assinante.
     */
    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    /**
     * Getter
     * @return certificado digital do assinante.
     */
    public X509Certificate getCertificado() {
        return certificado;
    }

    /**
     * Getter
     * @return cadeia de certificação do certificado do assinante.
     */
    public X509Certificate[] getCadeia() {
        return cadeia;
    }
    
    public String extractName(){
    	return extractName(this.getCertificado());
    }

	public static String extractName(X509Certificate c) {		
		String name = c.getSubjectDN().getName();
		return name.substring(name.toLowerCase().indexOf("=")+1, name.indexOf(","));
	}

}
