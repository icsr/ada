package br.icsr.assinador.addon;

import java.io.File;
import java.io.FileNotFoundException;

import br.icsr.assinador.core.Assinador;
import br.icsr.assinador.core.AssinadorBuilder;
import br.icsr.assinador.core.exception.ParametroNaoDefinidoException;
import br.icsr.assinador.core.observer.StatusObserver;
import br.icsr.assinador.politica.SignaturePolicy;
import br.icsr.assinador.repository.gui.StatusDialog;
import br.icsr.util.AssinadorUtils;
import br.icsr.util.Logger;

public class AssinadorAddon {

	public static final String CONTENT_FILEPATH = System.getProperty("java.io.tmpdir") + File.separatorChar
			+ "addon_content";

	public static StatusDialog status;

	static boolean exitAtEnd = true;

	public static void main(String[] args) {

		System.out.println(AssinadorUtils.concatWithoutSpaces("Iniciando ", AssinadorUtils.APP_TITLE, "..."));

		try {

			String compromissoOID = extractArguments("commitments", args, true);
			String conteudoPath = extractArguments("contentsFilepath", args, true);
			String accessTokenMethod = extractArguments("tokenAccessMethod", args, false);
			String assinadorHome = extractArguments("assinadorHome", args, false);

			if (assinadorHome != null) {
				AssinadorUtils.setAssinadorHome(assinadorHome);
			}

			checkFile(conteudoPath);

			initGraphics();

			Assinador assinador = new AssinadorBuilder(new SignaturePolicy(AssinadorUtils.politicaAssinaturaDER),
					compromissoOID, AssinadorUtils.lerConteudo(conteudoPath))
							.withStatusObserver(new StatusObserver(status)).build();

			assinador.iniciar(withPKCS11(accessTokenMethod));
			AssinadorUtils.tic();
			AssinadorUtils.salvarConteudo(conteudoPath, assinador.assinar());
			Logger.info("Tempo para assinar via Assinador Addon: ", AssinadorUtils.toc(), "ms.");

			assinador.finalizar();
			assinador = null;
		} catch (Exception e) {
			e.printStackTrace();
			Logger.error(e.getMessage());
		}
		
		finishGraphics();
		finishThread();

	}

	private static String extractArguments(String name, String[] args, boolean mandatory)
			throws ParametroNaoDefinidoException {
		System.out.println(AssinadorUtils.concatWithoutSpaces("Extraindo argumento ",
				(mandatory ? "obrigatório " : "opcional "), name, "..."));

		for (int i = 0; i < args.length; i++) {
			System.out.println("Scanning " + args[i]);
			if (args[i].equalsIgnoreCase("-" + name)) {
				System.out.println(AssinadorUtils.concatWithoutSpaces("Encontrado argumento nome:", name, ", value: ",
						args[i + 1]));
				return args[i + 1];
			}
		}

		if (mandatory) {
			showHelp();
			throw new ParametroNaoDefinidoException("O atributo obrigatório e ausente: " + name);
		}

		return null;

	}

	private static void showHelp() {
		AssinadorUtils.showLicense();

		System.out.println("\tInvoque o Assinador Addon da seguinte forma: \n");
		System.out.println("\tjava -cp ./ -jar assinador-digital-icsr-core.jar [parâmetros]\n\n");
		System.out.println("\t\tParâmetros:");
		System.out.println("\t\t\t-commitments [compromisso OID (obrigatório)]");
		System.out.println("\t\t\t-contentsFilepath [filepath para conteúdo a ser assinado (obrigatório)]");
		System.out.println("\t\t\t-tokenAccessMethod [1 para pcks11 (default), 2 para windowsMy]");
		System.out.println("\t\t\t-assinadorHome [pasta para guarda dos logs e caches (default: pasta home do usuário)]");
		
	}

	private static boolean withPKCS11(String accessTokenMethod) {

		return accessTokenMethod == null || !accessTokenMethod.equals("2");

	}

	private static void finishThread() {
		try {
			Thread.currentThread().interrupt();
		} catch (Exception e) {
		} finally {
		}

		Logger.info("Finalizando Aplicação.");

		if (exitAtEnd) {
			System.exit(0);
		}
	}

	private static void finishGraphics() {
		try {
			status.dispose();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void initGraphics() {
		status = new StatusDialog();
		status.exibir();
	}

	private static void checkFile(String... argumentos) throws Exception {
		for (String argumento : argumentos) {
			File file = new File(argumento);
			if (!file.exists() || file.isDirectory()) {
				throw new FileNotFoundException("Não encontrado arquivo " + file.getCanonicalPath());

			}
		}
	}

}
