package br.icsr.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.security.Provider;
import java.security.Security;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

import javax.swing.JOptionPane;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import com.itextpdf.text.pdf.PdfReader;

import br.icsr.assinador.core.Assinador;
import br.icsr.assinador.core.exception.AssinaturaException;
import br.icsr.assinador.repository.gui.ChooseOptionDialog;
import br.icsr.assinador.repository.pkcs11.exception.AccessCancelException;

public class AssinadorUtils {

	/**
	 * Título do aplicativo.
	 */
	public static final String APP_TITLE = "Assinador Digital ICSR";
	public static final String APP_SHORT_TITLE = "Assinador Digital ICSR";

	/**
	 * Licença simplificada do aplicativo.
	 */
	public static final String LICENSE_SHORT = "Propriedade de Israel Cordeiro Rocha:093.900.827-06";

	public static final String LICENSE_FULL;

	static {

		LICENSE_FULL = concatWithSpaces("\n\n\t\t",APP_TITLE.toUpperCase(),"\n\t", 
				LICENSE_SHORT, "\n\t\t", "israel.c.rocha@gmail.com","\n\n");

		checkExpiration();
	}

	private final static void checkExpiration() {
		Calendar cal = Calendar.getInstance();
		cal.set(2016, 4, 20);

		if (Calendar.getInstance().after(cal)) {
			exibeInfo("Esta versão de avaliação expirou e não pode mais ser usada!");
			System.exit(0);
		} else {
			exibeInfo(LICENSE_FULL);
		}
	}

	/**
	 * OIDs de compromisso da assinatura digital
	 * 
	 * @see <a href=
	 *      "http://www.jusbrasil.com.br/diarios/425865/pg-25-secao-1-diario-oficial-da-uniao-dou-de-13-01-2009">
	 *      DOU 13/01/2009</a>
	 */
	public static final String COMPROMISSO_CONCORDANCIA = "2.16.76.1.8.1";
	public static final String COMPROMISSO_AUTORIZACAO = "2.16.76.1.8.2";
	public static final String COMPROMISSO_TESTEMUNHO = "2.16.76.1.8.3";
	public static final String COMPROMISSO_AUTORIA = "2.16.76.1.8.4";
	public static final String COMPROMISSO_CONFERENCIA = "2.16.76.1.8.5";
	public static final String COMPROMISSO_REVISAO = "2.16.76.1.8.6";
	public static final String COMPROMISSO_CIENCIA = "2.16.76.1.8.7";
	public static final String COMPROMISSO_PUBLICACAO = "2.16.76.1.8.8";
	public static final String COMPROMISSO_PROTOCOLO = "2.16.76.1.8.9";
	public static final String COMPROMISSO_INTEGRIDADE = "2.16.76.1.8.10";
	public static final String COMPROMISSO_AUTENTICACAO = "2.16.76.1.8.11";
	public static final String COMPROMISSO_TESTE = "2.16.76.1.8.12";

	private static final BouncyCastleProvider provider = new BouncyCastleProvider();

	public static Provider getBCProvider() {
		addProvider(provider);
		return provider;
	}

	/**
	 * Separador que pode ser utilizado para separar vários conteúdos
	 * codificados em base64 e compromissos de assinatura para serem enviados ao
	 * applet e assinados simultaneamente.
	 * 
	 * @see {@link Assinador#setCompromissoOID(String)}
	 * @see {@link Assinador#assinar(int, String)
	 * 
	 */
	public static final String SEPARADOR_OBJETOS = "_NEWOBJ_";

	/**
	 * Máximo número de caracteres suportado pelo JAVASCRIPT.
	 */
	public static final int MAX_JS_LENGTH = 1000000;

	/**
	 * Diretório temporário da aplicação.
	 */
	public static String ASSINADOR_HOME;

	public static void setAssinadorHome(String filepath) {
		File home = new File(filepath);
		if (!home.exists()) {
			home.mkdirs();
		}

		ASSINADOR_HOME = filepath + File.separator;
	}

	public static String getAssinadorHome() {
		return ASSINADOR_HOME != null ? ASSINADOR_HOME
				: concatWithoutSpaces(System.getProperty("java.io.tmpdir"), File.separator);
	}

	private static final StringBuilder LINE = new StringBuilder();

	static {

		for (int i = 0; i < 100; i++) {
			LINE.append("-");
		}

	}

	public static final String READY_MESSAGE = "O " + AssinadorUtils.APP_SHORT_TITLE + " está pronto!";

	/**
	 * Identifica se Sistema Operacional é Windows(R).
	 * 
	 * @return
	 */
	public static boolean isWindows() {
		try {
			final int indexOf = System.getProperty("os.name").toLowerCase().indexOf("win");

			return indexOf >= 0;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;

	}

	/**
	 * Identifica se sistema operacional é Linux.
	 * 
	 * @return
	 */
	public static boolean isLinux() {
		return System.getProperty("os.name").toLowerCase().indexOf("linux") >= 0;
	}

	/**
	 * Identifica se sistema operacional é 64bits
	 * 
	 * @return
	 */
	public static boolean isSO64bits() {
		return System.getProperty("os.arch").indexOf("64") >= 0;
	}

	/**
	 * Identifica se sistema operacional é 34bits
	 * 
	 * @return
	 */
	public static boolean isSO32bits() {
		return !isSO64bits();
	}

	/**
	 * Identifica se JVM é 64 bits.
	 * 
	 * @return true se 64 bits.
	 */
	public static boolean isJVM64bits() {
		return System.getProperty("sun.arch.data.model").indexOf("64") >= 0;
	}

	/**
	 * Identifica se JVM é 32 bits.
	 * 
	 * @return true se 32 bits.
	 */
	public static boolean isJVM32bits() {
		return !isJVM64bits();
	}

	/**
	 * Exibe janela de alerta para informar algo ao usuário
	 * 
	 * @param message
	 *            Mensagem a ser exibida.
	 * @return
	 * @throws AccessCancelException
	 */
	public static void exibeAlerta(String message) throws AccessCancelException {
		exibeConfirmacao(message, JOptionPane.ERROR_MESSAGE);
	}

	public static void exibeAlerta(String message, int messageType, int optionType) throws AccessCancelException {

		if (optionType == JOptionPane.OK_CANCEL_OPTION) {
			exibeConfirmacao(message, messageType);
			return;
		}

		JOptionPane.showMessageDialog(null, message, AssinadorUtils.APP_SHORT_TITLE, messageType);
	}

	public static void exibeInfo(String message) {
		JOptionPane.showMessageDialog(null, message, AssinadorUtils.APP_SHORT_TITLE, JOptionPane.INFORMATION_MESSAGE);
	}

	public static void exibeErro(String message) throws AssinaturaException, AccessCancelException {
		exibeAlerta(message);
	}

	private static void exibeConfirmacao(String message, int messageType) throws AccessCancelException {

		Logger.warn(message);
		if (JOptionPane.showConfirmDialog(null, message, AssinadorUtils.APP_SHORT_TITLE, JOptionPane.OK_CANCEL_OPTION,
				messageType) == JOptionPane.CANCEL_OPTION) {
			throw new AccessCancelException("Operação cancelada pelo usuário.");
		}
	}

	public static int selectOption(String message, Set<?> acceptableOptions) throws AccessCancelException {

		int op;
		while (true) {
			// TODO PRECISA REFATORAR!
			String input = new ChooseOptionDialog().askForOption(message);

			if (input == null) {
				throw new AccessCancelException("Operação Cancelada pelo Usuário.");
			}

			try {
				op = Integer.parseInt(input);
			} catch (NumberFormatException e) {
				continue;
			}

			if (!acceptableOptions.contains(op)) {
				continue;
			}

			break;

		}

		return op;
	}

	/**
	 * Verifica se conteúdo codificado em
	 * <a href="http://pt.wikipedia.org/wiki/Base64">Base64</a> é um PDF ou
	 * <a href="http://pt.wikipedia.org/wiki/PAdES">PADES</a>.
	 * 
	 * @param conteudoBase64
	 *            conteúdo codificado em
	 *            <a href="http://pt.wikipedia.org/wiki/Base64">Base64</a>
	 * @return verdadeiro se conteúdo é PDF ou PADES.
	 */
	public static boolean isPADES(String conteudoBase64) {
		PdfReader pdfReader;
		try {
			pdfReader = new PdfReader(Base64.decode(conteudoBase64));
			pdfReader.getMetadata();
			pdfReader.close();

			return pdfReader.getAcroFields().getSignatureNames().size() > 0;

		} catch (Exception e) {
			return false;
		}
	}

	public static boolean isPDF(String conteudoBase64) {
		PdfReader pdfReader;
		try {
			pdfReader = new PdfReader(Base64.decode(conteudoBase64));
			pdfReader.getMetadata();
			pdfReader.close();

			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Imagem PNG do Token criptográfico.
	 */
	public static final byte[] LOGO = Base64.decode(
			"iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AQZFiEgrVvlHgAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAACAASURBVHjazZt3lBzllfZ/FTrH6e7JWTOa0UgaZY2EIgIRhEReorGNDTbG2dg47q5t1uAEeDHGAXttgjHRBAFCQoAACaRRGmVNzqlnejrnrvD9MS2MbXmNbZbzvX3qVPc5fd6q96n73vvc594SeP+HKX82AsL7OG82f87kz3p+fv1fmfT9uEFjftFOwA2UAd78YQDk9+E6KhAHgoA/fwSBFJAGtH8WiH/lxuyAB4FFBq9jbcviFc11jTNn+hY2e34/9rop3nZUxGgQLL4S1KYS4bNnfJifP/QjrGNJKs5fzfHntmB1OXALHpREmkydgeHbnwcRFn3lY7Q/+gKOunLqrlynH379Ld1QZNHMKUcm0XosFB8cGyOZO6Yr2j7gMNCbByiVB+M9D/mffOIVwBmIwtfR9ZnGhbXGyqd/IGRVgWOhUdLbQTAYMc+vQDY7SWZiVDfPxrNwNpa6HBULm+nv78RnsmMKSuBQsTb4GOZ50KBmViPdvEQ4HKT92CHBt3ox0aF+qemKcw2J61bbe7furnSWFy2NHuq4NvTQrg5VUV8BtgDtwGTeYrT/CwDsLpdrmWd9yxcn0hMbEy8eFgBUq5ld8ZMUil4yuTi6ppIbCKObDBisYXLROOHwGGaXHUeZF7PJSi6XJWtQMesiiqYxr7yRnfmLDAbHUXQVXdfAKhP3T2Is9eGPjOM2WRGdBkxVRcLZ5603T15w3vxDv3imKbzjwDpN0/4IPAMM5H2F+n4CYKei/KL/+c2v75k4d43vxKFX+NnOyyGrMetb13Oz+yyuNBZxr2uce8v3oBW2Y51dQuaNQUxqhtt+/V08SRnT3CZiTg/Eo5QvX82+h56g3F3JL678/DsXsggymtvEZ3/0Q/rtKWJOD1pwCvPMKhpkO527DqEaBYwOF3jt3PriZuPQ5pcX3n/tDZWKotQCDwBHgSSg/G+LEv+B/b6m5cE7fiGcu85nz6VxNc0FiwSyiKJq3JPoI4zOhJZFycTJjofRFAXbvCowicxdfzYNG9ZROn8xdU4vFLkorKrCLJgptfog96eHZXQ6kFWR+39+N8aKSvoGjhLRE4y2H+TZt59GMeQIjo7Rm5pk47JLaZStrL/8cmH+H75VCFwJfBqYB1gB6V8FwAjUmK4/41dXn7XJWYSEzWCBRAIyKsRVInuPgCThRKTM5EPUDYgOE1pWITU0RvbIOINbd7Lv148y8rOHCIXCqFNJjj31LGXuYuJGDUx/MkZvdQ1CSsGtmRg+uJtgRwdJIxhyErqm4a6s5MNXfpaNNZdRay5ERMQqGhCcFiwfb3GKBsMG4HKgNh+h/qazl94DABbge+rA5BrX+iYqvVXc2fcMm2/9NrkDQwAkh8dJL3eSslp55K3Hie7YQ2bbSXK9QVIHBpFsRlKDE4h9EQa2ttLT3Uuud5zMaJjhwydJjkdIDo6BOu23TLEM/u4+MlMREn1+sokE9iUzqFm6idqKSq5ddCFnWKpoV2P8bmQrTbYqCiQzr/a+zcJNlzDy4lumbDBSkPcFg+8Klf+UD6gFNpDVeeLpBzA2ribh9mKdXUNCbwWg/oefQq5fwU9/eAe5+1/TtUgsB0S1QDICpLKRtE5PQD/FYGKvtUE+ZgEEGf+zCx589qV3vieGJgTAGnlol2vsqs0FTbd9VlpsquTx7BGOtr1BziRgKBUBgUQ6TvfJVio+caF48mv3VQFn5H1BJO8L9H8UACPQgIgXVeW2a7/JufZyXhEVop//Gj/65XMwmkYRVXpv+ArZ59s0YAJ4E3gL6Mw7oty/yNisQHnk8bfPOawpl0u3fsSaETWMkRTu2jqMokgKBcWQw+AtYfXyqzj5tfvMwJw8Mev+W1Hh7wNgEGchCBJzC9GbSnksOcxyg4dtYhx5bg3KaDvBba0IR8YAYsCLwCNAFzD1t5D/J/nH3tSTe5WRy5d/rHbWQuQ55UxMDjOlgx0DomAl3jfE/uSbp0heSZ6zWPJW8E84QUEoBCCSIZuK0JaZwA64zQ6Uw/3TJvzbHWQGx3VgHNiXX7w/v/eU9+lIAiPAvoryRo4++QxpQaOufhEBoB2F0KEuvAU+VtasPAWAOU/R/6Yj/PsA6JjQgYEIPfuO8CFLBQcDx3jo1i+CPz1tRoUOBLNRz2/rQP6s5p/8+/H0350QRUZSftZ8+WsosQiB4BjzEFkA2JpKGBse5K1DL7zbwo3vykeEf9wJGkRJMMnooRQv/Op3HGmsJDzZjlTheucvykgI0sqppEX5V5KT9zKmnn2To5EMvllzkEQNBQ0RCZPDiTEt0b9rz19GOvGf5gGCLGHwOSm6eT1z7v0Gt3gWsbx6JZ6KOgylbqwzinF9ad0pbAU+gCG6LARf3c/x79yDzeFGRCBNjnB7Jwavl1w0+t7n+nt/kBxWZj/6DVwXtTB4+y+JhAdR4qMUNDdS8ZtPYjujgfiBATBIfFBDV1WMgkxsZycDHccwCCIaAiSzTOzaQ6Z94v0DQI0l6LrvD/Td/SRj+47RNt6OGAkweeQAfTf9kvCJXuwrqhHM8gcGgGy34Z3XhJ5TGdi7C0VT0XQFJZagoKISy+zi9y8d1hUNo8+B5LUSfbWDQC6CNhFlaMtOCq5chJrKEN8/DIr2gQEQeaIVFk6BpqFFUuRECRUdI1aUUJRM2whYZEgp/7oFiBYDkV3d2BaUQTjNa3f+hoPpQco/eT7h1zoQvXbUo2PoHyAAKCqZ7lFIKSQfO8A3Dv6cz/z0ZhgIYxdMSJL4nhb/3gAwyUgmGWUqQdGi2Vx506e5uvky0sEgJZeuRZ2KIxjkd3j8BzGKvngRarkV6+wytECC1Eg3cokDU305XU/vILG7D8lnf38AEEQR+1n1pDsmmXP+SraNvk2guARLSsG+vJ7MwWH0RAYk8QMDIJ2KYFtUj6HSBZqOMhZCVjTikSl0WUdwmBGs8vsDgJZViL/cSWJXHwanmVw8QSQdxFtay+iPN5PtmUKuKjgFgP4BrF/TLDoKCpZF9YCOrutET3Yj2Cw4ls/CVOZBko3/khM05lmXUcupyHYj1sUVDHX187kbPk1Z2snXf/AYlMgYK93kJmPvFlmFPPU0/6MC5XvMB8y5kRCyIBPZ2g4GGdFsATVMsq0T25wZSEYTybHgqfuR3iXX5/4eAMa8+lORz6IqRF2oEySZ3HCEjgOv89P2LuK1dmbecBHddz2AZJLRQ2lI5QSgADgXqHlXtvt+DgOwQJ1Koo7HMS4oITUQQLIYKaqpJu4tJt3WS3YsgIiIOr2eucD5+bS4FwjnEyMN0OW/kL2WGOuKP33L9757tqes0vZC/wHD/tatgsXmJBePQNcEOVXhlg99mp1yHF0EwWiASIpZ37lO+NXHv1O7Vch9qkiH//rlN8ipEXI7e8n1TCGWODA1FhN/4tCfrchZU4KhrIDGmy9nz633oI3HmP/9m+n4zTP4zluCa3UTx598nrqbLqKlZBl/vPsHCDYDYpUX5cQ4wrIKbj73CzhEIz958R6y2SxKOkv5pzcyfPfT9nO33b3x5QtvPU+ymCbVqeQe4HngdWAMyJ3yAXbBZvo3163rnnNvmHPF5DmNntsS200LV5whWufMEEpqZpLYM4ihpIArb/oEVY4ilntrUQXI7B+CAis3rbuKbzimmFVWI8wsnyFQZhGsS2oEucghSD6bIBfZBcljmd4i73wEIdo/Lky9fVJQtJSAqgqAoFmNAoomTLV3CYuXrBaW3Hi9cFXTZULridcFRqKCuncYg9eOaWUDl/7790g4yzmWSBFtbcdoNIOmw8AUiBLxREwwr6o3CBZTme3iBZcA/w5cDTTwLiGu3Lqx8XZsRqcoihhtTmSzjqOkEofJy9BQB66LZhPdPcADD95Hy50PsvO5h+FkYNrt5RTMRpleJUaRYOCR1CBKOIcsQGJXL5LHBlmF7FD4XUUtAUEWQBfRTSJZk4oeyoAA6UwKDcgdGuTxj9+CGTMnDA+g1bkQix1kUglqVq9hzfzLOMdVQkrNMWKz8/JZTUS3HUcXBcZ3H8FVWYKvcQ7pN7qY818foXvbW6Ln/CUzglv3fzSfrk+JedOfi8daqk3ESFnAMhlinn0W3cf2MvnqHlRVJz0SwTanBCGRY2h0gIP3PQbBNGgqxLK81Xmc6yQHg2qU49EuXIUlyGVOHGfORBF1GhacgdNbjGCSESQBBB1EEV3TIKnQvXUXujot2Pi7TqJm0qBDdjJKtHsAmgtRBkMIZXZue/hJblt7CyazhQPJoXyuqxHf04WxtgSxzIEyHiUyFeD4tm2Q0UjrOno0i3lOqQBUAsuAEinv+FYYGos2qeNRBKPEiZyf+OQYo1VuzNVOfKKPWDqIrIE+Gmf/th2IVhmlcxLU6cjXU5DFb4vT0nAGksHJmO5HiKUYf3wnTeecwb33PMzbE8cYf243CALoAug6gkFEcpjQ0znU8fi0Zy53ke0NoBsBlxEhmkOLpJCrPfiuW8k1dRfwi80/Y8sf78ff5GCFvYFwOsbeycNoBplsxyiCIEBGwVBRgBzKUXT9OkZ/t53EwDh6MCXk1apjp3yAlBsMIdpMZHsDzHBWcOGyS5hfOouJ3i4qVjZjKnOR7Jsi2x1Eml+InlLAKIEkIBhEZKsNy9wZVIoO3JKF8bEOcrNcOBbVMxqa5FdvPozF6UCwGxEMElKRDRAQvTZEhxXZ50KQRRAgNxHBvWY2Xzy4lc+9/QJ6mRU9lkWPpJj8yTauXjOfHT/9NYkTvUjqdGTTBZ3MRBh1JITqj0E8gyCIhA50EBkcJtB6HPdZ8/Ces/AU/7EApneIkGQ3TWd0U2lmVDZx/x/+G59sw+0sY6JnCKU3iCBOV6MFINsfREDA0FSMrulYyzwsNM2gXDBSjI5olCGQQs/miHYMYyx2o0SjmFqqMDWXTJ8XlGGaX4pjxSzsTbWIXguCJKFLsPYTH+aRh+6ls+cY5auXgkUm1zZKrncKKT29g9SxGKJRQgQ0VQGrBDJIs4rQBQHrypkY3A6kqgIyo0GcGxegKvq7OYv4DgBqJIWeyqGGEmiCCiaJrJIhk4zRfXwP2VQG97IGTPVe4q/3YF1ejVTnQfPHMC2uIDY+zi6mAHhCTeL0FVM5czGOpmosy2rZf3IPo9kA2YMj2C6ej9IdwLawlszuIUxVHoKP78JQ7AaDgJbKkYlHKWluoMRXTErIQkZBkCXIaqBNbx09lUOJp0loGvFMFilnxOj2oncHkIrtxF84TOKlo6jjUXw11STf6kaPJk9PhU1FTgRZRB2L0tffxZlnbsKfCGMtKKTAV04unMTgc6FG0ohGGclrRfRY0eNZ9GQO9YSf3MS0EHGF7CArSAgqjO89hNRcwfxMJXOWn4kWTmFrqCJ7cgItFEcNJ7GV+VDDKVR/DD2tIARSbL/rfkqTVjqf3UHw929ANIMeTqKPRdG6AqgHRyGVY/JYN326ymg0TvJAD7kDfajdU+SO+9+JOHavh8xUmFwgRqpr5PRM0LywguhLJxBSCocffxpF1Si/9Tomjxxh3Q3XQ4mR6I521EQW1+XNpLsDZN+aVoWzx8fxXLoW1WLiGXK4JAdnlDSxLz5E3Vc+hJTI8pUzr+AHbS+BCKOf/j2AFv5jmwboI3e+IOqqKhpsZiGnR1GGwgjBJK/cfh9iqQNBNiBYDWiRNIJFAp8VwSCha1DaVEeDIKLbTeAyIMimad+U/VMJwFRgh1SW6OFumIif3gJEuxlTYxG6QaCisIqzvnQzCyub0SSNXCJD6JFWstk0KBpykR1lIPzODrKe2UhRWSH3Vl/Cp5AZRcWUyiJHE2SDMSZe3Mu5H72cF877nI5GRpmKDQO7gCeA36WGJ/+Aqu9M9k6MIAiKo7kasc7Hh772VWZdfwm6oqJnVARZwnPNWWCSMM8oQZdAiKQQBRHdbMa3vIV4zyAzPnv1tD+QBGSfk8jkFKHOUZq+8iHcFyz6q9qgEVigquoFsstMZv8wLZ+/kdcPbqOhaCaWVcvITAWIWRK4i8vR57iJPXEY26Jy0geGsc2qoOann2Bur45z9QasSCwXZH44uAO5wEH4RAehH24hfWxIVVV1DHgJeDhfPNkKvAZsB14F9gJxk9NRrgiqPRSLkvMYSQYCmOaUkR0KUvK969ACMVSHROmGFqxWG2fXr2Q4F2CktoBUapKqTespbWhidMsuqn56PaEn9tD4kxtJo5N1yyS3HdXzNcPWdyzAWOpBtBmmUUkmscwoQhJ0ep95gs4juwk/dRDDjEKUnqnpiGGUECSR0o+vo/Nj96AVFDATAzu1NM9rGQKZKWw2H+qeQUgrWr5Q+RDwS+BZ4BDQAwzneXkPsBn4Xqx7+HalZ3JYyypIZgPqSAwdAUGBWE83rkUNEErgWLUAU6GXjK6gqjkmAu2ULV5K+/5tFF66GktlEUNffhDfxkV03vYIxeeeRWpk+PRbwD6vGnV0Oq19/cVnOP+MS8iWluFtmMWGS26k4MIW9ESO5ObjWJfXEHu5E9ea2QjjKZRuP8cqJZyonCeaWCgaWV3cwtj4MZIvtgEk8gnIK8DxPAlJv6uGoOV/q/kn8xgZ9bf+4+3a0COvoo5HyezsRhBFnPXzcDSWg88FZglXeTXoArHJELlwGI/BRvWmKxh4eStzvvZR1Ega1emg+uqzGHzoWRpbNpweANlkn85TTAZyU1G23PUTOjsOMtDeRmC0G8lnZuj323BcsxRjpRu52o19fhXd9zyDb+MSpvbu5uboPvYg8B+5EPMLS5BUHUHT9Xz6eQLoy9cP1b8jnkSBVlkyZIw+B4JBBElES6bJHB+gbO5yLEi4TW6Ov/0m3zr2BA/cfwemzhzjR49Ro7pwzGpGrrJhLnYTfPg1jDWl6E4NIZo9PQDJWIhMxyQV37qIips3cPW3buPcZRfgWTAPn68G4iql/7YGZJGpX+/Gc8NqlKkkBq+NxXd8DmNPgmeci7kQuFsuIJrTiT6/D80snurlS+dFFv09Ch+Wi779VWPhmQun62u6DhqM3f0Qg/5DiGMJUmqQwOOvEduzh9I1q0g+9QqepXOIxobweswM7T7Eqvu+Dhr03vkYVRedxcHv3Hl6JygZpAu0bIa0RaU07MC/wM5kcAyvtYQD/XuYvG8LroV1RB9vxbSwDOOMAsb/8zmMdT4GThykTHNTd9m1PK6l8aHy1e0/pKp+Nmo6TrptUMrv9c68+at/Z/FVjsbK7w719c3QiyzE3jgJkgA5DV0WafjENShjIcRYDqXaSaSjm1hfP7Wr1xD2jzN28iRWXwE2LATikxhDGYJHe1ArHMw4exX+zW//tRO0uD0U3nwO+uEA8QKBlc4m3OEc1pJSCn0VOJbORJaN6EYRqcBK6LetCAgok3EIZBjKJliLxCLRxFzRTEPlfExeD2oiheCx2IBNebWoIq/siH9DjZrrXjjzDuuaxjMFswHdIIDbjFjiALsBkln8oz2Y68ro+/GTyJJE+aZ1uKrLiasxtKEARa4i/PuPkK60k9rdgW11I3o8gxoMkOzsO/0W8HxyHcE7X8a5uBZ/10neyvZgXriC1ucfoiyhoWeyDHz/SXKDIWSbGefGueg5BWUihqnYSfSlN/iRHsAFXJGb4py0FZPJgmFmMYZl1UL9Ny6vLigp+hLwn8DKfFfpKb3OaKkpb/DMbviK98KWZyi2X4bVKEy0n8S/dR9GnwtsJgSjDAaJwPO7sDaWkAiFmdp3EltJEYpJRIiquFcuQGooZWZpA+mREaovPAtZUTE1lhJ6vo2uHz11eiY4/vMtVF68gr7trTgX1JGMxcgY/IgFTixWO5aGciwVxUSe3k/wwT2U/OclYDGgCzrmQjfE4Ei0m7NdLbiB0sIaAoHXiE8EqL70LC5au0Eo/soPir+6pOUa+kIe4Ef5Ls8EYF/75ZvvUFfWbVKLRcGXcxCcmKBw1Spm1jQQygWpsJfyyPe/BTv7UWM5mptWU//UA8ypq0b2lLDHVEh7fBzZYCK26zCB8jLKi2oZO9JDLBBm9k9uYGjPQQKHpyn0X4fB2mKGt7ai9k1hmVnIJ6ovYOToHq5Z/0nGVI1Eez+usxdhaiqi8hc3YLJ4IJNjxf1fxh63QUsdL7mWk9EVnjR42VEoYgwqLPjmp0jk4hzIjKB5fJDNGIAl+cOdv4cCqy419gRHhLbuN0mnkhQ2zsNcZUJsns2uN59i0p2i5aPX4b5hLXfd9yjLXU20LFuH11HIDNHGh2vO56bm6+h6eyft9z5F+kQ/obYOhGwGwWEi7NUQygvwfHbN6beAMhREDmTwfuYcAsd7GEtO0T45Spmis/+pBxErC5ECKun+SWwVVWQGRnCeu4D0cJju+5/DuWgGQ8APMwF6dZX2VIT+fbvIpRUmtuyjRHYxmBiBRBrAlldlbPl7MKQVxZiLJlH8USaEJKJowVzgIy2oiJEcAwOd+GbOYcP1t+BXRbaFj/FEYC+33vcF7hjeziPhNqKqxtoFl4EukIskmOwcZujR18mhkhkJIYoGDE7H6QHQnDbKvnc52hudpA4MYBB0brr0qwRUhRWf/yrFC+Yy8MsnUCQNq9VNxJxi3g2Xk5bBfulCipfOJY7GF4xeFggyifQIS266mejJbpRKN8uaNiKYSyi9YO2p65ryPkAAkHRNmGh7G7HQiexw0hk5TomnnqmBoyiRBJQX4Cyq4qqKRZgzIZ7f8mOkvk60UhuCxURSMlKNyvrmJRR/60KGX9jDjI+cj3ThXFKRGPaacuyNJQSf2/836gKLSzEXlhPxT+JLGHi0azfO4QA9qX5WrbuO9o5+XMvm4KsoIzXpx+osQJpVic2gI+0RMQ77Of/Az3BVz6HWt45qVyNDh7eR6O1m7syFrMZBvZBkqHEpm9nxV7FvTMsyZ8NlaOUuslkVIejnzc2/p+6KK7BWVlHongNqCq9s5Y2pw1iiIoPxEOaJOLlskL5UlEHZTam5AKvDjv/4APF9Jyior2fkZ2+SWFCHWuUid2zs9BZgdRVAWiPXFaDompUYMjHccRlDqRODw4iailN62VrChRqhF3ZjrqrAY3Ux/PB26tatINTaQ6J1L9doBXgRyGhpyue0kD0aYNWiJbyuJzCJFgJK4rTBP6wrtO96gcFjO+g9so3g2BiepsVEhTiNa9fTc+JlDEoCGYmRYD9ukwtbVS2ZTJapcBfJ2Cj+TBCTKCMjgS5w6Jb/IbzvOLlIArxGnFXlCE7z6QGI7zrJ8dt/jeyzIxSYSb1xiPPP20TV0mvZefddxF47jjoVg1gCSRcpb1yA/8Qxxl7dh//1/YxuaWXNwnWcXVDHXVqaSHiIYKQPg9dEVzpEJhfCoGfpnOr9y9hvnP4ikOkYIRMMQk6loMhL+EQ7o217OXj3PQT9PZQZ3CjoJLJJBra9TVEqi2IUUdU0VoePrCRjFGQIpUEU0HMaSiSCaVkNmWCUiTf24jxrzukBSPeMITeXsumZHxPfO8D4y/s50nGI9Gg388+6ANlkovP2Bwj/oZVsKkNscJiOza9DMM3Ik9NNSTtbX6UnOcYCBFZPmQlufpPUnm52/OBn7FX72alliZI+xUCrgLXABcDZWi5nkdx2zjj78+glDhwGB8mBEcREFpPswOopRtJ0dHQ0TQFNJ6OZsRS6QRQpds3EKNtA0FGDCdB1ECHx5KHp3gVRRDYZyQbCpwdANwiUn7+K4P5OBjbvwlVTga25CUM4iuyx4LluHb7zlpDu8pPqGSOwez9Tj+3Ke9Bpei8kU6RFgZ+lB6metRCDYsC2sBJtOMzQ4HG0bJR585cBmCWH5XzvZcvvqPn4uQ86Ll5yd1DoL1z3sc9Q6yok1TWIrmZIBafwOks457/vwm0v5ERohFEEEv0B6A2SmxjAGpdI7R9gfOAAaWROKkkiRwam5XoN9GSW7O4+xMk4wnCE1Bvdpwcgc2SYkYdf4s0v/QSAeZ+/kucfvZ/hnlZSE1EKFi9GSGdxrGvCvmEemXE/zjWNfzZZRtXwo1AgmCl1FRKfCKCiIZS4ubSshVkWL/2DndOAS8j1axebbVevN9bceIlx3oduFHb87l76snHMBW5y7gJMM4tJpaIcf3MLugDh2BhuBAorqsFuwj8wiHVNC1XLNhDLTtEsO3Bo2jsVz3X3fZ01L9+NrczLxG934ThnObM/uun0AKjBJOn9gwC03HQFfe3dfHLTzdTMbmYkOUbCP0g6EKa0fgaepc3E/3gYy/LaP5vsU6uvwmQr41lzBfeMPo8gCUhuC3q7n//Z/Sgd6jCBbW9Na5BuB+lYjMjeNib8fYymxsFhxCkIKJJOYnyQ+o9eR2wqSDaeAFVjeKwLyHB240rwWhjZshPNoGIvLKLUU06pZJ6W7U1mZIeZHXf+nGR3L5pBwnleI9hVGladcfowaFlVi1xkJ/rgfvY+8izGoXpecfhwLp2BNW1lpPsw8d1dhCb3YXzkNchlkcvduJfMJLy/C4BFDU18LXSCi70rkNunUI6OIdd5Kf/QmVTOmIUgmqDYgvcja0mm4vQ/ugP3x87G/3YbnsVL0f1xTLKMPhimt28vwqSOalQxCCJiUiHQ2097uJ9V7tm8es56TvQfRrIXoCtxrixbj1228vTe3xN88xCzPnouk2adjo7D4DHgWrqI6NGTvP5012nT4WZLS81GLZ1FmYxjKCtAqPegW4zMXX4hWx+5h+vOv5Hdr7yIubyAgqsWw4wiztx4JcO7j5HomLacoktWsap+KReIVp5O9ZNIjFP5b+tRFlSztvZMtiXGmDt/AbMuv4RFTi+t9z2FXmnDUlaIrCiEtrcxZcsR/t2r2JbUoHUEQVchpZHq8pMYHGbEk2Vl1RIalm9k6Zr1XORo5hxHE1Zd4tHOl+lqbSN+oJ3MRJwF3/0UPXf9keyRURRjDlNdEWpaI72/Xwf6gb2ntkAu/kq7numcwDt3Bhf+9ttkOycYeXUPsh4J6QAAA0BJREFUbUdfgaSCw+fBOKcYY6Wb4MsnKVy3BCEWJX6y5x00H/7Bj3lt98N8PnIMRzKO5rGARWamrZQVplKutjVilA2441Ee++1vQYCSxtmIspVQdz/Esvhf2IYNG7WLziaZjhE/0M3k5jeZ2t6KcthP+/Pb+eoT3+T3T93JcN9JArkou9Uw3/yPm3jqms9S0FCFschN4uQwjEeYd/eXECudODQLFeddjMNZRF6CUwFNzqs0o9pEPCX7HNbSa85k/9btiP4ki275CFaXG+2yDTz87e8gdE2RDKXwXtdCmctLW+thkl1/Yla5aIxSpxPNUsTg1AjuohIkk5VxNU1Ah7RspQIJf2SCzEgIwWSg//FtzLrzC7jqG9j56s0s+uTHOHjT9xg7foCSFUtJxCaJ/nEfQoGZ7N5BtOEIwy91MDgaY79RwPeZDVg3LGR4y1swliB+vBtjXCcRTWCaiFLYNAOTyczkriPoW15Cm27rV/MyXVTMA9CLUdrhOrNJ1xBQXu3FUF9EJBBiqK8d6cAwY4fbye4bInd4mBnzWtCMDrRn9v55/4rNyvqaFmqVFILdysz62RhSAuMj/UiZAHpyHNlTTFllPWZkdF1DD6UYfPJFMpkgy+/7D85ceyHYRLK7BtDSWVz1dWz69X9TuqAZkll0fxStJwjRNITS6J1+CjQbTpcXoikmX9jHoqvOA+ClG79NX9se3A3ViDYT8Wf3E9i6U8urUl1A6JQgkUbVx5P7+sontx8oi/aOyoYZHgwlHkJjw9ReezGTXR14NjZjOmcumRo7FxYtZPNn7vjz/kVFoXbdUjoLLDQUN9Ia3kuDu5SO2x/Ecv46+nMh3nptC93btxN4qQ3RaUFQNIwGmVjfON2/fBJlJMRI2xEWXXsxI4kRLLqR4OQwaiJFeF/ndE+HKEJWAVlEr3biXdGMTbIyufcoqY5RilbNYWTnIUhr+CqKmHvNJtp/9Ry57oCWf+X2lELd/u7ubgcwH7gCWAOU50vIH1wX9P/dUPMvXAwCb+eLMEcA/1+2t5vynV61QDVQnM/ZpQ+qFf7/aCTycvwgMJovxMSArPC/NFBa8jxB+gdesPz/eWTe9ULHqXeL9f8Hx9C9Y2QM77sAAAAASUVORK5CYII=");

	/**
	 * Getter de informações sobre o sistema operacional e JVM.
	 * 
	 * @return informações
	 */
	public static String getSystemInfo() {

		StringBuilder str = new StringBuilder();
		str.append(" Rodando em " + System.getProperty("os.name") + " ");

		str.append(System.getProperty("os.arch"));

		str.append(" com Java " + System.getProperty("java.version") + " ");
		str.append(System.getProperty("sun.arch.data.model") + "bits.\n");

		return str.toString();
	}

	public static void showLicense() {
		System.out.println(AssinadorUtils.LICENSE_FULL);

		System.out.println(AssinadorUtils.getSystemInfo());

	}

	public static final int MAX_TIME_TO_ACCEPT_CONFIG_IN_MINUTES = 15;

	public static final String REPOSITORY_FILE_EXTENSION = ".adcrep";

	public static void salvarConteudo(String filePath, String conteudo) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(filePath));
		try {
			writer.append(conteudo);
			writer.flush();
		} finally {
			writer.close();
		}
	}

	public static String lerConteudo(String filePath) throws FileNotFoundException, IOException {
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		StringBuilder conteudo = new StringBuilder();

		try {
			String c;
			while ((c = reader.readLine()) != null) {
				conteudo.append(c);
			}

			return conteudo.toString();
		} finally {
			reader.close();
		}
	}

	public static String politicaAssinaturaDER = "MIIKhjALBglghkgBZQMEAgEwggpTBghgTAEHAQECARgPMjAxMjAzMDcwMDAwMDBaMEuGSUM9QlIsTz1JQ1AtQnJhc2lsLE9VPUluc3RpdHV0byBOYWNpb25hbCBkZSBUZWNub2xvZ2lhIGRhIEluZm9ybWFjYW8gLSBJVEkMggKQRXN0ZSB0aXBvIGRlIGFzc2luYXR1cmEgZGV2ZSBzZXIgdXRpbGl6YWRvIGVtIGFwbGljYWNvZXMgb3UgcHJvY2Vzc29zIGRlIG5lZ29jaW8gbm9zIHF1YWlzIGEgYXNzaW5hdHVyYSBkaWdpdGFsIGFncmVnYSBzZWd1cmFuY2EgYSBhdXRlbnRpY2FjYW8gZGUgZW50aWRhZGVzIGUgdmVyaWZpY2FjYW8gZGUgaW50ZWdyaWRhZGUsIHBlcm1pdGluZG8gc3VhIHZhbGlkYWNhbyBkdXJhbnRlIG8gcHJhem8gZGUgdmFsaWRhZGUgZG9zIGNlcnRpZmljYWRvcyBkb3Mgc2lnbmF0YXJpb3MuIFVtYSB2ZXogcXVlIG5hbyBzYW8gdXNhZG9zIGNhcmltYm9zIGRvIHRlbXBvLCBhIHZhbGlkYWNhbyBwb3N0ZXJpb3Igc28gc2VyYSBwb3NzaXZlbCBzZSBleGlzdGlyZW0gcmVmZXJlbmNpYXMgdGVtcG9yYWlzIHF1ZSBpZGVudGlmaXF1ZW0gbyBtb21lbnRvIGVtIHF1ZSBvY29ycmV1IGEgYXNzaW5hdHVyYSBkaWdpdGFsLiBOZXNzYXMgc2l0dWFjb2VzLCBkZXZlIGV4aXN0aXIgbGVnaXNsYWNhbyBlc3BlY2lmaWNhIG91IHVtIGFjb3JkbyBwcmV2aW8gZW50cmUgYXMgcGFydGVzIGRlZmluaW5kbyBhcyByZWZlcmVuY2lhcyBhIHNlcmVtIHV0aWxpemFkYXMuIFNlZ3VuZG8gZXN0YSBQQSwgZSBwZXJtaXRpZG8gbyBlbXByZWdvIGRlIG11bHRpcGxhcyBhc3NpbmF0dXJhcy4wggdTMCIYDzIwMTIwMzA3MDAwMDAwWhgPMjAyMzA2MjEwMDAwMDBaMIIHI6BBMD8wOTAwBgkqhkiG9w0BCQMGCSqGSIb3DQEJBAYLKoZIhvcNAQkQAg8GCyqGSIb3DQEJEAIvMAChAwoBATACMAChgga/MIIGuzCCBqkwggalMIIGoTCCBImgAwIBAgIBATANBgkqhkiG9w0BAQ0FADCBlzELMAkGA1UEBhMCQlIxEzARBgNVBAoTCklDUC1CcmFzaWwxPTA7BgNVBAsTNEluc3RpdHV0byBOYWNpb25hbCBkZSBUZWNub2xvZ2lhIGRhIEluZm9ybWFjYW8gLSBJVEkxNDAyBgNVBAMTK0F1dG9yaWRhZGUgQ2VydGlmaWNhZG9yYSBSYWl6IEJyYXNpbGVpcmEgdjIwHhcNMTAwNjIxMTkwNDU3WhcNMjMwNjIxMTkwNDU3WjCBlzELMAkGA1UEBhMCQlIxEzARBgNVBAoTCklDUC1CcmFzaWwxPTA7BgNVBAsTNEluc3RpdHV0byBOYWNpb25hbCBkZSBUZWNub2xvZ2lhIGRhIEluZm9ybWFjYW8gLSBJVEkxNDAyBgNVBAMTK0F1dG9yaWRhZGUgQ2VydGlmaWNhZG9yYSBSYWl6IEJyYXNpbGVpcmEgdjIwggIiMA0GCSqGSIb3DQEBAQUAA4ICDwAwggIKAoICAQC6RqQO3edA8rWgfFKVV0X8bYTzhgHJhQOtmKvS8l4Fmcm7b2Jn/XdEuQMHPNIbAGLUcCxCg3lmq5lWroG8akm983QPYrfrWwdmlEIknUasmkIYMPAkqFFB6quV8agrAnhptSknXpwuc8b+I6Xjps79bBtrAFTrAK1POkw85wqIW9pemgtW5LVUOB3yCpNkTsNBklMgKs/8dG7U2zM4YuT+jkxYHPePKk3/xZLZCVK9z3AAnWmaM2qIh0UhmRZRDTTfgr20aah8fNTd0/IVXEvFWBDqhRnLNiJYKnIMmpbeys8IUWG/tAUpBiuGkP7pTcMEBUfLz3bZf3Gmh3sVQOQzgHgHHaTyjptAO8lyUN9pvvAslh+QtdWudONltIwa6Wob+3JcxYJU6uBTB8TMEun33tcv1EgvRz8mYQSxEpoza7WGSxMr0IadR+1p+/yEEmb4VuUOimx2xGsaesKgWhLRI4lYAXwIWNoVjhXZfn03tqRF9QOFzEf6i3lFuGZiM9MmSt4c6dR/5m0muTx9zQ8oCikPm91jq7mmRxqE14WkA2UGBEtSjYM0Qn8xjhEu5rNnlUB+l3pAAPkRbIM4WK0DM1umxMHFsKwNqQbwpmkBNLbp+JRITz6mdQnsSsU74MlesDL/n2lZzzwwbw3OJ1fsWhto/+xPb3gyPnnFtF2VfwIDAQABo4H1MIHyME4GA1UdIARHMEUwQwYFYEwBAQAwOjA4BggrBgEFBQcCARYsaHR0cDovL2FjcmFpei5pY3BicmFzaWwuZ292LmJyL0RQQ2FjcmFpei5wZGYwPwYDVR0fBDgwNjA0oDKgMIYuaHR0cDovL2FjcmFpei5pY3BicmFzaWwuZ292LmJyL0xDUmFjcmFpenYyLmNybDAfBgNVHSMEGDAWgBQMOSA6twEfy9cofUGgx/pKrTIkvjAdBgNVHQ4EFgQUDDkgOrcBH8vXKH1BoMf6Sq0yJL4wDwYDVR0TAQH/BAUwAwEB/zAOBgNVHQ8BAf8EBAMCAQYwDQYJKoZIhvcNAQENBQADggIBAFmaFGkYbX0pQ3B9dpth33eOGnbkqdbLdqQWDEyUEsaQ0YEDxa0G2S1EvLIJdgmAOWcAGDRtBgrmtRBZSLp1YPw/jh0YVXArnkuVrImrCncke2HEx5EmjkYTUTe2jCcK0w3wmisig4OzvYM1rZs8vHiDKTVhNvgRcTMgVGNTRQHYE1qEO9dmEyS3xEbFIthzJO4cExeWyCXoGx7P34VQbTzq91CeG5fep2vb1nPSz3xQwLCM5VMSeoY5rDVbZ8fq1PvRwl3qDpdzmK4pv+Q68wQ2UCzt3h7bhegdhAnu86aDM1tvR3lPSLX8uCYTq6qz9GER+0Vn8x0+bv4qSyZEGp+xouA82uDkBTp4rPuooU2/XSx3KZDNEx3vBijYtxTzW8jJnqd+MRKKeGLE0QW8BgJjBCsNid3kXFsygETUQuwq8/JAhzHVPuIKMgwUjdVybQvm/Y3kqPMFjXUXd5sKufqQkplliDJnQwWOLQsVuzXxYejZZ3ftFuXoAS1rND+Og7P36g9KHj41hJ2MgDQ/qZXow63EzZ7KFBYsGZ7kNou5uaNCJQc+w+XVaE+gZhyms7ZzHJAaP0C5GlZCcIf/by0PEf0e//eFMBUO4xcx7ieVzMnpmR6Xx21bB7UFaj3yRd+6gnkkcC6bgh9mqaVtJ8z2KqLRX4Vv4EadqtKlTlUOMAwwAwoBA6AFMAMKAQOiAjAApBcwFaATMBEwDwYJKoZIhvcNAQELAgIIADAGMAQwAgUABCDdV8mKQxO8E5jOZUPTgCRYlXz3Fq4ylOxNjCYlEpHmwQ==";
	private static Date inicioTempo;

	public static byte[] readFile(String filePath) throws FileNotFoundException, IOException {
		FileInputStream in = null;
		byte[] bin = null;
		in = new FileInputStream(filePath);
		bin = new byte[in.available()];
		in.read(bin);
		in.close();
		return bin;
	}

	public static void addProvider(Provider provider) {
		if (Security.getProvider(provider.getName()) == null || Security.getProvider(provider.getName()).isEmpty()) {
			Security.addProvider(provider);
		}
	}

	public static void removeProvider(String keyRepositoryProviderName) {
		if (Security.getProvider(keyRepositoryProviderName) != null) {
			Security.removeProvider(keyRepositoryProviderName);
		}
	}

	private static String concat(boolean withSpacesInBeetwen, Object... strings) {

		StringBuilder str = new StringBuilder();
		for (int i = 0; i < strings.length; i++) {
			str.append(strings[i]);
			if (withSpacesInBeetwen && (i + 1) < strings.length) {
				str.append(" ");
			}
		}

		return str.toString();

	}

	public static String concatWithSpaces(Object... strings) {
		return concat(true, strings);
	}

	public static String concatWithoutSpaces(Object... strings) {
		return concat(false, strings);
	}

	public static void tic() {
		inicioTempo = new Date();
	}

	public static long toc() {
		return (new Date().getTime() - inicioTempo.getTime());
	}

}