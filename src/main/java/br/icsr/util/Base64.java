package br.icsr.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Class Base64 
 * Classe originária do BouncyCastle e com 3 métodos adicionais.
 * Base64 é uma codificação especial para a internet contendo apenas os caracteres [A-Z,a-z,0-9, e + /]
 * @author 1T Israel C. S. Rocha (CCASJ)
 * @version 1.0 - 2012
 */
public class Base64 {
	private static final Base64Encoder encoder = new Base64Encoder();
	
	/**
	 * Verifica se uma String está codificada em Base64.
	 * @param str String a ser verifica.
	 * @return true se está codificada.
	 */
	public static boolean isBase64Encoded(String str){
	    return str.matches("^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$");
	}

	/**
	 * Codifica em Base64
	 * @param s String a codificar.
	 * @return String codificada
	 */
	public static String encodeToBase64(String s) {

		return new String(encode(s.getBytes()));

	}

	/**
	 * Codifica em Base64
	 * @param b byte[] a codificar.
	 * @return String codificada
	 */
	public static String encodeToBase64(byte[] b) {

		return new String(encode(b));
	}

	/**
	 * Codifica em Base64
	 * @param sBase64 String em Base64 a decodificar.
	 * @return String decodificada
	 */
	public static String decodeToString(String sBase64) {

		return new String(decode(sBase64));

	}

	/**
	 * encode the input data producing a base 64 encoded byte array.
	 * 
	 * @return a byte array containing the base 64 encoded data.
	 */
	private static byte[] encode(byte[] data) {
		int len = (data.length + 2) / 3 * 4;
		ByteArrayOutputStream bOut = new ByteArrayOutputStream(len);

		try {
			encoder.encode(data, 0, data.length, bOut);
		} catch (IOException e) {
			throw new RuntimeException("exception encoding base64 string: " + e);
		}

		return bOut.toByteArray();
	}

	

	/**
	 * Encode the byte data to base 64 writing it to the given output stream.
	 * @param data
	 * @param off
	 * @param length
	 * @param out
	 * @return the number of bytes produced.
	 * @throws IOException
	 */
	protected static int encode(byte[] data, int off, int length, OutputStream out)
			throws IOException {
		return encoder.encode(data, off, length, out);
	}

	/**
	 * decode the base 64 encoded input data. It is assumed the input data is
	 * valid.
	 * @param data
	 * @return a byte array representing the decoded data.
	 */
	protected static byte[] decode(byte[] data) {
		int len = data.length / 4 * 3;
		ByteArrayOutputStream bOut = new ByteArrayOutputStream(len);

		try {
			encoder.decode(data, 0, data.length, bOut);
		} catch (IOException e) {
			throw new RuntimeException("exception decoding base64 string: " + e);
		}

		return bOut.toByteArray();
	}

	/**
	 * decode the base 64 encoded String data - whitespace will be ignored.
	 * @param data String em Base64 a decodificar.
	 * @return a byte array representing the decoded data.
	 */
	public static byte[] decode(String data) {
		int len = data.length() / 4 * 3;
		ByteArrayOutputStream bOut = new ByteArrayOutputStream(len);

		try {
			encoder.decode(data, bOut);
		} catch (IOException e) {
			throw new RuntimeException("exception decoding base64 string: " + e);
		}

		return bOut.toByteArray();
	}

	/**
	 * decode the base 64 encoded String data writing it to the given output
	 * stream, whitespace characters will be ignored.
	 * @param data
	 * @param out
	 * @throws IOException Falha de IO
	 * @return the number of bytes produced.
	 */
	protected static int decode(String data, OutputStream out) throws IOException {
		return encoder.decode(data, out);
	}
}
