package br.icsr.util;

public class Constantes {
	
	/**
	 * Configuração passada como parâmetro (<i>param</i>) para o <b>JApplet</b>
	 * de Assinatura Digital. <br>
	 * Este parâmetro é parte de uma grande <i>String</i> , chamada
	 * <b>configuracoesCodificadas</b>, que identifica uma sequência de
	 * configuração de dispositivos criptográficos e que é necessária ao
	 * <b>SunPKCS11</b>.
	 * <p>
	 * * As configuracoesCodificadas, deve ser passada ao applet como parâmetro
	 * (<i>param</i>) com o seguinte formato:
	 * <p>
	 * <code>
	 * [configForWindows][BLOCO DE CONFIGURAÇÃO]{*}[configForWindows][configForLinux][BLOCO DE CONFIGURAÇÃO]{*}[configForLinux]
	 * <code>
	 * <p>
	 * Em que <b>[BLOCO DE CONFIGURAÇÃO]</b> figura <b>n</b> (0 ou mais vezes) e é da igual a:
	 * <p>
	 * <code>name=[VALOR <b>n</b>][breakLine]library=[PATH PARA BIBLIOTECA PKCS11][breakLine]showInfo = true[breakLine][objectSeparator]</code>
	 * <p>
	 * Em que <i>configForWindows</i>, <i>configForLinux</i>, <i>breakLine</i> e
	 * <i>objectSeparator</i> são passados ao applet como parâmetros (param).
	 * <p>
	 * O SunPKCS11 toma em seu construtor um <b>arquivo .cfg</b> ou
	 * <i>InputStream</i> contendo em seu conteúdo pelo menos um <b>name</b> e
	 * um <b>library</b> PKCS#11 (uma biblioteca .dll ou .so) de manipulação do
	 * dispositivo criptográfico.
	 * 
	 * @see <a
	 *      href="http://docs.oracle.com/javase/6/docs/technotes/guides/security/p11guide.html">PKCS#11
	 *      Reference Guide</a>
	 */
	public static final String CONFIG_WIN = "_WIN_";
	/**
	 * Configuração passada como parâmetro (<i>param</i>) para o <b>JApplet</b>
	 * de Assinatura Digital. <br>
	 * Este parâmetro é parte de uma grande <i>String</i> , chamada
	 * <b>configuracoesCodificadas</b>, que identifica uma sequência de
	 * configuração de dispositivos criptográficos e que é necessária ao
	 * <b>SunPKCS11</b>.
	 * 
	 * @see br.icsr.assinador.applet.AssinadorCADESApplet#CONFIG_WIN
	 */
	public static final String CONFIG_LINUX = "_LINUX_";
	/**
	 * Configuração passada como parâmetro (<i>param</i>) para o <b>JApplet</b>
	 * de Assinatura Digital. <br>
	 * Este parâmetro é parte de uma grande <i>String</i> , chamada
	 * <b>configuracoesCodificadas</b>, que identifica uma sequência de
	 * configuração de dispositivos criptográficos e que é necessária ao
	 * <b>SunPKCS11</b>.
	 * 
	 * @see br.icsr.assinador.applet.AssinadorCADESApplet#CONFIG_WIN
	 */
	public static final String SEPARADOR_OBJETOS = "_NEWOBJ_";
	/**
	 * Configuração passada como parâmetro (<i>param</i>) para o <b>JApplet</b>
	 * de Assinatura Digital. <br>
	 * Este parâmetro é parte de uma grande <i>String</i> , chamada
	 * <b>configuracoesCodificadas</b>, que identifica uma nova linha em uma sequência de
	 * configuração de dispositivos criptográficos e que é necessária ao
	 * <b>SunPKCS11</b>.
	 * 
	 * @see br.icsr.assinador.applet.AssinadorCADESApplet#CONFIG_WIN
	 */
	public static final String BREAKLINE = "_BR_";
	
	public static final int MAX_JS_LENGTH = 1000000;
	public static final String TEMP_FOLDER = System.getProperty("java.io.tmpdir") + "/assinadordigitalccasj/";


}
