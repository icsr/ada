package br.icsr.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//TODO substituir por LOG4J

public class Logger {

	private static List<String> logs = new ArrayList<String>();

	public static SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

	public static enum LEVEL {
		INFO("[info]"), WARN("[warn]"), ERROR("[erro]"), CRITICAL("[crit]");

		private String header;

		LEVEL(String header) {
			this.header = header;
		}

		@Override
		public String toString() {
			return this.header.toUpperCase();
		}

	};

	public static void info(Object... message) {
		log(AssinadorUtils.concatWithoutSpaces(message), LEVEL.INFO);
	}

	public static void warn(Object... message) {
		log(AssinadorUtils.concatWithoutSpaces(message), LEVEL.WARN);
	}

	public static void error(Object... message) {
		log(AssinadorUtils.concatWithoutSpaces(message), LEVEL.ERROR);
	}

	public static void critical(Object... message) {
		log(AssinadorUtils.concatWithoutSpaces(message), LEVEL.CRITICAL);
	}

	private static void log(String message, LEVEL level) {
		try {
			String logMessage = AssinadorUtils.concatWithoutSpaces(format.format(new Date()), " - ", level.toString(),
					" ", message, System.lineSeparator());
			System.out.println(logMessage);

			logs.add(logMessage);

			BufferedWriter out = new BufferedWriter(new FileWriter(getLogFile()));

			for (String s : logs) {
				out.append(s);
			}

			out.write(logMessage);

			out.flush();
			out.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static String getLogFile() {
		return AssinadorUtils.concatWithoutSpaces(AssinadorUtils.getAssinadorHome(), File.separator, "log");
	}

}
