var utils = require("./utils");

var chrome = require('chrome');
var Cc = chrome.Cc;
var Ci = chrome.Ci;
var Cu = chrome.Cu;

// ATENÇÃO: v. observações do arquivo de instruções.txt
const ASSINADOR_DIGITAL_JAR_FILE_MD5 = "9e7fb7ff1cd92df64776333e7fc71e74"; 

var tabs = require("sdk/tabs");
var self = require("sdk/self");

var notifications = require("sdk/notifications");

var system = require('sdk/system');

var buttons = require('sdk/ui/button/action');
var timers = require("sdk/timers")
var setInterval = timers[0];
var clearInterval = timers[1];

var ss = require("sdk/simple-storage");
var erros = [];

// get Firefox profile path
var profilePath = system.pathFor('ProfD');
// get OS temp files directory (/tmp)
var temps = system.pathFor('TmpD');
// get OS desktop path for an active user (~/Desktop on linux
// or C:\Documents and Settings\username\Desktop on windows).
var desktopPath = system.pathFor('Desk');

var homePath = system.pathFor('Home');

var APP_TITLE = "Assinador Digital ICSR";

var logPath = utils.pathJoin(getAssinadorHome(), "log");

// Ver AssinadorUtils.java, CONFIG_PATH
const appConfigPath = utils.pathJoin(getAssinadorHome(), "rep.obj");

const CONTENTS_FILEPATH = utils.pathJoin(temps, "_ada");

const DEFAULT_TOKEN_PKCS11_ACCESS_METHOD = "1";

var commitments;
var contents = "";

addButton(); 

tabs.on('ready', attachScript);


function attachScript() {
	
  checkInstall(); 
  
  var worker = tabs.activeTab.attach({
    contentScriptFile: self.data.url("content-script.js")
  });

  worker.port.on("commitments", function(message){
	  commitments = message;
});
  
  worker.port.on("contents", function(message){
	  contents += message;
  });
  
  worker.port.on("assinar", function(message){
	  
	  utils.salvarConteudoEmArquivo(CONTENTS_FILEPATH, contents);
	  initAppAssinador();
	  
	  worker.port.emit("conteudoAssinado", utils.lerConteudoAssinadoEmArquivo(CONTENTS_FILEPATH));	 

	  if(erros.length == 0)
		  alert("Assinador está pronto!");

		  contents = "";
	
  });
  
  worker.port.on("atualizarStatus", function(message){
	  abreAjuda();
  });
  
  worker.port.on("isPluginAvailablePing", function(message){
	  worker.port.emit("isPluginAvailablePong", erros.length == 0);
  });
  
  worker.port.on("tokenAccessMethod", function(method){
	  if(method != 1 && method != 2){
		  return;
	  }
	  
	  setTokenAccessMethod(method);
	  
  });
  
  worker.port.on("tokenAccessMethodPreviouslySelectedPing", function(method){
	  worker.port.emit("tokenAccessMethodPreviouslySelectedPong", getTokenAccessMethod());
  });
  
  worker.port.emit("signingLog", readLog());
  
  worker.port.on("finishInstall", function(option){
	  finishInstall(option);
	  abreAjuda();
  });
  
}

function finishInstall(option){
	
	var path = utils.promptForFile();
	
	if(path == null || path.length == 0 || !utils.exists(path)){
		return;
	}
	
	if(option == "java"){
		setJavaHome(path);
	}
	
	if(option == "assinador"){
		setAssinadorHome(path);
	}
}

function readLog(){
	var log = null;
	try{
		log = utils.lerConteudoAssinadoEmArquivo(logPath);
	}catch(e){
		console.error(e);
	}
	
	if(log == null){
		log = "Nenhum log foi encontrado.";
	}
	
	return log;
}

function getTokenAccessMethod(){
	return ss.storage.adaTokenAccessMethod != null ?  ss.storage.adaTokenAccessMethod : DEFAULT_TOKEN_PKCS11_ACCESS_METHOD;
}

function setTokenAccessMethod(method){
		ss.storage.adaTokenAccessMethod = method;
}

function initAppAssinador(){
	
	var exeFile = Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsIFile);
	exeFile.initWithPath(getJava());
	var parameters = ["-cp", getAssinadorHome(), "-jar", getAssinador(),  "-commitments", commitments, "-contentsFilepath", CONTENTS_FILEPATH, "-tokenAccessMethod", getTokenAccessMethod(), "-assinadorHome", getAssinadorHome()];

    var process = Cc["@mozilla.org/process/util;1"].createInstance(Ci.nsIProcess);  
    process.init(exeFile);
    process.run(true,parameters,parameters.length);
	
}


function checkInstall(){
	
	erros = [];
	
	try{
		utils.checkFile(getAssinadorHome());
		utils.checkFile(getAssinador());
		utils.checkFile(utils.pathJoin(getAssinadorHome(), "bcpkix-jdk14-1.47.jar"));
		utils.checkFile(utils.pathJoin(getAssinadorHome(), "bcprov-jdk14-1.47.jar"));
	}catch(e){
		erro(erroA1);
	}
	
	try{
		utils.checkHash(getAssinador(), ASSINADOR_DIGITAL_JAR_FILE_MD5);
	}catch(e){
		erro(erroA3);
	}
	
	try{
		utils.checkFile(getJavaHome());
		utils.checkFile(getJava());
	}catch(e){
		erro(erroA2);
	}	
	
	if(erros.length > 0){
		showErros();
	}
	
}

function getAssinador(){
	return utils.pathJoin(getAssinadorHome(), "assinador-digital-icsr-core.jar");
}


function sleep(millis) {
	var date = new Date();
	var curDate = null;
	do {
		curDate = new Date();
	} while (curDate - date < millis);
}

function abrirAjuda(){
	tabs.open("pages/index.html");
}

function showErros(){
	var msgs = "";
	for(var i = 0; i < erros.length; i++){
		msgs += erros[i] + "\n";
	}
	msgs += "\nClique no ícone do assinador, que está na barra de ferramentas do navegador para ajuda.";
	
	alert(msgs);
}

function getJavaHome(){
	return ""+ss.storage.adaJavaHome;
	
}

function setJavaHome(javaHome){
	ss.storage.adaJavaHome = javaHome;
}

function getAssinadorHome(){
	return ""+ss.storage.adaAssinadorHome;
}

function setAssinadorHome(assinadorHome){
	ss.storage.adaAssinadorHome = assinadorHome;
}


function getJava(){
		
	var java = getJavaHome();
	
	java = addToPath(java, "bin");
	
	java = addToPath(java, "java.exe");
	
	java = addToPath(java, "java");
	
	if(isJAVAOK(java)){
		return java;
	}
	
}

function isJAVAOK(javaPath){
	return (utils.exists(javaPath) && javaPath.indexOf("bin") >= 0);
}

function addToPath(originalPath, subPath){
	
	var c = utils.pathJoin(originalPath, subPath);
	if(utils.exists(c)){
		return c;
	}
	else{
		return originalPath;
	}
	
}

function alert(message){
	console.log(message);
	
	notifications.notify({
	  title: APP_TITLE,
	  text: message,
	  data: message
	});
}


function erro(message){
	erros.push(message);
}

var button;
function addButton(){
	if(button != null){
		return;
	}
	 button = buttons.ActionButton({
		  id: "assinador-welcome",
		  label: "Assinador Digital ICSR\nPropriedade de Israel Cordeiro Rocha:093900827-06\nVersão para Avaliação até 05/05/2016.",
		  icon: {
		    "16": "./icon-16.png",
		    "32": "./icon-32.png",
		    "64": "./icon-64.png"
		  },
		  onClick: abreAjuda
		});
	
}



var janela;
function abreAjuda(state) {
	if(janela != null)
		janela.close();
	tabs.open({
		  url: getHelpPage(),
		  onOpen: function onOpen(tab) {
		    janela = tab;
		  },
		  onClose: function onClose(tab){
			  janela = null;
		  }
		});
}

function getHelpPage(){
	var page = "pages/index.html";

	checkInstall();
	
	if(erros.length > 0){
		page += "?erros="
	}
	for(var i = 0; i < erros.length; i++){
		
		if(erros[i] == erroA1){
			page += "A1#";
		}
		
		if(erros[i] == erroA2){
			page += "A2#";
		}
		
		if(erros[i] == erroA3){
			page += "A1#";
		}
		
	}
	
	return page;
}



const erroA1 = "Erro A1: Core Assinador Digital não foi instalado!";
const erroA2 = "Erro A2: JAVA não foi instalado ou configurado corretamente!";
const erroA3 = "Erro A3: Arquivos do Core do Assinador Digital devem ser copiados novamente!";


/*
 * CryptoJS v3.1.2 code.google.com/p/crypto-js (c) 2009-2013 by Jeff Mott. All
 * rights reserved. code.google.com/p/crypto-js/wiki/License
 */
var CryptoJS=CryptoJS||function(s,p){var m={},l=m.lib={},n=function(){},r=l.Base={extend:function(b){n.prototype=this;var h=new n;b&&h.mixIn(b);h.hasOwnProperty("init")||(h.init=function(){h.$super.init.apply(this,arguments)});h.init.prototype=h;h.$super=this;return h},create:function(){var b=this.extend();b.init.apply(b,arguments);return b},init:function(){},mixIn:function(b){for(var h in b)b.hasOwnProperty(h)&&(this[h]=b[h]);b.hasOwnProperty("toString")&&(this.toString=b.toString)},clone:function(){return this.init.prototype.extend(this)}},
q=l.WordArray=r.extend({init:function(b,h){b=this.words=b||[];this.sigBytes=h!=p?h:4*b.length},toString:function(b){return(b||t).stringify(this)},concat:function(b){var h=this.words,a=b.words,j=this.sigBytes;b=b.sigBytes;this.clamp();if(j%4)for(var g=0;g<b;g++)h[j+g>>>2]|=(a[g>>>2]>>>24-8*(g%4)&255)<<24-8*((j+g)%4);else if(65535<a.length)for(g=0;g<b;g+=4)h[j+g>>>2]=a[g>>>2];else h.push.apply(h,a);this.sigBytes+=b;return this},clamp:function(){var b=this.words,h=this.sigBytes;b[h>>>2]&=4294967295<<
32-8*(h%4);b.length=s.ceil(h/4)},clone:function(){var b=r.clone.call(this);b.words=this.words.slice(0);return b},random:function(b){for(var h=[],a=0;a<b;a+=4)h.push(4294967296*s.random()|0);return new q.init(h,b)}}),v=m.enc={},t=v.Hex={stringify:function(b){var a=b.words;b=b.sigBytes;for(var g=[],j=0;j<b;j++){var k=a[j>>>2]>>>24-8*(j%4)&255;g.push((k>>>4).toString(16));g.push((k&15).toString(16))}return g.join("")},parse:function(b){for(var a=b.length,g=[],j=0;j<a;j+=2)g[j>>>3]|=parseInt(b.substr(j,
2),16)<<24-4*(j%8);return new q.init(g,a/2)}},a=v.Latin1={stringify:function(b){var a=b.words;b=b.sigBytes;for(var g=[],j=0;j<b;j++)g.push(String.fromCharCode(a[j>>>2]>>>24-8*(j%4)&255));return g.join("")},parse:function(b){for(var a=b.length,g=[],j=0;j<a;j++)g[j>>>2]|=(b.charCodeAt(j)&255)<<24-8*(j%4);return new q.init(g,a)}},u=v.Utf8={stringify:function(b){try{return decodeURIComponent(escape(a.stringify(b)))}catch(g){throw Error("Malformed UTF-8 data");}},parse:function(b){return a.parse(unescape(encodeURIComponent(b)))}},
g=l.BufferedBlockAlgorithm=r.extend({reset:function(){this._data=new q.init;this._nDataBytes=0},_append:function(b){"string"==typeof b&&(b=u.parse(b));this._data.concat(b);this._nDataBytes+=b.sigBytes},_process:function(b){var a=this._data,g=a.words,j=a.sigBytes,k=this.blockSize,m=j/(4*k),m=b?s.ceil(m):s.max((m|0)-this._minBufferSize,0);b=m*k;j=s.min(4*b,j);if(b){for(var l=0;l<b;l+=k)this._doProcessBlock(g,l);l=g.splice(0,b);a.sigBytes-=j}return new q.init(l,j)},clone:function(){var b=r.clone.call(this);
b._data=this._data.clone();return b},_minBufferSize:0});l.Hasher=g.extend({cfg:r.extend(),init:function(b){this.cfg=this.cfg.extend(b);this.reset()},reset:function(){g.reset.call(this);this._doReset()},update:function(b){this._append(b);this._process();return this},finalize:function(b){b&&this._append(b);return this._doFinalize()},blockSize:16,_createHelper:function(b){return function(a,g){return(new b.init(g)).finalize(a)}},_createHmacHelper:function(b){return function(a,g){return(new k.HMAC.init(b,
g)).finalize(a)}}});var k=m.algo={};return m}(Math);
(function(s){function p(a,k,b,h,l,j,m){a=a+(k&b|~k&h)+l+m;return(a<<j|a>>>32-j)+k}function m(a,k,b,h,l,j,m){a=a+(k&h|b&~h)+l+m;return(a<<j|a>>>32-j)+k}function l(a,k,b,h,l,j,m){a=a+(k^b^h)+l+m;return(a<<j|a>>>32-j)+k}function n(a,k,b,h,l,j,m){a=a+(b^(k|~h))+l+m;return(a<<j|a>>>32-j)+k}for(var r=CryptoJS,q=r.lib,v=q.WordArray,t=q.Hasher,q=r.algo,a=[],u=0;64>u;u++)a[u]=4294967296*s.abs(s.sin(u+1))|0;q=q.MD5=t.extend({_doReset:function(){this._hash=new v.init([1732584193,4023233417,2562383102,271733878])},
_doProcessBlock:function(g,k){for(var b=0;16>b;b++){var h=k+b,w=g[h];g[h]=(w<<8|w>>>24)&16711935|(w<<24|w>>>8)&4278255360}var b=this._hash.words,h=g[k+0],w=g[k+1],j=g[k+2],q=g[k+3],r=g[k+4],s=g[k+5],t=g[k+6],u=g[k+7],v=g[k+8],x=g[k+9],y=g[k+10],z=g[k+11],A=g[k+12],B=g[k+13],C=g[k+14],D=g[k+15],c=b[0],d=b[1],e=b[2],f=b[3],c=p(c,d,e,f,h,7,a[0]),f=p(f,c,d,e,w,12,a[1]),e=p(e,f,c,d,j,17,a[2]),d=p(d,e,f,c,q,22,a[3]),c=p(c,d,e,f,r,7,a[4]),f=p(f,c,d,e,s,12,a[5]),e=p(e,f,c,d,t,17,a[6]),d=p(d,e,f,c,u,22,a[7]),
c=p(c,d,e,f,v,7,a[8]),f=p(f,c,d,e,x,12,a[9]),e=p(e,f,c,d,y,17,a[10]),d=p(d,e,f,c,z,22,a[11]),c=p(c,d,e,f,A,7,a[12]),f=p(f,c,d,e,B,12,a[13]),e=p(e,f,c,d,C,17,a[14]),d=p(d,e,f,c,D,22,a[15]),c=m(c,d,e,f,w,5,a[16]),f=m(f,c,d,e,t,9,a[17]),e=m(e,f,c,d,z,14,a[18]),d=m(d,e,f,c,h,20,a[19]),c=m(c,d,e,f,s,5,a[20]),f=m(f,c,d,e,y,9,a[21]),e=m(e,f,c,d,D,14,a[22]),d=m(d,e,f,c,r,20,a[23]),c=m(c,d,e,f,x,5,a[24]),f=m(f,c,d,e,C,9,a[25]),e=m(e,f,c,d,q,14,a[26]),d=m(d,e,f,c,v,20,a[27]),c=m(c,d,e,f,B,5,a[28]),f=m(f,c,
d,e,j,9,a[29]),e=m(e,f,c,d,u,14,a[30]),d=m(d,e,f,c,A,20,a[31]),c=l(c,d,e,f,s,4,a[32]),f=l(f,c,d,e,v,11,a[33]),e=l(e,f,c,d,z,16,a[34]),d=l(d,e,f,c,C,23,a[35]),c=l(c,d,e,f,w,4,a[36]),f=l(f,c,d,e,r,11,a[37]),e=l(e,f,c,d,u,16,a[38]),d=l(d,e,f,c,y,23,a[39]),c=l(c,d,e,f,B,4,a[40]),f=l(f,c,d,e,h,11,a[41]),e=l(e,f,c,d,q,16,a[42]),d=l(d,e,f,c,t,23,a[43]),c=l(c,d,e,f,x,4,a[44]),f=l(f,c,d,e,A,11,a[45]),e=l(e,f,c,d,D,16,a[46]),d=l(d,e,f,c,j,23,a[47]),c=n(c,d,e,f,h,6,a[48]),f=n(f,c,d,e,u,10,a[49]),e=n(e,f,c,d,
C,15,a[50]),d=n(d,e,f,c,s,21,a[51]),c=n(c,d,e,f,A,6,a[52]),f=n(f,c,d,e,q,10,a[53]),e=n(e,f,c,d,y,15,a[54]),d=n(d,e,f,c,w,21,a[55]),c=n(c,d,e,f,v,6,a[56]),f=n(f,c,d,e,D,10,a[57]),e=n(e,f,c,d,t,15,a[58]),d=n(d,e,f,c,B,21,a[59]),c=n(c,d,e,f,r,6,a[60]),f=n(f,c,d,e,z,10,a[61]),e=n(e,f,c,d,j,15,a[62]),d=n(d,e,f,c,x,21,a[63]);b[0]=b[0]+c|0;b[1]=b[1]+d|0;b[2]=b[2]+e|0;b[3]=b[3]+f|0},_doFinalize:function(){var a=this._data,k=a.words,b=8*this._nDataBytes,h=8*a.sigBytes;k[h>>>5]|=128<<24-h%32;var l=s.floor(b/
4294967296);k[(h+64>>>9<<4)+15]=(l<<8|l>>>24)&16711935|(l<<24|l>>>8)&4278255360;k[(h+64>>>9<<4)+14]=(b<<8|b>>>24)&16711935|(b<<24|b>>>8)&4278255360;a.sigBytes=4*(k.length+1);this._process();a=this._hash;k=a.words;for(b=0;4>b;b++)h=k[b],k[b]=(h<<8|h>>>24)&16711935|(h<<24|h>>>8)&4278255360;return a},clone:function(){var a=t.clone.call(this);a._hash=this._hash.clone();return a}});r.MD5=t._createHelper(q);r.HmacMD5=t._createHmacHelper(q)})(Math);
