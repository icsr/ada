const
DEFAULT_APPLET_CLASS = "ada-applet";
const
DEFAULT_SIGN_BUTTOM_CLASS = "ada-sign";
const
DEFAULT_SUBMIT_CLASS = "ada-submit";

var conteudoInput, compromissoInput;
var submitted = false;

disableAppletAssinador();

self.port.emit("isPluginAvailablePing", null);

self.port.on("isPluginAvailablePong", function(available) {
	replaceAppletAssinador(available);
});

self.port.on("signingLog", function(log) {
	
	var logElm = document.getElementById("signingLog");
	if(logElm == null){
		return;
	}
	
	logElm.innerHTML = log.replace(/\n/g,"<br/>");
	
});

exportFunction(assinarComPlugin, unsafeWindow, {
	defineAs : "assinarComPlugin"
});


exportFunction(assinar, unsafeWindow, {
	defineAs : "assinar"
});

exportFunction(selectTokenAccessMethod, unsafeWindow, {
	defineAs : "selectTokenAccessMethod"
});

exportFunction(getTokenAccessMethodPreviouslySelected, unsafeWindow, {
	defineAs : "getTokenAccessMethodPreviouslySelected"
});

exportFunction(replaceAppletAssinador, unsafeWindow, {
	defineAs : "replaceAppletAssinador"
});

exportFunction(atualizarStatus, unsafeWindow, {
	defineAs : "atualizarStatus"
});

exportFunction(finishInstall, unsafeWindow,{
	defineAs: "finishInstall"
});


function finishInstall(option){
	self.port.emit("finishInstall", option);
}

function replaceAppletAssinador(available) {
	disableAppletAssinador();

	if (available) {
		enableAppletAssinador();
		return;
	}

	changeSigningMethod();
}

function getTokenAccessMethodPreviouslySelected() {
	self.port.emit("tokenAccessMethodPreviouslySelectedPing", null);
}

self.port.on("tokenAccessMethodPreviouslySelectedPong", function(method) {

	if (method == 1) {
		document.getElementById("m1").setAttribute("checked", "true");
	}

	if (method == 2) {
		document.getElementById("m2").setAttribute("checked", "true");
	}

});

function isWindows() {
	return unsafeWindow.navigator.oscpu.toLowerCase().indexOf("win") >= 0;
}

function atualizarStatus() {
	self.port.emit("atualizarStatus", null);
}

function enableAppletAssinador() {
	var applet = document.getElementsByClassName(DEFAULT_APPLET_CLASS)[0];

	if (applet == null) {
		return;
	}

	console
			.log("Add-on ADC instalado incorretamente! A assinatura digital via applet será automaticamente habilitada!");
	applet.style.display = "block";
}

function disableAppletAssinador() {
	var applet = document.getElementsByClassName(DEFAULT_APPLET_CLASS)[0];

	if (applet == null) {
		return;
	}

	console
			.log("Add-on ADC instalado! A assinatura digital via applet será automaticamente desabilitada!");
	applet.style.display = "none";
}

function changeSigningMethod() {
	var signButton = document.getElementsByClassName(DEFAULT_SIGN_BUTTOM_CLASS)[0];

	if (signButton == null) {
		return;
	}

	var onclick = signButton.getAttribute("onclick");

	var oldcmd = onclick.match(/javascript:assinar.*?;/g)[0];

	if (oldcmd.indexOf("assinarComPlugin") > 0) {
		return;
	}

	var args = oldcmd.match(/'.*?'/g);

	signButton.setAttribute("onclick", "javascript:assinarComPlugin(" + args[1]
			+ ", " + args[2] + ");");

}

function assinar(appletId, conteudoId, compromissosId) {
	assinarComPlugin(conteudoId, compromissosId);
}

/**
 * Javascript de auxílio ao Assinador
 */

function assinarComPlugin(conteudoId, compromissosId) {

	submitted = false;

	var conteudoElm = document.getElementById(conteudoId);
	var compromissosElm = document.getElementById(compromissosId);

	try {
		checkConteudoDisponivel(conteudoElm);
	} catch (e) {
		window.alert(e);
		return;
	}

	var compromissos = compromissosElm.value;

	// Obtém os conteúdos a serem assinados
	var conteudos = conteudoElm.value;

	// O conteúdo assinado pelo assinador será devolvido no mesmo elemento HTML
	conteudoElm.value = "";

	self.port.emit("commitments", compromissos);
	self.port.emit("contents", conteudos);
	self.port.emit("assinar", null);

	self.port.on("conteudoAssinado", function(message) {

		conteudoElm.value = message;
		submit();

	});

}

function submit() {

	if (submitted) {
		return;
	}

	if (unsafeWindow.salvarConteudoAssinado != null) {
		unsafeWindow.salvarConteudoAssinado();
		return;
	}

	if (unsafeWindow.salvarConteudosAssinados != null) {
		unsafeWindow.salvarConteudosAssinados();
		return;
	}

	var elm = document.getElementsByClassName(DEFAULT_SUBMIT_CLASS)[0];

	if (elm == null) {
		return;
	}

	elm.tagName == "form" ? elem.submit() : elm.click();

	submitted = true;
}

function checkConteudoDisponivel(conteudoElm) {

	if (conteudoElm.value.length > 0) {
		return;
	}
	sleep(2000);

	if (conteudoElm.value.length <= 0) {
		throw "Nenhum conteúdo disponível no componente HTML '"
				+ conteudoElm.id + "'.";
	}

}

function selectTokenAccessMethod(method) {
	self.port.emit("tokenAccessMethod", "" + method);
}

function sleep(millis) {
	var date = new Date();
	var curDate = null;
	do {
		curDate = new Date();
	} while (curDate - date < millis);
}

function arrayBufferToBase64(buffer) {
	var binary = '';
	var bytes = new Uint8Array(buffer);
	var len = bytes.byteLength;
	for (var i = 0; i < len; i++) {
		binary += String.fromCharCode(bytes[i]);
	}
	return window.btoa(binary);
}

function base64ToArrayBuffer(base64) {
	var binary_string = window.atob(base64);
	var len = binary_string.length;
	var bytes = new Uint8Array(len);
	for (var i = 0; i < len; i++) {
		var ascii = binary_string.charCodeAt(i);
		bytes[i] = ascii;
	}
	return bytes.buffer;
}