window.onload = function() {
	
	// Check for the various File API support.
	if (window.File && window.FileReader && window.FileList && window.Blob) {
		// Great success! All the File APIs are supported.
	} else {
		alert('The File APIs are not fully supported in this browser.');
	}
}

function calculaBase64DoConteudoDoArquivo(fileId, conteudoId) {

	var conteudoInput = document.getElementById(conteudoId);
	var fileInput = document.getElementById(fileId);
	
	try {
		var reader = new FileReader();

		reader.onload = function() {
			conteudoInput.value = arrayBufferToBase64(reader.result);
		}
		
		reader.readAsArrayBuffer(fileInput.files[0]);

	} catch (e) {
		window.alert(e);
	}
}

function salvar(fileId, conteudoId) {
	
	var conteudoInput = document.getElementById(conteudoId);
	var fileInput = document.getElementById(fileId);
	
	if(fileInput.value == null || fileInput.value.length == 0){
		window.alert("Nenhum conteúdo disponível para baixar");
		return;
	}
	
	var bloob = new Blob([base64ToArrayBuffer(conteudoInput.value)], {type: "application/pdf"});
	saveAs(bloob, getFilename(fileInput.value));
}

function getFilename(filepath){
	
	var delimiter = "";
	if(filepath.indexOf("/") >= 0){
		delimiter = "/";
	}
	if(filepath.indexOf("\\") >= 0){
		delimiter = "\\";
	}
	if(delimiter == ""){
		return filepath;
	}
	
	var resp = filepath.split(delimiter);
	return resp[resp.length - 1];
}

function sleep(millis) {
	var date = new Date();
	var curDate = null;
	do {
		curDate = new Date();
	} while (curDate - date < millis);
}

function arrayBufferToBase64(buffer) {
	var binary = '';
	var bytes = new Uint8Array(buffer);
	var len = bytes.byteLength;
	for (var i = 0; i < len; i++) {
		binary += String.fromCharCode(bytes[i]);
	}
	return window.btoa(binary);
}

function base64ToArrayBuffer(base64) {
	var binary_string = window.atob(base64);
	var len = binary_string.length;
	var bytes = new Uint8Array(len);
	for (var i = 0; i < len; i++) {
		var ascii = binary_string.charCodeAt(i);
		bytes[i] = ascii;
	}
	return bytes.buffer;
}



function habilitarBtn(baixarBtnId, conteudoId){
	
	var conteudoInput = document.getElementById(conteudoId);
	
	if(conteudoInput == null || conteudoInput.value == null || conteudoInput.value.length == 0){
		return;
	}
	
	var btn = document.getElementById(baixarBtnId);
	
	btn.disabled=false;
}

function desabilitarBtn(baixarBtnId, conteudoId){
	
	var btn = document.getElementById(baixarBtnId);
	
	btn.disabled=true;
}