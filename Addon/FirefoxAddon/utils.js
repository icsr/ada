var chrome = require('chrome');
var Cc = chrome.Cc;
var Ci = chrome.Ci;
var Cu = chrome.Cu;

const path = require('sdk/fs/path');
const file = require('sdk/io/file');
const osfile = Cu.import("resource://gre/modules/osfile.jsm", {});
var TextDecoder = osfile.TextDecoder;
var TextEncoder = osfile.TextEncoder;
var OS = osfile.OS;

exports.checkHash  = checkHash;

exports.promptForFile = promptForFile;

exports.pathJoin = pathJoin;

exports.salvarConteudoEmArquivo = salvarConteudoEmArquivo;

exports.lerConteudoAssinadoEmArquivo = lerConteudoAssinadoEmArquivo;

exports.checkFile = checkFile;

exports.exists = exists;

function exists(filepath){
	return file.exists(filepath);
}


function checkFile(filepath){
	
	console.log(filepath);
	
	if(filepath != null && file.exists(filepath)){
		return;
	}
	
	throw "Arquivo não encontrado em " + filepath;
	
}

function pathJoin(path1, path2){
	return "" + path.join(path1, path2);
}


function checkHash(filepath, validHash){
	
	if(filepath == null || !file.exists(filepath)){
		return;
	}
	
	if(md5File(filepath).toLowerCase() != validHash.toLowerCase()){
		throw "Arquivo " + filepath + " está obsoleto.";
	}
}

function promptForFile() {
	  const nsIFilePicker = Ci.nsIFilePicker;

	  var fp = Cc["@mozilla.org/filepicker;1"]
	           .createInstance(nsIFilePicker);

	  var window = require("sdk/window/utils").getMostRecentBrowserWindow();
	  fp.init(window, "Selecione pasta desejada...", nsIFilePicker.modeGetFolder);
	  fp.appendFilters(nsIFilePicker.filterAll | nsIFilePicker.filterText);

	  var rv = fp.show();
	  if (rv == nsIFilePicker.returnOK || rv == nsIFilePicker.returnReplace) {
	    var file = fp.file;
	    // Get the path as string. Note that you usually won't
	    // need to work with the string paths.
	    var path = fp.file.path;
	    // work with returned nsILocalFile...
	  }
	  return ""+path;
}

//return the two-digit hexadecimal code for a byte
function toHexString(charCode) {
  return ("0" + charCode.toString(16)).slice(-2);
}

function md5File(path) {
  var f = Cc["@mozilla.org/file/local;1"]
          .createInstance(Ci.nsILocalFile);
  f.initWithPath(path);
  var istream = Cc["@mozilla.org/network/file-input-stream;1"]           
                .createInstance(Ci.nsIFileInputStream);
  // open for reading
  istream.init(f, 0x01, 0444, 0);
  var ch = Cc["@mozilla.org/security/hash;1"]
           .createInstance(Ci.nsICryptoHash);
  // we want to use the MD5 algorithm
  ch.init(ch.MD5);
  // this tells updateFromStream to read the entire file
  const PR_UINT32_MAX = 0xffffffff;
  ch.updateFromStream(istream, PR_UINT32_MAX);
  // pass false here to get binary data back
  var hash = ch.finish(false);

  // convert the binary hash data to a hex string.
  var s = Array.from(hash, (c, i) => toHexString(hash.charCodeAt(i))).join("");
  return s;
}

function salvarConteudoEmArquivo(arquivo, conteudo){
	var TextWriter = file.open(arquivo, "w");
	if (!TextWriter.closed) {
	    TextWriter.write(conteudo);
	    TextWriter.close();
	}
}

function lerConteudoAssinadoEmArquivo(arquivo){
	try{
		var text = null;
		  if (file.exists(arquivo)) {
		    var TextReader = file.open(arquivo, "r");
		    if (!TextReader.closed) {
		      text = TextReader.read();
		      TextReader.close();
		    }
		  }
		  return text;
	}catch(e){
		console.log(e);
	}
}

//Não funciona em Windows
function copyFile(sourcePath, destPath){
	var promise = file.copy(sourcePath, destPath);
	
	promise.then(
	  function(aVal) {
	    console.log('Arquivo ' + sourcePath + " copiado!");
	  },
	  function(aReason) {
	    console.error('FAIL, aReason:', aReason)
	    console.error('FAIL, aReason:', aReason.toString())
	  }
	);
}